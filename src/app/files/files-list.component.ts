/*
 * Angular 2 decorators and services
 */
import { Component, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { File } from './file';

// Directives

// Services
import { ConfigService } from '../config.service';
import { AuthService } from '../auth/auth.service';
import { PeopleService } from '../people/people.service';

// Pipes

/*
 * Files List Component
 * 
 */
@Component({

  selector: 'files-list-component',
  providers: [ File ],
  styleUrls: ['./files.scss'],
  template: `
    <div class="files-wrapper">
      <div *ngFor="let file of fileList" class="row">
        <div class="small-3 columns">{{file.created | todate | date:'d MMM y'}}</div>
        <div class="small-4 columns"><a [href]="config.fileServer + file.itemID + '/' + file.name" target="_blank">{{file.name}}</a></div>
        <div class="small-4 columns"> by {{(file.person | async)?.name}}</div>
        <div *ngIf="deleteable" class="small-1 columns"><a (click)="delete(file)">x</a></div>
      </div>  
    </div>
  `
})

 export class FilesListComponent {

  @Input() itemID: string;
  @Input() deleteable: boolean = false;
  @Output() deleted = new EventEmitter();
  public files: FirebaseListObservable<any>; 
  public fileList: Array<any>; 

  constructor(
    private _af: AngularFire,
    private _auth: AuthService,
    private _file: File,
    public people: PeopleService,
    public config: ConfigService
  ) {
    this.fileList = [];
  }

  ngOnInit() {
    this._file.init(this.itemID);
    this.load();
  }

  ngOnChanges() {
    this.load();
  }

  load() {
    
    this.files = this._af.database.list("/files/" + this._auth.companyID + '/' + this.itemID + '/'); 

    this.files.subscribe((values) => {
      this.fileList = [];
      values.forEach((val) => {
        val['person'] = this._af.database.object("/people/" + this._auth.companyID + '/' + val['createdByID']);
        this.fileList.push(val);
      });
    });
  }

  delete(file) {
    this._file.removeAndDelete(file, () => {
      this.deleted.emit(file);
    });
  }

}

