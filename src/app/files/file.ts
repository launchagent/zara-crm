/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Model } from '../model';
// Services
import { AuthService } from '../auth/auth.service';
import { ConfigService } from '../config.service';
import { HttpService } from '../http.service';

/*
 * File Model
 * 
 */
@Injectable()

export class File extends Model {

  public snapshot: fileFields = {
    created: '',
    description: '',
    name: '',
    extension: '',
    createdByID: '',
    itemID: ''
  };  

  constructor(
      _af: AngularFire,
      _auth: AuthService,
      private _config: ConfigService,
      private _http: HttpService
  ) {
    super(_af, _auth);
  }

  init(itemID) {
    this._ref = "/files/" + this._auth.companyID + '/' + itemID;
    this.list = this._af.database.list(this._ref);    
  }

  removeAndDelete(file, callback) {
    this.load(file['$key'], {'onSuccess': () => {
      this._http.postPath(this._config.restAPI + '/files/delete', {files: [file['itemID'] + '/' + file['name']], type: 'file'}).subscribe((response) => {
        super.delete();
        callback();
      });
    }});
  }
}

export interface fileFields {
  created: string,
  description: string,
  name: string,
  extension: string,
  createdByID: string,
  itemID: string
}

