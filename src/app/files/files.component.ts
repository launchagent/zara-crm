/*
 * Angular 2 decorators and services
 */
import { Component, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { File } from './file';

// Directives

// Services
import { ConfigService } from '../config.service';
import { AuthService } from '../auth/auth.service';
import { PeopleService } from '../people/people.service';

// Pipes

/*
 * File Component
 * 
 */
@Component({
  selector: 'files-component',
  providers: [ File ],
  styleUrls: ['./files.scss'],
  template: `
  
    <div class="files-wrapper">
      <div class="row" *ngIf="file && file.object">
        <div class="small-3 columns">{{file.snapshot?.created}}</div>
        <div class="small-4 columns"><a [href]="config.fileServer + file.snapshot?.itemID + '/' + file.snapshot?.name" target="_blank">{{file.snapshot?.name}}</a></div>
        <div class="small-4 columns"> by {{(person | async)?.name}}</div>
        <div *ngIf="deleteable" class="small-1 columns"><a (click)="delete(fileID)">x</a></div>
      </div>
    </div>
  `
})

 export class FilesComponent {

  @Input() itemID: string = '';
  @Input() fileID: string = ''; 
  @Input() deleteable: boolean = false;
  @Output() deleted = new EventEmitter();

  public person: FirebaseObjectObservable<any>; 
  public test: string = 'no change';

  constructor(
    private _af: AngularFire,
    private _auth: AuthService,
    public file: File,
    public people: PeopleService,
    public config: ConfigService
  ) {
    this.file.loaded.subscribe(() => {
      if(this.file.ID) {
        if(this.file.snapshot && this.file.snapshot['createdByID']) {
          console.log(this.file.snapshot);
          
          this.person = this._af.database.object("/people/" + this._auth.companyID + '/' + this.file.snapshot['createdByID']);
        }
      }
    });
  }

  ngOnInit() {    
    this.load();
  }

  ngOnChanges() {
    this.load();
  }

  load() {
    this.file.init(this.itemID);
    this.file.load(this.fileID);
    console.log(this.file);
    
  }

  delete(file) {
    this.file.removeAndDelete(file, () => {
      this.deleted.emit(file);
    });
  }

}

