/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Model } from '../model';

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

/*
 * Marketing Item Model
 * 
 */
@Injectable()

export class MarketingItem extends Model {

  public fields: marketingItemFields = {
      created: '',
      createdByID: '',
      name: '',
      options: {}
  };  

  constructor(
      _af: AngularFire,    
      _auth: AuthService,
      private _message: MessageService

  ) {
    super(_af, _auth);
    this._ref = "/marketing-items/" + this._auth.companyID;
    this.list = this._af.database.list(this._ref);
  }

  addOption(option, callback) {
    var list = this._af.database.list(this._ref + '/' + this.ID + '/options');
    list.push(option).then(() => {
      callback();
    });
  }

}

export interface marketingItemFields {
  created: string,
  createdByID: string,
  name: string,
  options: any 
}

