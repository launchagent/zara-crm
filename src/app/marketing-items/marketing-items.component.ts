/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Observable } from 'rxjs/Observable';

// Models
import { MarketingItem } from '../marketing-items/marketing-item';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Marketing Pane Component
 * 
 */
@Component({
  selector: 'marketing-items-component',
  providers: [ WorkflowItem, MarketingItem ],
  styles: [''],
  templateUrl: './marketing-items.component.html'
})

export class MarketingItemsComponent {

  @Input() workflowItem: WorkflowItem;
  public marketingItemsList: Observable<any>;
  public marketingItems: Observable<any>;
  public options: Observable<any>;
  public orderedBy: string;
  public marketingItem: any;
  public selectedOption: any;
  public price: number;
  public showForm: boolean = false;
  public showFormLabel: string = '+ Add item';

  constructor(
    private _confirm: ModalService,
    private _auth: AuthService,
    private _af: AngularFire,
    private _message: MessageService, 
    public workflowItemsService: WorkflowItemsService
  ) {
   this.marketingItemsList = this._af.database.list('/marketing-items/' + this._auth.companyID);
 
  }

  ngOnInit() {
    if(this.workflowItem) {
      this.marketingItems = this._af.database.list('/workflow-items/' + this._auth.companyID + '/' + this.workflowItem.ID + '/marketing-items');
    }
  }

  ngOnChanges() {

  }

  loadOptions(id) {
    this.options = this._af.database.list('/marketing-items/' + this._auth.companyID + '/' + id + '/options');
  }

  selectPrice(id) {
    this.options.forEach((values) => {
      for (var i = 0; i < values.length; ++i) {
        if(values[i].$key == id) {
          this.price = values[i].price;
        }
      }
    });
  }

  updateBooked(Item, state) {
    Item.booked = state;
    this.workflowItem.updateMarketingItem(Item);
  }

  addPayment(Item, event) {
    this.workflowItem.addMarketingItemPayment(Item, event.target.value);
  }

  updateCompleted(Item, state) {
    Item.completed = state;
    this.workflowItem.updateMarketingItem(Item);
  }

  toggleForm() {
    this.showForm = (this.showForm) ? false : true;
    this.showFormLabel = (this.showForm) ? 'Done' : '+ Add Item';
  }

  delete(ID) {
    this._confirm.triggerConfirm('Are you sure you want to delete this marketing item?', () => {
      this.workflowItem.removeMarketingItem(ID).subscribe(() => {
        this._message.set('Marketing item deleted', 'success');
      });
    }, () => { });
  }

  addItem() {
    var item = '';
    var option = '';

    // Loop through and get the marketing item name and option name
    this.marketingItemsList.forEach((values) => {
      for (var i = 0; i < values.length; ++i) {
        if (values[i].$key == this.marketingItem) {
          item = values[i]['name'];
          for (var j in values[i]['options']) {
            if(j == this.selectedOption) {
              option = values[i]['options'][j]['option'];
            }
          }
        }  
      }
    });

    this.workflowItem.addMarketingItem(item, option, this.price, this.orderedBy).then(() => {
      this.marketingItem = '';
      this.selectedOption = '';
      this.orderedBy = '';
      this.price = null;
      this._message.set('Marketing Item added', 'success');
    });
  }


}

