/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives
import { SplashOverlayComponent } from '../modal/splash-overlay.component';

// Services
import { AuthService } from '../auth/auth.service';
import { ConfigService } from '../config.service';
import { MessageService } from '../message/message.service';

// Pipes


/*
 * Login Component
 * 
 */
@Component({
  selector: 'login',
  providers: [],
  styleUrls: ['./login.component.scss'],
  template: `
  	<div class="row">
      <div class="small-12 medium-10 medium-centered large-6 columns content">
      <form (ngSubmit)="login()" #loginForm="ngForm">
        <div><p>Enter your email and password to login:</p></div>
        <input type="email" placeholder="Email" [(ngModel)]="email" name="email" required>
        <input type="password" placeholder="Password" [(ngModel)]="password" name="password" required>
        <button type="submit" class="button" [disabled]="!loginForm.form.valid">Login</button>
        <div><a (click)="toggleResetPassword()">Forgot your password? Click here to reset it.</a></div>
        <div *ngIf="showResetPassword">
          <input type="text" [(ngModel)]="resetEmail" name="resetEmail" placeholder="your email" />
          <a (click)="resetPassword()" class="button">Reset</a>
        </div>
      </form>
      </div>
	</div>
  <splash-overlay [state]="overlayState">
    <div class="loading">
      <h1>Zara CRM</h1>
      <p>Loading...Please wait</p>
      <div class="spinner white"></div>
      </div>
  </splash-overlay>
  `
})

export class LoginComponent {

  private _ref: FirebaseListObservable<any>;
  public email: string;
  public resetEmail: string;
  public showResetPassword: boolean = false;
  public password: string;
  public newPassword: string;
  public confirmedPassword: string;
  public overlayState: string = 'open';

  constructor(
    private _af: AngularFire,
    private _auth: AuthService,
    private _router: Router,
    private _message: MessageService
  ) {
   
    this._af.auth.subscribe(authData => {
      if(authData) {
        this._router.navigate(['/workflow']);         
      }
      this.overlayState = 'closed';
    });
  }

  ngOnInit() {
    this._auth.isLoggedIn.subscribe(() => {
      this._router.navigate(['/workflow']); 
      this.overlayState = 'closed'; 
    });
  }


  toggleResetPassword() {
    this.showResetPassword = (this.showResetPassword) ? false : true;
  }

  login() {
    this._message.set('Logging in...', 'success');
    this._af.auth.login({"email": this.email, "password": this.password });

  }

  resetPassword() {
    // firebase.user.sendPasswordResetEmail(this.resetEmail, (error) => {
    //   if (error) {
    //     switch (error.code) {
    //       case "INVALID_USER":
    //         this._message.set("The specified user account does not exist.", 'error');
    //         break;
    //       default:
    //         this._message.set("Error resetting password: " + error, 'error');
    //     }
    //   } else {
    //     this._message.set("Password reset email sent successfully!", 'success');
    //   }
    // });
  }

}

