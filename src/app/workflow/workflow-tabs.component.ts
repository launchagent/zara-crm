/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives

// Services

// Pipes

/*
 * Workflow Tabs Component
 * 
 */
@Component({
  selector: 'workflow-tabs',
  providers: [ WorkflowItem ],
  styleUrls: ['./workflow-tabs.scss'],
  template: `
  	<ul class="tabs" *ngIf="workflowItem">
		<li><a [routerLink]="['/workflow/details', workflowItem.ID]" routerLinkActive="active">Listing</a></li>
		<li><a [routerLink]="['/workflow/buyers', workflowItem.ID]" routerLinkActive="active">Buyers</a></li>
		<li><a [routerLink]="['/workflow/timeline', workflowItem.ID]" routerLinkActive="active">Timeline</a></li>
		<li><a [routerLink]="['/workflow/files', workflowItem.ID]" routerLinkActive="active">Files</a></li>
		<li><a [routerLink]="['/workflow/notes', workflowItem.ID]" routerLinkActive="active">Notes</a></li>
	</ul>
  `
})

export class WorkflowTabsComponent {

  @Input() workflowItem: WorkflowItem;

  constructor(
      private _route: ActivatedRoute,
      private _workflowItem: WorkflowItem
  ) {

  }

  ngOnInit() {
    var ID = this._route.snapshot.params['id'];
    if(!this.workflowItem && ID) {
      this._workflowItem.load(ID);
      this.workflowItem = this._workflowItem;
    }
  }

}

