/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';

/*
 * Workflow Service
 * 
 */
@Injectable()

export class WorkflowService {

  public stages: Array<any>;

  constructor(
  ) {
    this.stages = [
      {title: 'Prospect', className: 'prospect', requirements: 1, base: {vendor: 'ID', property: 'ID'}},
      { title: 'Appraisal', className: 'appraisal', requirements: 1},
      { title: 'Listing', className: 'listing', requirements: 4},
      { title: 'Market', className: 'market', requirements: 1},
      { title: 'Sold', className: 'sold', requirements: 1}
    ];
  }

  getNextStage(stage: string) {
    var index = this.getStageIndex(stage);
    if(index < (this.stages.length - 1)) {
      return this.stages[index + 1].className; 
    } else {
      return 'archive';
    }
  }

  getStageIndex(stage: string) {
    for (var i = 0; i < this.stages.length; ++i) {
      if(this.stages[i].className == stage) {
        return i;
      }  
    }
  }

  loadStage(stage: string) {
    for (var i = 0; i < this.stages.length; ++i) {
      if(this.stages[i].className === stage) {
        return this.stages[i];
      }
    }    
  }

}

