/*
 * Angular 2 decorators and services
 */
import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Person } from '../people/person';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { WorkflowService } from './workflow.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';
import { ModalService } from '../modal/modal.service';

// Pipes

/*
 * Workflows Component
 * 
 */
@Component({
  selector: 'workflows',
  providers: [ WorkflowItem, Person ],
  styleUrls: ['./workflow.component.scss'],
  templateUrl: './workflow.component.html'
})

export class WorkflowComponent {

  private _root: string;
  public prospectItems: FirebaseListObservable<any>;
  public appraisalItems: FirebaseListObservable<any>;
  public listingItems: FirebaseListObservable<any>;
  public marketItems: FirebaseListObservable<any>;
  public soldItems: FirebaseListObservable<any>;
  public archiveItems: FirebaseListObservable<any>;

  public selectedItem: WorkflowItem;
  public showArchive: boolean = false;
  public overlayState: string = 'open';
  public modalState: string = 'closed';

  constructor(
      private _auth: AuthService,
      private _message: MessageService,
      private _person: Person,
      private _af: AngularFire,
      private _router: Router,
      public modal: ModalService,
      public workflows: WorkflowService,
      public workflowItemsService: WorkflowItemsService
      
  ) {
    this._auth.isLoggedIn.subscribe((val) => {
      if(val) {
        this.setupLists();
      }
    });
  }

  ngOnInit() {
    if(this._auth.companyID) {
     this.setupLists();
    }
  }

  setupLists() {
      this._root = "/workflow-items/" + this._auth.companyID;
      this.prospectItems = this._af.database.list(this._root, {query: {orderByChild: 'stage', equalTo: 'prospect'}});
      this.appraisalItems = this._af.database.list(this._root, { query: { orderByChild: 'stage', equalTo: 'appraisal' } });
      this.listingItems = this._af.database.list(this._root, { query: { orderByChild: 'stage', equalTo: 'listing' } });
      this.marketItems = this._af.database.list(this._root, { query: { orderByChild: 'stage', equalTo: 'market' } });
      this.soldItems = this._af.database.list(this._root, { query: { orderByChild: 'stage', equalTo: 'sold' } });
      this.archiveItems = this._af.database.list(this._root, { query: { orderByChild: 'isArchived', equalTo: true } }); 
      this.overlayState = 'closed';   
  }

  getItems(stage: string) {
    var items = null;
    switch (stage) {
      case "prospect":
        items = this.prospectItems;
      break;
      case "appraisal":
        items = this.appraisalItems;
      break;
      case "listing":
        items = this.listingItems;
      break;
      case "market":
        items = this.marketItems;
      break;
      case "sold":
        items = this.soldItems;
      break;
    }
    return items;
  }

  getFirst(obj) {
    if(obj) {
      return obj[Object.keys(obj)[0]];      
    }
  }

  toggleArchive() {
    this.showArchive = (this.showArchive) ? false : true;
  }

  checkNavState(state) {
    if(state == 'closed') {
      this._router.navigate(['/workflow']);
    }
  }

}

