/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { WorkflowItem } from '../workflow-items/workflow-item';

/*
 * Workflow Model
 * 
 */
@Injectable()

export class Workflow {

  public stages: Array<Object>;
  public selectedWorkflowItem: WorkflowItem;

  constructor(
  ) {
    this.stages = [
      {
        title: 'Prospect',
        className: 'prospect', 
        requirements:[
          'this',
          'that',
          'the other',
        ]
      },
      {title: 'Appraisal', className: 'appraisal'},
      {title: 'Listing', className: 'listing'},
      {title: 'To Market', className: 'market'},
      {title: 'Offers', className: 'offers'},
      {title: 'Sold', className: 'sold'}
    ];
  }

}

