import { Routes, RouterModule }          from '@angular/router';
import { AuthGuard } from '../auth/auth-guard';
import { WorkflowComponent } from './workflow.component';
import { DefaultSectionComponent } from './sections/default-section.component';
import { DetailsSectionComponent } from './sections/details-section.component';
import { BuyersSectionComponent } from './sections/buyers-section.component';
import { TimelineSectionComponent } from './sections/timeline-section.component';
import { FilesSectionComponent } from './sections/files-section.component';
import { NotesSectionComponent } from './sections/notes-section.component';

export const routes: Routes = [
  {
    path: 'workflow',
    component: WorkflowComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'buyers/:id',   component: BuyersSectionComponent },
      { path: 'timeline/:id',   component: TimelineSectionComponent },
      { path: 'files/:id',   component: FilesSectionComponent },
      { path: 'details/:id',  component: DetailsSectionComponent },
      { path: 'notes/:id',  component: DetailsSectionComponent },
      { path: '',  component: DefaultSectionComponent }
    ]
  }
];

export const WorkflowRouting = RouterModule.forChild(routes);