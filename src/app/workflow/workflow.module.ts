import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PeopleModule } from '../people/people.module';
import { PropertiesModule } from '../properties/properties.module';
import { WorkflowRouting } from './workflow.routing';
import { WorkflowComponent } from './workflow.component';

// Directives
import { PersonForm } from '../people/person-form.component';
import { WorkflowTabsComponent } from './workflow-tabs.component';
import { WorkflowItemsComponent } from '../workflow-items/workflow-items.component';
import { WorkflowItemDetail } from '../workflow-items/workflow-item-detail.component';

import { DefaultSectionComponent } from './sections/default-section.component';
import { DetailsSectionComponent } from './sections/details-section.component';
import { BuyersSectionComponent } from './sections/buyers-section.component';
import { TimelineSectionComponent } from './sections/timeline-section.component';
import { FilesSectionComponent } from './sections/files-section.component';
import { NotesSectionComponent } from './sections/notes-section.component';

import { MarketingSectionComponent } from '../workflow-items/sections/marketing-section.component';
import { PropertySectionComponent } from '../workflow-items/sections/property-section.component';
import { VendorSectionComponent } from '../workflow-items/sections/vendor-section.component';

import { MarketingItemsComponent } from '../marketing-items/marketing-items.component';
import { OpenHomesComponent } from '../workflow-items/open-homes.component';

import { StagesModule } from '../stages/stages.module';

@NgModule({
    imports: [ CommonModule, SharedModule, WorkflowRouting, PeopleModule, PropertiesModule, StagesModule ],
    declarations: [ 
        WorkflowComponent, 
        WorkflowTabsComponent, 
        WorkflowItemsComponent, 
        WorkflowItemDetail, 
        DefaultSectionComponent,
        DetailsSectionComponent,
        BuyersSectionComponent,
        TimelineSectionComponent,
        FilesSectionComponent,
        NotesSectionComponent,
        MarketingSectionComponent,
        PropertySectionComponent,
        VendorSectionComponent,
        MarketingItemsComponent,
        OpenHomesComponent
    ],
    exports: [ WorkflowComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class WorkflowModule {
}