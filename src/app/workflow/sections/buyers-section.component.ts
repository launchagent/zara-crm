/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Event } from '../../events/event';
import { Person } from '../../people/person';
import { WorkflowItem } from '../../workflow-items/workflow-item';

// Directives

// Services
import { AuthService } from '../../auth/auth.service';
import { MessageService } from '../../message/message.service';
import { ModalService } from '../../modal/modal.service';
import { PeopleService } from '../../people/people.service';
import { WorkflowItemsService } from '../../workflow-items/workflow-items.service';

// Pipes

/*
 * Buyers Section Component
 * 
 */
@Component({
  selector: 'buyers-section',
  providers: [ Event, Person, WorkflowItem ],
  styleUrls: ['./sections.scss'],
  template: `
  <div class="buyers-wrap">	
    <modal-component #personFormModal ><person-form (update)="person.name = $event.name; person.ID = $event.ID; personFormModal.close();"></person-form></modal-component>
    <ul class="no-bullet"> 
      <li *ngFor="let buyer of buyerList" class="row buyer">
        <div class="small-2 columns"><a [routerLink]=" ['/people', buyer.personID] ">{{(buyer.person | async)?.name}}</a></div>
        <div class="small-3 columns"><a href="mailto:{{(buyer.person | async)?.email}}">{{(buyer.person | async)?.email}}</a></div>
        <div class="small-2 columns"><a *ngIf="(buyer.person | async)?.attributes" href="tel:{{(buyer.person | async)?.attributes['mobile-phone']}}">{{(buyer.person | async)?.attributes['mobile-phone']}}</a></div>
        <div class="small-2 columns"><a *ngIf="(buyer.person | async)?.attributes" href="tel:{{(buyer.person | async)?.attributes['work-phone']}}">{{(buyer.person | async)?.attributes['work-phone']}}</a></div>
        <div class="small-2 columns"><a *ngIf="(buyer.person | async)?.attributes" href="tel:{{(buyer.person | async)?.attributes['home-phone']}}">{{(buyer.person | async)?.attributes['home-phone']}}</a></div>
          <a (click)="toggleStatusCallout(buyer)">{{buyer.status}}</a>
          <div class="status-callout" *ngIf="buyer.showStatusCallout">
            <ul class="no-bullet">
              <li *ngFor="let status of buyerStatuses"><a *ngIf="status != buyer.status" (click)="selectBuyerStatus(status, buyer)">{{status}}</a></li>     
            </ul>
          </div>
        <div class="small-1 columns actions"><a (click)="removeBuyer(buyer)">x</a></div>
      </li>
    </ul>   
    <div class="add-buyer-wrap">   
	    <input type="text" placeholder="Add Buyer" [(ngModel)]="person.name" name="name" #name="ngModel" (keyup)="autocompletePerson.open()" (blur)="autocompletePerson.close();" autocomplete="off" required>
	    <autocomplete-person #autocompletePerson [key]="person.name" [values]="peopleList | async" (update)="person.name = $event.name; person.ID = $event.$key;  addBuyer(); personFormModal.close();" [createForm]="personFormModal"></autocomplete-person>    
	    <button class="button" (click)="personFormModal.toggle()">+ Add</button>
	</div>
  </div>
  `
})

export class BuyersSectionComponent {

  public peopleList: Observable<any[]>;
  public buyers: Observable<any[]>;
  public buyerStatuses: Array<string>;
  public buyerList: Array<any>;

  constructor(
      private _auth: AuthService,
      private _confirm: ModalService,
      private _message: MessageService,
      private _route: ActivatedRoute,
      public af:AngularFire,
      public event: Event,
      public person: Person,
      public people: PeopleService,
      public workflowItem: WorkflowItem,
      public workflowItemsService: WorkflowItemsService
  ) {
    this.peopleList = this.af.database.list("/people/" + this._auth.companyID);
    this.buyerStatuses = [
      'Interested',
      'Thinking about it',
      'Not interested',
      'Cannot contact'
    ];

  }

  ngOnInit() {
    if(this._auth.companyID) {
      this.load();
    } else {
      this._auth.isLoggedIn.subscribe((val) => {
        if(val) {
          this.load();
        }
      });
    }
  }

  load() {
    if(this.workflowItemsService.workflowItem) {
      this.workflowItem = this.workflowItemsService.workflowItem;
    } else {
      var ID = this._route.snapshot.params['id'];
      this.workflowItem.load(ID);
    }
    this.workflowItem.object.subscribe(() => {  
      this.buyers = this.af.database.list('/workflow-items/' + this._auth.companyID + '/' + this.workflowItem.ID + '/buyers');
      this.buyers.subscribe((values) => {
        this.buyerList = [];
        values.forEach((val) => {
          val['person'] = this.af.database.object("/people/" + this._auth.companyID + '/' + val['personID']);
          this.buyerList.push(val);
        });
      });
    });
  }

  clear() {
    this.person.ID = '';
    this.person.object['name'] = '';
  }

  addBuyer() {
    if(this.person.ID && this.person.ID !== '' && this.person.ID !== '') {
      this.person.load(this.person.ID, {'onSuccess': () => {
        this.person.addType('buyer');
      }});  
      this.workflowItem.addTo('buyers', {'personID': this.person.ID, 'status': 'interested'}, {'triggerSave': false, 'testKey': 'personID', 'testValue': this.person.ID, 'onDuplicate': () => {
        this._message.set('Buyer already exists on this item', 'warning');
      }});
    }
  }

  removeBuyer(buyer) {
    this._confirm.triggerConfirm('Are you sure you want to remove this buyer?', () => {
      this.workflowItem.removeFrom('buyers', buyer.$key, {'triggerSave': false});
    }, () => {});
  }

  toggleStatusCallout(buyer) {
    buyer.showStatusCallout = (buyer.showStatusCallout) ? false : true;
    return buyer;
  }

  selectBuyerStatus(status, buyer) {
    buyer.status = status;
    this.af.database.object(this.workflowItem.path + '/buyers/' + buyer.$key ).update({'status': status})
    return buyer;
  }

}

