/*
 * Angular 2 decorators and services
 */
import { Component, Input, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives
import { FilesListComponent } from '../../files/files-list.component';

// Services
import { ConfigService } from '../../config.service';
import { AuthService } from '../../auth/auth.service';

// Pipes


/*
 * Files Section Component
 * 
 */
@Component({
  selector: 'files-section',
  providers: [ ],
  styleUrls: ['./sections.scss'],
  template: `
    <files-list-component [itemID]="workflowItemID"></files-list-component>
  `
})

export class FilesSectionComponent {

  public workflowItemID: string;

  constructor(
    private _af: AngularFire,
    private _auth: AuthService,
    private _route: ActivatedRoute,
    public config: ConfigService
  ) {
  }

  ngOnInit() {
    this.workflowItemID = this._route.snapshot.params['id'];
  }


}

