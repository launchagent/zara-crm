/*
 * Angular 2 decorators and services
 */
import { Component, Input, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';


// Models
import { Event } from '../../events/event';

// Directives

// Services
import { AuthService } from '../../auth/auth.service';
import { PeopleService } from '../../people/people.service';

// Pipes

/*
 * Timeline Section Component
 * 
 */
@Component({
  selector: 'timeline-section',
  providers: [ Event ],
  styleUrls: ['./sections.scss'],
  template: `
    <div class="timeline-wrapper">
      <div *ngFor="let event of events | async" class="row">
        <div class="small-4 columns">{{event.created}}</div>
        <div class="small-4 columns">{{event.description}} 
          <span *ngIf="event.personID && event.personID != '' && people.values[event.personID]">{{people.values[event.personID].name}}</span>
        </div>  
        <div class="small-4 columns" *ngIf="event.createdByID && event.createdByID != '' && people.values[event.createdByID]"> by {{people.values[event.createdByID].name}}</div>
    </div>
  `
})

export class TimelineSectionComponent {

  @Input() workflowItemID: string;
  public events: Observable<any>;  

  constructor(
    private _auth: AuthService,
    public people: PeopleService,
    public af:AngularFire
  ) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    if(this.workflowItemID) {
      this.events = this.af.database.list("/events/" + this._auth.companyID, {
        query: {
          orderByChild: 'workflowID',
          equalTo: this.workflowItemID
        }
      });
    }
  }

}

