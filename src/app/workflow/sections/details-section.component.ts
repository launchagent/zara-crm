/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// Models
import { WorkflowItem } from '../../workflow-items/workflow-item'

// Directives
import { MarketingSectionComponent } from '../../workflow-items/sections/marketing-section.component';
import { PropertySectionComponent } from '../../workflow-items/sections/property-section.component';
import { VendorSectionComponent } from '../../workflow-items/sections/vendor-section.component';
import { ProspectStageComponent } from '../../stages/prospect.component';
import { AppraisalStageComponent } from '../../stages/appraisal.component';
import { ListingStageComponent } from '../../stages/listing.component';
import { MarketStageComponent } from '../../stages/market.component';
import { SoldStageComponent } from '../../stages/sold.component';

// Services
import { AuthService } from '../../auth/auth.service';
import { WorkflowItemsService } from '../../workflow-items/workflow-items.service';

// Pipes

/*
 * Details Section Component
 * 
 */
@Component({
  selector: 'details-section',
  providers: [ WorkflowItem ],
  styleUrls: ['./sections.scss'],
  templateUrl: './details-section.component.html'
})

export class DetailsSectionComponent {

  @Input() workflows;

  public selected: string = ''; 

  constructor(
    private _route: ActivatedRoute,
    private _auth: AuthService,
    public workflowItem: WorkflowItem,
    public workflowItemsService: WorkflowItemsService
  ) {

  }

  ngOnInit() {
    if(this._auth.companyID) {
      this.load();
    } else {
      this._auth.isLoggedIn.subscribe((val) => {
        if(val) {
          this.load();
        }
      });
    }
  }

  load() {
    
    this.selected = '';
    // If the workflow item is supplied from the service then we don't need to load it
    if(this.workflowItemsService.workflowItem) {
      this.workflowItem = this.workflowItemsService.workflowItem;
      this.setup();
    } else {
      var ID = this._route.snapshot.params['id'];
      this.workflowItem.load(ID).subscribe(() => {  
        if(this.workflowItem.snapshot) {
          this.workflowItemsService.workflowItem = this.workflowItem;
          this.setup();
        }
      });
    }
  }

  setup() {
      this.workflowItemsService.loadCurrentStage();
      this.workflowItemsService.setTotals();
  }

  ngOnChanges() {
    
  }

  getActive(stage: string) {
    return (this.workflowItemsService.selectedStage.className == stage) ? stage + ' active' : '';
  }

  getActivePane(pane: string) {
    return (this.selected == pane) ? 'active' : '';
  }

}

