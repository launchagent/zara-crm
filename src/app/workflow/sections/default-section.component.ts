/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { WorkflowItem } from '../../workflow-items/workflow-item';

// Directives

// Services
import { AuthService } from '../../auth/auth.service';
import { MessageService } from '../../message/message.service';
import { ModalService } from '../../modal/modal.service';
import { PeopleService } from '../../people/people.service';

// Pipes

/*
 * Default Section Component
 * 
 */
@Component({
  selector: 'default-section',
  providers: [ WorkflowItem ],
  styleUrls: ['./sections.scss'],
  template: `
  <div class="default-wrap">	
  </div>
  `
})

export class DefaultSectionComponent {

  @Input() workflowItem: WorkflowItem;

  constructor(
      private _auth: AuthService,
      private _message: MessageService,
      public af:AngularFire
  ) {

  }

  ngOnInit() {

  }

  ngOnChanges() {

  }

}

