/*
 * Angular 2 decorators and services
 */
import { Component, Input, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services
import { ConfigService } from '../../config.service';
import { AuthService } from '../../auth/auth.service';
import { PeopleService } from '../../people/people.service';

// Pipes


/*
 * Notes Section Component
 * 
 */
@Component({
  selector: 'notes-section',
  providers: [ ConfigService ],
  styleUrls: ['./sections.scss'],
  template: `
    <div>
      <div *ngFor="let note of notes | async" class="row">
        <div class="small-4 columns">{{note.created}}</div>
        <div class="small-4 columns">{{note.title}}</div>
        <div class="small-4 columns"> by {{people.values[note.createdByID].name}}</div>
      </div> 
      <note-form [itemID]="workflowItemID"></note-form> 
    </div>
  `
})

export class NotesSectionComponent {

  @Input() workflowItemID: string;
  public notes: Observable<any>;  

  constructor(
    private _af: AngularFire,
    private _auth: AuthService,
    public people: PeopleService,
    public config: ConfigService
  ) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    if(this.workflowItemID) {
      this.notes = this._af.database.list("/notes/" + this._auth.companyID + '/' + this.workflowItemID);
    }
  }

}

