import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { AutocompletePropertyPipe } from './autocomplete-property.pipe';

@Component({

  selector: 'autocomplete-property',
  template: `
    <div class="autocomplete" [ngClass]="state">
      <ul>
        <li *ngFor="let value of values | lookup: key; let i = index" (click)="selectSuggestion(value)">{{value.name}}</li>
        <li> <a (click)="onAddNew.emit(true);">+ Add</a></li>
      </ul>
    </div>
  `,
  styleUrls: ['./autocomplete.scss']
})
export class AutocompleteProperty {
  @Input() key = 'a';
  @Input() values = [];
  @Output() onAddNew = new EventEmitter(); 
  @Output() update = new EventEmitter();
  public selected: string;
  public index: number = 0;
  public state: string = 'closed';
  public matches: Array<string> = [];
	constructor(
    public elementRef: ElementRef
   ) {

	}

  open() {
    if(this.key && this.key.length > 1) {
      this.state = 'open';
    }
  }

  close() {
    setTimeout(() => {
      this.state = 'closed';
     }, 100);    
  }

  ngOnInit() {
    this.update.emit('');
  }
  addMatch(value: string) {
    this.matches.push(value);
  }
  selectSuggestion(value: any) {
    this.selected = value;
    this.update.emit(this.selected);
  }
  suggest(event: any) {
    // Arrow down
    if (event.keyCode === 40) {

    // Arrow up
    } else if (event.keyCode === 38) {

    // Enter
    } else if(event.keyCode === 13) {

    }
  }


}
