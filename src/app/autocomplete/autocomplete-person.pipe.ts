import { Pipe } from '@angular/core';

@Pipe({
  name: 'lookup'
})
export class AutocompletePersonPipe {
  transform(value, term) {
    term = term || ' ';
    value = value || [];
    return value.filter((val) => { 
		if (val.name.toUpperCase().startsWith(term.toUpperCase()) || val.email.toUpperCase().startsWith(term.toUpperCase()) ) {
			return val.name;
		}
    });
	}
}
