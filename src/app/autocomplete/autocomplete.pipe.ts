import { Pipe } from '@angular/core';

@Pipe({
  name: 'lookup'
})
export class AutocompletePipe {
  transform(value, term) {
    term = term || ' ';
    value = value || [];
    return value.filter((val) => val.name.toUpperCase().startsWith(term.toUpperCase()));
  }
}
