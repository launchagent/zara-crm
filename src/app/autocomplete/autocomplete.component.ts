import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { AutocompletePipe } from './autocomplete.pipe';

@Component({

  selector: 'autocomplete',
  template: `
    <div class="autocomplete" [ngClass]="state">
      <ul>
        <li *ngFor="let value of values | lookup: key; let i = index" (click)="selectSuggestion(value)">{{value}}</li>
      </ul>
    </div>
  `,
  styleUrls: ['./autocomplete.scss']
})
export class Autocomplete {
  @Input() key = '';
  @Input() values = [];
  @Output() update = new EventEmitter();
  public selected: string;
  public index: number = 0;
  public state: string = 'closed';
  public matches: Array<string> = [];
	constructor(
    public elementRef: ElementRef
   ) {

	}

  open() {
    if(this.key && this.key.length > 1) {
      this.state = 'open';
    }
  }

  close() {
    setTimeout(() => {
      this.state = 'closed';
     }, 100);    
  }

  ngOnInit() {
    this.update.emit('');
  }
  addMatch(value: string) {
    this.matches.push(value);
  }
  selectSuggestion(value: any) {
    this.selected = value;
    this.update.emit(this.selected);
  }
  suggest(event: any) {
    // Arrow down
    if (event.keyCode === 40) {

    // Arrow up
    } else if (event.keyCode === 38) {

    // Enter
    } else if(event.keyCode === 13) {

    }
  }


}
