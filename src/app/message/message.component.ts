/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';

// Models

// Directives

// Services
import { MessageService } from './message.service';

// Pipes

/*
 * Message Component
 * 
 */
@Component({
  selector: 'message',
  providers: [ ],
  styleUrls: ['./message.component.scss'],
  templateUrl: './message.component.html'
})

export class MessageComponent {

  private _delay: Number = 2500;
  public state: String = 'message-closed';
  public flash: String;

  constructor( 
    public messageService: MessageService
  ) {
    this.flash = '';
  }

  ngOnInit() {
    this.messageService.flash.subscribe(updatedFlash => { 
      this.flash = updatedFlash;
      if(this.flash !== '') {
          this.trigger();
      }
    });

    this.messageService.staticFlash.subscribe(updatedFlash => {
      this.flash = updatedFlash;
      if (this.flash !== '') {
        this.open();
      } else {
        this.close();
      }
    });
  }

  trigger() {
    this.state = 'message-open';
    setTimeout(() => {
      this.state = 'message-closed';
    }, this._delay);
  }

  open() {
    this.state = 'message-open';
  }

  close() {
    this.state = 'message-closed';
  }

}

