import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';

@Injectable()
export class MessageService {

  public flash: Observable<any>;
  public staticFlash: Observable<any>;
  private _flashObserver: Observer<any>;
  private _staticFlashObserver: Observer<any>;
  public message: string = '';
  public type: string = '';

  constructor() {
    // Create Observable Stream to output our data
    this.flash = new Observable(observer => this._flashObserver = observer)
      .startWith(this.message)
      .share();
    this.staticFlash = new Observable(observer => this._staticFlashObserver = observer)
      .startWith(this.message)
      .share();                
  }

  set(message: string, type: string = '') {
    this.message = message;
    this.type = type || 'message';
    this._flashObserver.next(this.message);
  }

  openStatic(message: string, type: string = '') {
    this.message = message;
    this.type = type || 'message';
    this._staticFlashObserver.next(this.message);
  }

  closeStatic() {
    this.message = '';
    this.type = 'message';
    this._staticFlashObserver.next(this.message);
  }

}
