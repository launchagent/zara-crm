import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ConfigService } from '../config.service';
import { MessageService } from '../message/message.service';

@Component({
  selector: 'file-uploader',
  providers: [ ConfigService ],
  styleUrls: ['./file-uploader.component.scss'],
  template: `
    <input type="file" (change)="fileChangeEvent($event)" [multiple]="multi" placeholder="Upload file..." />
    <button type="button" class="button" [disabled]="disableSave" (click)="upload()">{{label}}</button>
  `  
})
export class FileUploaderComponent {
  
    @Input() type: string = 'profile';
    @Input() ID: string = '';
    @Input() index: number = 0;
    @Input() multi: boolean = false;
    @Output() update = new EventEmitter();
   public filesToUpload: Array<File>;
   public label: string = 'Upload';
   public disableSave: boolean = false;

    constructor(
      private _config: ConfigService,
      private _message: MessageService
      ) {
        this.filesToUpload = [];
    }

    clearButton() {
        this.label = 'Upload';
        this.disableSave = false;      
    }
 
    upload() {
        this.disableSave = true;
        this.label = 'Uploading...';
        this.makeFileRequest(this._config.imageUploadServer + this.type, [], this.filesToUpload).then((result) => {
            this.update.emit(result);
            this.clearButton();
        }, (error) => {
            this._message.set('There was an error uploading the file', 'error');
            this.clearButton();
        });
    }
 
    fileChangeEvent(fileInput: any){
        this.filesToUpload = <Array<File>> fileInput.target.files;
    }
 
    makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
      return new Promise((resolve, reject) => {
        var formData: any = new FormData();
        formData.append('id', this.ID); //ID for file names
        if(this.index > 0) {
          formData.append('index', this.index);
        }
        var xhr = new XMLHttpRequest();
        for(var i = 0; i < files.length; i++) {
            formData.append("uploads[]", files[i], files[i].name);
        }

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(xhr.response);
                }
            }
        }
        xhr.open("POST", url, true);
        xhr.send(formData);
      });
    }

}