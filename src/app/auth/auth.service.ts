/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// 
import { Person } from '../people/person';

/*
 * Auth Service
 * 
 */
@Injectable()
export class AuthService {

  private _ref: FirebaseListObservable<any>;
  private _isLoggedInObserver: Observer<any>;
  public uid: string;
  public companyID: string
  public officeID: string;
  public isLoggedIn: Observable<any>;
  public userLevel: number = 0;
  public person: any;

  constructor(
    public af: AngularFire
  ) {
  
    // Set up the observable that we can subscribe to to check logged in status
    this.isLoggedIn = new Observable(observer => this._isLoggedInObserver = observer)
                      .startWith(false)
                      .share();

    // Subscribe to the Firebase auth state and load the logged in user if we are logged in
    this.af.auth.subscribe((authData) => {
      if(authData) {
        
        this.uid = authData.uid;
        this._ref = this.af.database.list('/people', {
          query: {
             orderByKey: true
          },
          preserveSnapshot: true
        });
     
        this._ref.subscribe((snapshot) => {
          snapshot.forEach((childSnapshot) => {
            for (var index in childSnapshot.val()) {
              if(index === authData.uid) {
                this.person = childSnapshot.val()[index];
                this.userLevel = childSnapshot.val()[index].userLevel;
                this.companyID = childSnapshot.key;
                this.officeID = childSnapshot.val()[index].officeID;   
                this._isLoggedInObserver.next(true);             
              }
            }
          });      
        });         
      }
    });
  }

}

