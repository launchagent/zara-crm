import { Injectable }      from '@angular/core';
import { AuthService }     from './auth.service';
import { CanActivate, Router }     from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private _authService: AuthService, private _router: Router) {

  }
  canActivate() {
    if (this._authService.uid && this._authService.companyID) { return true; }
    this._router.navigate(['/login']);
    return false;
  }
}