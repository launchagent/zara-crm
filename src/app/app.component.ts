/*
 * Angular 2 decorators and services
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/Observable';

// Models

// Services
import { TrademeService } from './integrations/trademe.service';
import { ModalService } from './modal/modal.service';

// Pipes

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {

  public version: string;

  constructor(
    public modalService: ModalService,
    public trademeService: TrademeService
  ) {
    this.version = '0.101';
  }

  ngOnInit() {
    
  }


}

