/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { AngularFire, FirebaseListObservable, FirebaseObjectObservable}  from 'angularfire2';
import * as moment from 'moment';

// Models

// Services
import { AuthService } from './auth/auth.service';

export interface methodOptions {
  onSuccess?: any,
  onError?: any,
  onDuplicate?: any,
  triggerSave?: boolean
};

export interface addToMethodOptions extends methodOptions {
  testKey?: string,
  testValue?: any
};


/*
 * Base Model Class
 * 
 */
@Injectable()

export class Model {

  protected _ref: string;
  public path: string;
  public list: FirebaseListObservable<any>;
  public object: FirebaseObjectObservable<any>;
  public attributes: FirebaseObjectObservable<any>;
  public snapshotObservable: FirebaseObjectObservable<any>;
  public snapshot: any;
  private _loaded: Subject<any>;
  private _saved: Subject<any>;
  private _userSaved: Subject<any>;
  public ID: string;
  public canDuplicate: boolean = true;
  public htmlTagRegex: RegExp = /<\/?[\w\s="/.':;#-\/\?]+>/gi;

  constructor(
      protected _af: AngularFire,
      protected _auth: AuthService
  ) {

    this._loaded = new Subject();
    this._saved = new Subject(); 
    this._userSaved = new Subject();       

    // Wait for the auth service to be logged in before running init()                    
    if(this._auth.companyID) {
      this.init();
    } else {
      this._auth.isLoggedIn.subscribe((val) => {
        if(val) {
          this.init();
        }
      });
    }                           
  }

  get loaded() {
    return this._loaded.asObservable();
  }

  get saved() {
    return this._saved.asObservable();
  }

  get userSaved() {
    return this._userSaved.asObservable();
  }

  /*
  * Empty method used by child models to run code dependant on htis._auth.companyID
  */
  init(key?: any) {

  }  

  /*
  * Clean up any attributes and make sure there are no html tags in them
  */
  sanitizeAttributes(attributes) {
     for (var prop in attributes) {
       if (typeof attributes[prop] === 'string') {
         attributes[prop] = attributes[prop].replace(this.htmlTagRegex, "");
       }
     }
     return attributes;
  }

  /*
  * Create a new record for the model in the database
  */
  create(object) {
    object.created = moment().format('YYYY-MM-DD H:mm:ss');
    object.lastUpdated = object.created;
    object.createdByID = this._auth.uid;
    const newRef = this.list.push(object);
    this.load(newRef.key);
    return newRef;
  }

  /*
  * Set a single field on the model and save to the database
  */
  update(field: string, value: any, triggerSave?: boolean) {
    var object = {};
    object[field] = value;
    this.object.update(object).then(() => {
      this.setLastUpdated(triggerSave);
    });
    return this.object;
  } 

  /*
  * Delete the entire model record from the database
  */
  delete(ID?: string) {
    this._saved.next(moment().format('YYYY-MM-DD H:mm:ss'));
    var ID = (ID) ? ID : this.ID;
    var update =  this.list.remove(ID);
    return update;
  }

  /*
  * Load the model by ID and set the async object and snapshot properties
  */
  load(ID: string, options?: methodOptions) {
    this.ID = ID;
    this.path = this._ref + "/" + ID;
    this.object = this._af.database.object(this.path);
    this.attributes = this._af.database.object(this.path + '/attributes/');
    this.snapshotObservable = this._af.database.object(this.path, { preserveSnapshot: true });

    this.snapshotObservable.subscribe((snap) => {
      if(snap.exists()) {
        this._loaded.next(true);
        this.snapshot = snap.val();        
      }
      if(!snap.exists() && options && options['onError']) {
        options['onError']();
      }
      else if(options && options['onSuccess']) {
        options['onSuccess']();
      }
    });      
   
    return this.object;      
  } 

  /*
  * Clear the fields for the model
  */
  clear() {
    this.ID = '';
    for (var i in this.snapshot) {
      this.snapshot[i] = null;
    } 
  }  

  /*
  * Add a thing onto an endpoint of the model and save to the database
  */
  addTo(endpoint: string, value: any, options?: addToMethodOptions) {

    var query;
    var list = this._af.database.list(this.path + '/' + endpoint);

    if(options && options['testKey'] && options['testKey'] != '') {
      query = list.$ref.orderByChild(options['testKey']).equalTo(options['testValue']);
    } else {
      query = list.$ref.orderByValue().equalTo(value); 
    } 

    query.once('value', (snapshot) => {
      if(!snapshot.exists()) {     
        var newRef = list.push(value).then(() => {
          if(!options || typeof options.triggerSave === 'undefined') {
            this.setLastUpdated(true);
          }
        });
        if(options && options['onSuccess'] ) {
          options['onSuccess']();
        }
      } else if(options && options['onDuplicate'] ) {
          options['onDuplicate']();
      }
    });

    return list;
  }  

  /*
  * Remove a thing from an endpoint of the model and save to the database
  */
  removeFrom(endpoint: string, ID: string, options?: methodOptions) {
    var list = this._af.database.list(this._ref + '/' + this.ID + '/' + endpoint);

    if(!ID || ID === '') {
        if(options && options['onError'] ) {
          options['onError']();
        }        
    } else {
      list.remove(ID).then(() => {
        if(options && options['onSuccess'] ) {
          options['onSuccess']();
        }
        if(!options || typeof options.triggerSave === 'undefined') {
          this.setLastUpdated(true);
        }
      });
    }
    return list;
  }  

  /*
  * Save all the attributes on the model
  */
  saveAttributes(callback?: any) {
    this.sanitizeAttributes(this.snapshot['attributes']);
    this.object.update({ attributes: this.snapshot['attributes'] }).then(() => {
      this.setLastUpdated();
      if(callback) {
        callback();
      }
    });
    return this.object;
  }   

  /*
  * Set a single attribute on the model and save to the database
  */
  setAttribute(attribute: string, value: any) {
    var object = {};
    object[attribute] = value;
    this.attributes.update(object).then(() => {
      this.setLastUpdated();
    });
    return this.attributes;
  }  

  /*
  * Remove a single attribute on the workflow item
  */
  removeAttribute(attribute: string) {
    var object = {};
    object[attribute] = null;
    this.attributes.update(object).then(() => {
      this.setLastUpdated();
    });
    return this.attributes;
  }

  /*
  * Creates a new record in the database with the current snapshot
  * 
  */
  duplicate(field: string, value: any) {
    if(this.canDuplicate) {
      let copy = this.snapshot;
      copy[field] = value;
      return this.create(copy);
    }
  }  

  /*
  * Utility method for getting the first item in an object
  */
  getFirst(obj) {
    if(obj) {
      return obj[Object.keys(obj)[0]];      
    }
  }

  /*
  * Method run everytime the model gets saved to the database - it sets a last updated timestamp on the model then triggers a next on our saved observable
  */
  setLastUpdated(triggerSave?: boolean) {
    triggerSave = (triggerSave || typeof triggerSave == "undefined") ? true : false;
    var timestamp = moment().format('YYYY-MM-DD H:mm:ss');
    this.object.update({'lastUpdated': timestamp}).then(() => {
        if(triggerSave) {
          this._saved.next(timestamp);
        }
    });
  }

  /*
  * Run a list query on the ref for this model
  */
  queryList(query: Object, preserveSnapshot: boolean) {
    var options = {'query': query};
    if(preserveSnapshot) {
      options['preserveSnapshot'] = preserveSnapshot;
    }
    return this._af.database.list(this._ref, options);
  }

  /*
  * Watch for property file to be changed and auto-load the url
  */
  savedByUser() {
    var timestamp = moment().format('YYYY-MM-DD H:mm:ss');
    this._userSaved.next(timestamp);
  }  

}

