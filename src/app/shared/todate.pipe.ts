import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'todate'})
export class ToDatePipe implements PipeTransform {
    transform(value: any) {
        if(typeof value === 'string') {
            return Date.parse(value);
        }
    }
}