import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { sharedPipes } from './shared.pipes';
import { sharedDirectives } from './shared.directives';

@NgModule({
    imports: [ CommonModule, FormsModule ],
    declarations: [ 
        ...sharedPipes,
        ...sharedDirectives
    ],
    exports: [ 
        FormsModule,
        ...sharedPipes,
        ...sharedDirectives       
     ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
          ngModule: SharedModule,
          providers: [  ]
        };
    }
}