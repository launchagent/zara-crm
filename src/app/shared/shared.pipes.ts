import { SortPipe } from './sort.pipe';
import { ToDatePipe } from './todate.pipe';
import { UserLevelPipe } from './user-level.pipe';
import { ValuesPipe } from './values.pipe';
import { SearchPipe } from '../search/search.pipe';
import { PeoplePipe } from '../people/people.pipe';
import { AutocompletePipe } from '../autocomplete/autocomplete.pipe';
import { AutocompletePersonPipe } from '../autocomplete/autocomplete-person.pipe';
import { AutocompletePropertyPipe } from '../autocomplete/autocomplete-property.pipe';

export const sharedPipes =  [ 
        SortPipe,
        ToDatePipe,
        UserLevelPipe,
        ValuesPipe,
        SearchPipe,
        PeoplePipe,
        AutocompletePipe,
        AutocompletePersonPipe,
        AutocompletePropertyPipe
    ];