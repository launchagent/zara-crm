import { Pipe } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Pipe({
  name: 'userlevel'
})
export class UserLevelPipe {

	constructor(
		private _auth: AuthService
		) {

	}	

	transform(value) {
		if(value) {
			return value.filter((val) => (val.createdByID == this._auth.uid || this._auth.userLevel >= 10));	
		}
	}
}
