import { AttributesComponent } from '../attributes/attributes.component';
import { AttributesList } from '../attributes/attributes-list.component';
import { PriceSelectComponent } from '../attributes/price-select.component';
import { ModalComponent } from '../modal/modal.component';
import { SplashOverlayComponent } from '../modal/splash-overlay.component';
import { OverlayComponent } from '../modal/overlay.component';
import { ConfirmComponent } from '../modal/confirm.component';
import { SearchComponent } from '../search/search.component';
import { FileUploaderComponent } from '../uploader/file-uploader.component';
import { FilesComponent } from '../files/files.component';
import { FilesListComponent } from '../files/files-list.component';
import { Autocomplete } from '../autocomplete/autocomplete.component';
import { AutocompletePerson } from '../autocomplete/autocomplete-person.component';
import { AutocompleteProperty } from '../autocomplete/autocomplete-property.component';
import { ChattelsComponent } from '../workflow-items/chattels.component';
import { EventsComponent } from '../events/events.component';
import { PeopleLinksComponent } from '../people/people-links.component';
import { TodoForm } from '../todos/todo-form.component';
 
export const sharedDirectives = [
        AttributesComponent,
        AttributesList,
        EventsComponent,
        PeopleLinksComponent,
        PriceSelectComponent,
        ModalComponent,
        OverlayComponent,
        ConfirmComponent,
        FilesListComponent,
        FileUploaderComponent,
        Autocomplete,
        AutocompletePerson,
        AutocompleteProperty,
        SearchComponent,
        ChattelsComponent,
        FilesComponent,
        SplashOverlayComponent,
        TodoForm
    ];