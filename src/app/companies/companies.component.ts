/*
 * Angular 2 decorators and services
 */
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Company } from './company';

// Directives


// Services

// Pipes

/*
 * Companies Component
 * 
 */
@Component({

  selector: 'companies',
  providers: [ Company ],
  styleUrls: ['./companies.scss'],
  template: `
      <div>Companies</div>
      <div>

      </div>
      
      <div *ngFor="let company of companies | async">{{company.name}} <a (click)="addOffice(company.$key)">Add office</a></div>
  `
})

export class CompaniesComponent {

  public companies:Observable<any[]>

  constructor(
      private _router: Router,
      af:AngularFire,
      public company: Company
  ) {
    this.companies = af.database.list('/companies');
  }

  ngOnInit() {

  }

  addOffice(ID) {
    this.company.ID = ID;
    this.company.addOffice({name: 'Head Office'}).then(() => {});
  }

}

