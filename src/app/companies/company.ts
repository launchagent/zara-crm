/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Model } from '../model';

// Services
import { AuthService } from '../auth/auth.service';

/*
 * Company Model
 * 
 */
@Injectable()

export class Company extends Model {

  public snapshot: companyFields = {
    name: ''
  };

  constructor(
    _af: AngularFire,
    _auth: AuthService
    
  ) {
    super(_af, _auth);
    this._ref = '/companies';
    this.list = this._af.database.list(this._ref);
  }

  addOffice(office) {
    var list = this._af.database.list(this._ref + '/' + this.ID + '/offices');
    return list.push(office);
  }

}

export interface companyFields {
  name: string;
};

