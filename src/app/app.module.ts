import { enableProdMode, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppRouting } from './app.routing';
import { HttpModule, JsonpModule } from '@angular/http';
import { SharedModule } from './shared/shared.module';
import { AdminModule } from './admin/admin.module';
import { OffersModule } from './offers/offers.module';
import { WorkflowModule } from './workflow/workflow.module';
import { WorkflowItemsModule } from './workflow-items/workflow-items.module';
import { PeopleModule } from './people/people.module';
import { PropertiesModule } from './properties/properties.module';
 
import { AngularFireModule, AuthMethods, AuthProviders } from 'angularfire2';
import { AuthGuard } from './auth/auth-guard';
import { appModels } from './app.models';
import { appServices } from './app.services';
import { appDirectives } from './app.directives';

import { environment } from '../environments/environment';


if (environment.production) {
  enableProdMode();
}

const firebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};

@NgModule({
  imports: [ 
    BrowserModule,
    HttpModule,
    AdminModule,
    AppRouting,
    AngularFireModule.initializeApp(environment.configFirebase, firebaseAuthConfig),
    SharedModule.forRoot(),
    OffersModule,
    PeopleModule,
    PropertiesModule,
    WorkflowModule,
    WorkflowItemsModule
  ],
  providers: [ 
    AuthGuard,
    ...appModels,
    ...appServices
  ],
  declarations: [    
    AppComponent,
    ...appDirectives
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [ AppComponent ]
})
export class AppModule {

}

