/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives


// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';

// Pipes

/*
 * Settings Component
 * 
 */
@Component({
  selector: 'settings.component',
  providers: [ WorkflowItem ],
  styles: [''],
  template: `
    <div class="row">
    <div class="small-12 columns content" >
      <h1>Settings</h1>

    </div>
    </div>
  `
})

export class SettingsComponent {
  @Input() workflowItem: WorkflowItem;
  public settings: Observable<any[]>;
  public startDate: any;
  public startTime: any;
  public endDate: any;
  public endTime: any;

  constructor(
    private _auth: AuthService,
    private _message: MessageService,
    private _confirm: ModalService,
    public af: AngularFire
  ) {
  }

  ngOnInit() {
    
  }

  ngOnChanges() {
    if(this.workflowItem) {
      this.settings = this.af.database.list('/settings/' + this._auth.companyID);
    }
  }

  getFirst(obj) {
    if(obj) {
      return obj[Object.keys(obj)[0]];      
    }
  }

  add() {
    this.workflowItem.addOpenHome(this.startDate, this.startTime, this.endTime).then(() => {
      this._message.set('Offer added', 'success');
    });
  }

  delete(ID) {
    this._confirm.triggerConfirm('Are you sure you want to delete this offer?', () => {
      this.workflowItem.removeOpenHome(ID).subscribe(() => {
        this._message.set('Offer deleted', 'success');
      });
    }, () => {});
  }


}

