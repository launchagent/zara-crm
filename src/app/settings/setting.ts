/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Model } from '../model';

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

/*
 * Setting Model
 * 
 */
@Injectable()

export class Setting extends Model {

  constructor(
      _af: AngularFire,
      _auth: AuthService,
      private _message: MessageService
  ) {
    super(_af, _auth);

  }

  init() {
    this._ref = "/settings/" + this._auth.companyID;
    this.object = this._af.database.object(this._ref);
  }

  get(key) {
    var ref = this._af.database.object(this._ref + '/' + key);
    return ref;
  }

  generate(key, value) {
    if(key == '') {
      this._message.set('You need a key to save a setting', 'warning');
    } else {
      return this.update(key, value, false);
    }
  }

}

