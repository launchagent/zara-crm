// Directives
import { CompaniesComponent } from './companies/companies.component';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileMenuComponent } from './profile/profile-menu.component';
import { TodosComponent } from './todos/todos.component';
import { SettingsComponent } from './settings/settings.component';
import { ChangePasswordComponent } from './profile/change-password.component'

// Form Components
import { WorkflowItemForm } from './workflow-items/workflow-item-form.component';

export const appDirectives = [
    TodosComponent, 
    MessageComponent,
    LoginComponent,
    ProfileComponent,
    ProfileMenuComponent, 
    WorkflowItemForm, 
    CompaniesComponent,
    SettingsComponent,
    ChangePasswordComponent
];
