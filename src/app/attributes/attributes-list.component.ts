/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services
import { AttributeService } from './attribute.service';
import { MessageService } from '../message/message.service';

// Pipes

/*
 * Attributes List Component
 * 
 */
@Component({

  selector: 'attributes-list',
  providers: [ AttributeService ],
  styleUrls: ['./attributes.scss'],
  template: `
    <a *ngIf="editable" (click)="toggleForm()">Edit</a>
   
    <div *ngIf="!showForm && model.snapshot && model.snapshot.attributes">
      <div *ngFor="let field of attributes">
       <div class="row" *ngIf="model.snapshot.attributes[field.slug]">
          <div class="small-12 medium-6 columns"><label>{{field.display}}</label></div>
          <div class="small-12 medium-6 columns" *ngIf="field.slug == 'description'">{{model.snapshot.attributes[field.slug].substring(0,80)}}...</div>
          <div class="small-12 medium-6 columns" *ngIf="field.slug != 'description'">{{model.snapshot.attributes[field.slug]}}</div> 
       </div>

       <div class="row" *ngIf="field.type == 'suburb'">
          <div class="small-12 medium-6 columns"><label>Suburb</label></div>
          <div class="small-12 medium-6 columns">{{model.snapshot.attributes['suburb-name']}}</div> 
       </div>
      <div class="row" *ngIf="field.type == 'district'">
        <div class="small-12 medium-6 columns"><label>City</label></div>
        <div class="small-12 medium-6 columns">{{model.snapshot.attributes['city-name']}}</div> 
      </div>
      <div class="row" *ngIf="field.type == 'region'">
        <div class="small-12 medium-6 columns"><label>Region</label></div>
        <div class="small-12 medium-6 columns">{{model.snapshot.attributes['region-name']}}</div> 
      </div>
     
    </div>
    </div>
    
    <div *ngIf="showForm">
      <form (ngSubmit)="onSubmit()" #attributeForm="ngForm">
        <attributes [(model)]="model" [attributes]="attributes" [namespace]="namespace"></attributes>
        <button type="submit">Save</button>
      </form>
       
    </div>
  `
})

export class AttributesList {

  @Input() attributes: Array<any>;
  @Input() model: any; 
  @Input() section: string = '';
  @Input() namespace: string = '';
  @Input() editable: boolean = false; 
  public showForm: boolean = false;

  constructor(
    private _attributeService: AttributeService,
    private _message: MessageService
  ) {

  }

  ngOnInit() {
    this._attributeService.attributes = this.attributes;
    this.attributes = this._attributeService.mapAttributes(this.namespace, this.section);
  }

  toggleForm() {
    this.showForm = (this.showForm) ? false : true;
  }

  onSubmit() {
    this.model.saveAttributes(() => {
      this._message.set('Saved', 'success');
    });
  }

}

