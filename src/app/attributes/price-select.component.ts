/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

// Models

// Directives

// Services

// Pipes

/*
 * Price Select Component
 * 
 */
@Component({
  selector: 'price-select',
  styles: [''],
  template: `
    <div>
      <select [(ngModel)]="selected" #sel (change)="update.emit(sel.value)">
        <option *ngFor="let price of prices" [value]="price.Value">{{price.Display}}</option>
      </select>
    </div>
  `
})

export class PriceSelectComponent {

  @Input() selected: any;
  @Output() update = new EventEmitter();

  public prices: Array<any>;


  constructor(

  ) {
    this.prices = [
      {"Value":"100000", "Display":"$100,000"},
      {"Value":"150000", "Display":"$150,000"},
      {"Value":"200000", "Display":"$200,000"},
      {"Value":"250000", "Display":"$250,000"},
      {"Value":"300000", "Display":"$300,000"},
      {"Value":"350000", "Display":"$350,000"},
      {"Value":"400000", "Display":"$400,000"},
      {"Value":"450000", "Display":"$450,000"},
      {"Value":"500000", "Display":"$500,000"},
      {"Value":"550000", "Display":"$550,000"},
      {"Value":"600000", "Display":"$600,000"},
      {"Value":"650000", "Display":"$650,000"},
      {"Value":"700000", "Display":"$700,000"},
      {"Value":"750000", "Display":"$750,000"},
      {"Value":"800000", "Display":"$800,000"},
      {"Value":"850000", "Display":"$850,000"},
      {"Value":"900000", "Display":"$900,000"},
      {"Value":"950000", "Display":"$950,000"},
      {"Value":"1000000", "Display":"$1,000,000"},
      {"Value":"1100000", "Display":"$1,100,000"},
      {"Value":"1250000", "Display":"$1,250,000"},
      {"Value":"1350000", "Display":"$1,350,000"},
      {"Value":"1500000", "Display":"$1,500,000"},
      {"Value":"1600000", "Display":"$1,600,000"},
      {"Value":"1750000", "Display":"$1,750,000"},
      {"Value":"2000000", "Display":"$2,000,000"},
      {"Value":"2250000", "Display":"$2,250,000"},
      {"Value":"2500000", "Display":"$2,500,000"},
      {"Value":"3000000", "Display":"$3,000,000"},
      {"Value":"3500000", "Display":"$3,500,000"},
      {"Value":"4000000", "Display":"$4,000,000"},
      {"Value":"5000000", "Display":"$5,000,000"},
      {"Value":"7500000", "Display":"$7,500,000"},
      {"Value":"10000000", "Display":"$10,000,000+"}
    ];
  }

  ngOnInit() {
    this.update.emit(this.selected);
  }


}

