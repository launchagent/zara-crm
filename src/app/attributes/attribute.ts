/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';

/*
 * Attribute Model
 * 
 */
@Injectable()

export class Attribute {

  public namespace: string;
  public slug: string;
  public display: string;
  public section: string;
  public type: string;
  public values: Array<any>;
  public workflows: Array<any>;
  public required: string;
  public showIfField: string;
  public showIfValue: string;
  public setIfField: string;
  public setIfValue: string;
  public setIfSetValue: string;

  constructor(
    data: any
  ) {
    this.set(data);
  }

  set(data) {
    this.namespace = data.namespace;
    this.createSlug(data.display);
    this.display = data.display;
    this.type = data.type;
    this.section = data.section;
    this.values = data.values;
    this.workflows = data.workflows;
    this.required = data.required;
    this.showIfField = data.showIfField || '';
    this.showIfValue = data.showIfValue || '';
    this.setIfField = data.setIfField || '';
    this.setIfValue = data.setIfValue || '';
    this.setIfSetValue = data.setIfSetValue || '';
  }

  createSlug(name: String) {
    this.slug = name.toLowerCase().replace(/ /g, '-').replace(/[&]/g, 'and').replace(/[/]/g, '');
    this.slug = this.slug.replace(/[$]/g, '').replace(/[[]/g, '').replace(/[\]]/g,'');
    this.slug = this.slug.replace(/[\.]/g, '-').replace(/[,]/g, '-').replace(/[#]/g,'');
    return this.slug;
  }

  get() {
    return {
      namespace: this.namespace,
      slug: this.slug,
      display: this.display,
      type: this.type,
      section: this.section,
      values: this.values,
      workflows: this.workflows,
      required: this.required,
      showIfField: this.showIfField,
      showIfValue: this.showIfValue,
      setIfField: this.setIfField,
      setIfValue: this.setIfValue,
      setIfSetValue: this.setIfSetValue      
    };
  }

}

