/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Person Attribute Service
 * 
 */
@Injectable()

export class PersonAttributeService extends AttributeService {

  public attributes: Array<any>; 

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();
  }

  //Setup default values method
  // ----
  setup() {

    // General person fields
    var fields = ['Address', 'Mobile Phone', 'Home Phone', 'Work Phone'];
    this.addTextAttributes('general', 'info', [{ name: 'person', stage: '' }], fields, {});

    var data = {
      namespace: 'general',
      display: 'Referral',
      type: 'dropdown',
      section: 'info',
      values: ['Property Manager', 'Mortgage Broker', 'Insurance Broker', 'Friend', 'Other'],
      workflows: [{ name: 'person', stage: '' }],
      required: ''
    };
    var att = new Attribute(data);
    this.attributes.push(att);  

    // Vendor
    var vendorFields = ['Legal Name', 'Partner Name'];
    this.addTextAttributes('vendor', 'info', [{ name: 'person', stage: '' }], vendorFields, {});

    // Agent
    this.addBasicAttributes('agent', 'bio', [{ name: 'person', stage: '' }], 'textarea', ['Description'], {});
    this.addTextAttributes('agent', 'info', [{ name: 'person', stage: '' }], ['Job Title', 'REAA Number', 'Youtube Video ID', 'Trademe Image ID'], {});    
    this.addBasicAttributes('agent', 'info', [{ name: 'person', stage: '' }], 'checkbox', ['ARIENZ'], {});
  
    // Solicitor
    this.addTextAttributes('solicitor', 'info', [{ name: 'person', stage: '' }], ['Firm', 'Website', 'Account Name', 'Account Number'], {});

    // Buyer
    this.addTextAttributes('buyer', 'info', [{ name: 'person', stage: '' }], ['Price Range', 'Looking Area', 'Property Types'], {});
    this.addOneToFiveDropdown('Bedrooms', 'buyer', 'info', [{ name: 'person', stage: '' }]);
    this.addOneToFiveDropdown('Bathrooms', 'buyer', 'info', [{ name: 'person', stage: '' }]);
    this.addOneToFiveDropdown('Livingrooms', 'buyer', 'info', [{ name: 'person', stage: '' }]);
    this.addBasicAttributes('buyer', 'info', [{ name: 'person', stage: '' }], 'textarea', ['Special Requirements'], {});

  }


}

