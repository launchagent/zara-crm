/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Property Attribute Service
 * 
 */
@Injectable()

export class PropertyAttributeService extends AttributeService {

  public attributes: Array<any>; 

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();

  }

  //Setup default values method
  // ----
  setup() {
    // Listing type
    var data = {
      namespace: 'properties',
      display: 'Listing Type',
      type: 'dropdown',
      section: 'listing',
      values: ['Residential', 'Rural', 'Commercial'],
      workflows: [{ name: 'property', stage: 'appraisal' }],
      required: ''
    };
    var att = new Attribute(data);
    this.attributes.push(att);


    // Property type
    var data = {
      namespace: 'properties',
      display: 'Property Type',
      type: 'dropdown',
      section: 'listing',
      values: ['Apartment', 'House', 'Lifestyle - Bare land', 'Lifestyle - Dwelling', 'Section', 'Townhouse', 'Unit'],
      workflows: [{ name: 'property', stage: 'appraisal' }],
      required: ''
    };
    var att = new Attribute(data);
    this.attributes.push(att);


    // Property address
    var fields = ['Building/Complex Name', 'Line 2', 'Local Authority', 'Zoning', 'Rates', 'CV'];
    this.addTextAttributes('properties', 'address', [{ name: 'property', stage: '' }], fields, {});

    this.addDateAttributes('properties', 'address', [{ name: 'property', stage: '' }], ['CV Date'], {});

    var fields = ['Street Name'];
    this.addTextAttributes('properties', 'address', [{ name: 'property', stage: 'appraisal' }], fields, {});
    this.addBasicAttributes('properties', 'address', [{ name: 'property', stage: '' }], 'number', ['Street Number'], {});
    this.addTextAttributes('properties', 'address', [{ name: 'property', stage: '' }], ['Unit Number'], {});

    // Property location
    this.addBasicAttributes('properties', 'address', [{ name: 'property', stage: 'appraisal' }], 'region', ['Region Id'], {});     
    this.addBasicAttributes('properties', 'address', [{ name: 'property', stage: 'appraisal' }], 'district', ['City Id'], {});  
    this.addBasicAttributes('properties', 'address', [{ name: 'property', stage: 'appraisal' }], 'suburb', ['Suburb Id'], {});  

    var data = {
      namespace: 'properties',
      display: 'Country',
      type: 'dropdown',
      section: 'address',
      values: ['New Zealand'],
      workflows: [{name: 'property', stage: 'appraisal'}],
      required: '' 
    };
    var att = new Attribute(data);
    this.attributes.push(att);   

    var fields = ['Lot', 'DP', 'CT', 'Floor Area', 'Land Area', 'Year Built', 'Latitude', 'Longitude'];
    this.addTextAttributes('properties', 'address', [{ name: 'property', stage: '' }], fields, {});

    var data = {
    namespace: 'properties',
    display: 'Smoke Alarm',
    type: 'dropdown',
    section: 'listing',
    values: ['yes', 'no', 'dont know'],
    workflows: [{ name: 'property', stage: 'listing' }],
    required: ''
    };
    var att = new Attribute(data);
    this.attributes.push(att); 

    //Rooms
    this.addOneToTenDropdown('Bedrooms', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);
    this.addOneToTenDropdown('Bathrooms', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);
    this.addOneToTenDropdown('Ensuites', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);
    this.addOneToTenDropdown('Livingrooms', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);
    this.addOneToTenDropdown('Diningrooms', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);

    // Title
    var data = {
      namespace: 'properties',
      display: 'Property Title',
      type: 'dropdown',
      section: 'listing',
      values: ['Free Simple', 'Cross-lease', 'Strata', 'Leasehold', 'Cross-lease (Lease hold)', 'Strata (Lease hold)'],
      workflows: [{name: 'property', stage: 'listing'}],
      required: '' 
    };
    var att = new Attribute(data);
    this.attributes.push(att); 

    // Tenancy
    var data = {
      namespace: 'properties',
      display: 'Tenancy',
      type: 'dropdown',
      section: 'listing',
      values: ['Owner occupied', 'Tenanted', 'Vacant'],
      workflows: [{name: 'property', stage: 'listing'}],
      required: '' 
    };
    var att = new Attribute(data);
    this.attributes.push(att); 

    // Parking
    this.addOneToTenDropdown('Garages', 'properties', 'listing', [{ name: 'property', stage: '' }]);
    this.addOneToTenDropdown('Carports', 'properties', 'listing', [{ name: 'property', stage: '' }]);
    this.addOneToTenDropdown('Off Street', 'properties', 'listing', [{ name: 'property', stage: '' }]);  

    // Joinery
    var data = {
      namespace: 'properties',
      display: 'Joinery',
      type: 'dropdown',
      section: 'listing',
      values: ['Wooden', 'Aluminium', 'Mixed', 'Other'],
      workflows: [{name: 'property', stage: 'listing'}],
      required: '' 
    };
    var att = new Attribute(data);
    this.attributes.push(att);
    this.addTextAttributes('properties', 'listing', [{ name: 'property', stage: '' }], ['Joinery Other'], { showIfField: 'joinery', showIfValue: 'Other'});


    // Floors
    this.addOneToFiveDropdown('Floors', 'properties', 'listing', [{ name: 'property', stage: '' }]);
  

  }


}

