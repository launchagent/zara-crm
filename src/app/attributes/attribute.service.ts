/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';

/*
 * Attribute Service
 * 
 */
@Injectable()

export class AttributeService {

  public attributes: Array<any>;

  constructor(
  ) {
  }

  addBasicAttributes(namespace, section, workflows, type, values, options) {
    var options = options || {};
    for (var i = 0; i < values.length; ++i) {
      var required = '';
      if(values[i].match(/\*/)) {
        required = 'required';
        values[i] = values[i].replace('*', '');
      } 
      var data = {
        namespace: namespace,
        display: values[i],
        type: type,
        section: section,
        values: [],
        workflows: workflows,
        required: required,
        showIfField: '',
        showIfValue: '',
        setIfField: '',
        setIfValue: '',
        setIfSetValue: ''
      };

      if(options != {}) {
        data.showIfField = (options.showIfField) ? options.showIfField : '';
        data.showIfValue = (options.showIfValue) ? options.showIfValue : '';
        data.setIfField = (options.setIfField) ? options.setIfField : '';
        data.setIfValue = (options.setIfValue) ? options.setIfValue : '';
        data.setIfSetValue = (options.setIfSetValue) ? options.setIfSetValue : '';
      }

      var att = new Attribute(data);
      this.attributes.push(att);
    }      
  }

  addTextAttributes(namespace, section, workflows, values, options) {
    var options = options || {};
    this.addBasicAttributes(namespace, section, workflows, 'text', values, options);     
  }

  addNumberAttributes(namespace, section, workflows, values, options) {
    var options = options || {};
    this.addBasicAttributes(namespace, section, workflows, 'number', values, options);     
  }

  addDateAttributes(namespace, section, workflows, values, options) {
    var options = options || {};
    this.addBasicAttributes(namespace, section, workflows, 'date', values, options);     
  }

  addOneToTenDropdown(display, namespace, section, workflows) {
    var data = {
      namespace: namespace,
      display: display,
      type: 'dropdown',
      section: section,
      values: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
      workflows: workflows
    };
    var att = new Attribute(data);
    this.attributes.push(att);    
  }

  addOneToFiveDropdown(display, namespace, section, workflows) {
    var data = {
      namespace: namespace,
      display: display,
      type: 'dropdown',
      section: section,
      values: ['0', '1', '2', '3', '4', '5'],
      workflows: workflows
    };
    var att = new Attribute(data);
    this.attributes.push(att);    
  }

  mapAttributes(namespace: string, section: string) {
    var sectionAttributes = [];
    if(this.attributes) {
      for (var i = 0; i < this.attributes.length; ++i) {
        if (namespace == 'all' || namespace == this.attributes[i]['namespace'] || section == this.attributes[i]['section']) {
          sectionAttributes.push(this.attributes[i]);
        }
      }      
    }
    return sectionAttributes;
  }


}

