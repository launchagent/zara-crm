/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Listing Attribute Service
 * 
 */
@Injectable()

export class ListingAttributeService extends AttributeService {

  public attributes: Array<any>; 
  public prices: Array<any>;

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();

  }

  //Setup default values method
  // ----
  setup() {

    //Agency type
    var data = {
      namespace: 'properties',
      display: 'Agency Type',
      type: 'dropdown',
      section: 'listing',
      values: ['Exclusive', 'General', 'Joint'],
      workflows: [{name: 'property', stage: 'listing'}],
      required: '' 
    };
    var att = new Attribute(data);
    this.attributes.push(att);


    // Property address
    var fields = ['Agency Start Date', 'Agency End Date'];
    this.addDateAttributes('properties', 'address', [{ name: 'property', stage: 'listing' }], fields, {});

    //Commission
    var commissionData = {
      namespace: 'properties',
      display: 'Commission',
      type: 'dropdown',
      section: 'listing',
      values: ['Standard', 'Negotiated'],
      workflows: [{ name: 'property', stage: '' }],
      required: ''
    };
    var att = new Attribute(commissionData);
    this.attributes.push(att);

    this.addBasicAttributes('properties', 'address', [{ name: 'property', stage: '' }], 'textarea' , ['Negotiated commission details'], {showIfField: 'commission', showIfValue: 'Negotiated'});

    //Sales method
    var data = {
      namespace: 'properties',
      display: 'Sales Method',
      type: 'dropdown',
      section: 'listing',
      values: ['Asking Price', 'By Negotiation', 'Auction', 'Tender', 'Enquiries Over'],
      workflows: [{name: 'property', stage: 'listing'}],
      required: '' 
    };
    var att = new Attribute(data);
    this.attributes.push(att);

    this.addDateAttributes('properties', 'pricing', [{ name: 'property', stage: '' }], ['Auction Date'], { showIfField: 'sales-method', showIfValue: 'Auction' });
    this.addBasicAttributes('properties', 'pricing', [{ name: 'property', stage: '' }], 'time', ['Auction Time'], { showIfField: 'sales-method', showIfValue: 'Auction' });    

    this.addTextAttributes('properties', 'pricing', [{ name: 'property', stage: 'listing' }], ['Price'], {});
    this.addTextAttributes('properties', 'pricing', [{ name: 'property', stage: '' }], ['Min Price'], {});
    this.addTextAttributes('properties', 'pricing', [{ name: 'property', stage: '' }], ['Max Price'], {});

    //

  }


}

