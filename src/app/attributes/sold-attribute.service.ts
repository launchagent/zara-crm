/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Workflow Attribute Service
 * 
 */
@Injectable()

export class SoldAttributeService extends AttributeService {

  public attributes: Array<any>; 

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();
  }

  //Setup default values method
  // ----
  setup() {
    this.addDateAttributes('sold', 'settlement', [{name: 'property', stage: 'sold'}], ['Settlement Date'], {});

    this.addBasicAttributes('sold', 'settlement', [{ name: 'property', stage: 'sold' }], 'textarea', ['Vendor lawyer confirmation'], {});

    this.addBasicAttributes('sold', 'settlement', [{ name: 'property', stage: 'sold' }], 'checkbox', ['Contract sent to buyer lawyer', 'Contract sent to buyer', 'Contract sent to vendor lawyer', 'Contract sent to vendor'], {});

  }


}

