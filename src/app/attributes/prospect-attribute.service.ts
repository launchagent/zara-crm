/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Prospect Attribute Service
 * 
 */
@Injectable()

export class ProspectAttributeService extends AttributeService {

  public attributes: Array<any>; 

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();
  }

  //Setup default values method
  // ----
  setup() {


  }


}

