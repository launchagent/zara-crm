/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Listing Attribute Service
 * 
 */
@Injectable()

export class MarketingAttributeService extends AttributeService {

  public attributes: Array<any>; 
  public prices: Array<any>;

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();

  }

  //Setup default values method
  // ----
  setup() {

    // Advetising Copy
    this.addTextAttributes('general', 'general', [{ name: 'property', stage: 'listing' }], ['Youtube Video ID', 'Advertising Title'], {});
    this.addBasicAttributes('general', 'general', [{ name: 'property', stage: 'listing' }], 'textarea', ['Advertising Copy'], {});
    
    this.addBasicAttributes('general', 'general', [{ name: 'property', stage: 'listing' }], 'textarea', ['Local Amenities'], {});

    //

  }


}

