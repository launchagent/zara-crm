/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services
import { AttributeService } from './attribute.service';
import { TrademeService } from '../integrations/trademe.service';

// Pipes

/*
 * Attributes Component
 * 
 */
@Component({

  selector: 'attributes',
  providers: [ AttributeService ],
  styleUrls: ['./attributes.scss'],
  template: `
    <div *ngIf="model.snapshot && model.snapshot.attributes">
      <div *ngFor="let field of attributes">
        <div class="form-section row" [ngSwitch]="field.type" [ngClass]="field.validForStage">     
        <div *ngSwitchCase="'region'">
            <select #region [(ngModel)]="model.snapshot.attributes['region-id']" (change)="selectedRegion = region.value; updateSelects()">
              <option *ngFor="let region of localities" [value]="region.LocalityId" >{{region.Name}}</option>
            </select>
        </div>
        <div *ngSwitchCase="'district'">    
            <select #district [(ngModel)]="model.snapshot.attributes['city-id']" (change)="selectedDistrict = district.value; updateSelects()">
              <option *ngFor="let district of districts" [value]="district.DistrictId" >{{district.Name}}</option>
            </select>
        </div>
        <div *ngSwitchCase="'suburb'">    
            <select #suburb [(ngModel)]="model.snapshot.attributes['suburb-id']" >
              <option *ngFor="let suburb of suburbs" [value]="suburb.SuburbId" >{{suburb.Name}}</option>
            </select>       
        </div>

        <div *ngSwitchCase="'text'">
          <input *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue" type="text" [placeholder]="field.display" [(ngModel)]="model.snapshot.attributes[field.slug]" [name]="field.slug" >
        </div>

        <div *ngSwitchCase="'number'">
          <input *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue" type="number" [placeholder]="field.display" [(ngModel)]="model.snapshot.attributes[field.slug]" [name]="field.slug" >
        </div>  

        <div *ngSwitchCase="'checkbox'">
          <input *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue" type="checkbox" [(ngModel)]="model.snapshot.attributes[field.slug]" value="1" [name]="field.slug">{{field.display}}
        </div>

        <div *ngSwitchCase="'date'">
          <div *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue">
            <label>{{field.display}}</label>
            <input type="date" [(ngModel)]="model.snapshot.attributes[field.slug]" >          
          </div>
        </div>
      
        <div *ngSwitchCase="'time'">
          <div *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue">
            <label>{{field.display}}</label>
            <input type="time" [(ngModel)]="model.snapshot.attributes[field.slug]" >
            </div>
        </div>

        <div *ngSwitchCase="'dropdown'">
          <label *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue">{{field.display}}</label>
          <select *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue" [(ngModel)]="model.snapshot.attributes[field.slug]" >
           <option *ngFor="let value of field.values" value="{{value}}">{{value}}</option>
          </select>
        </div>

        <div *ngSwitchCase="'textarea'">
          <textarea *ngIf="field.showIfField === '' || model.snapshot.attributes[field.showIfField] == field.showIfValue" placeholder="{{field.display}}" [(ngModel)]="model.snapshot.attributes[field.slug]" rows="10"></textarea>
        </div>

        <div *ngSwitchCase="'fileupload'">
           test
        </div>

        </div>
      </div>
    </div>
  `
})

export class AttributesComponent {

  @Input() attributes: Array<any>;
  @Input() model: any;
  @Input() section: string = ''; 
  @Input() namespace: string = '';

  public localities: Array<any> = [];
  public districts: Array<any> = [];
  public suburbs: Array<any> = [];
  public selectedRegion: number;
  public selectedDistrict: number;
  public selectedSuburb: number;

  constructor(
      private _attributeService: AttributeService,
      public trademe: TrademeService
  ) {

    if (!this.trademe.localities || this.trademe.localities.length == 0 ) {
      this.trademe.getLocalities(() => {
        this.localities = this.trademe.localities;
      });
    } else {
      this.localities = this.trademe.localities;
    }
  }

  ngOnInit() {
    this._attributeService.attributes = this.attributes;
    this.attributes = this._attributeService.mapAttributes(this.namespace, this.section);
    this.selectedRegion = this.model.snapshot.attributes['region-id'];
    this.selectedDistrict = this.model.snapshot.attributes['city-id'];
    this.selectedSuburb = this.model.snapshot.attributes['suburb-id'];
    this.updateSelects();
  }

  updateSelects() {
    this.districts = [];
    this.suburbs = [];
    for (var i = 0; i < this.localities.length; ++i) {
      if(this.localities[i].LocalityId == this.selectedRegion) {
        this.districts = this.localities[i].Districts;
        for (var j = 0; j < this.localities[i].Districts.length; ++j) {
          if (this.localities[i].Districts[j].DistrictId == this.selectedDistrict) {
            this.suburbs = this.localities[i].Districts[j].Suburbs;
          }
        }
      }
    }      
  }


}

