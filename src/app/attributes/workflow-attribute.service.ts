/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Workflow Attribute Service
 * 
 */
@Injectable()

export class WorkflowAttributeService extends AttributeService {

  public attributes: Array<any>; 

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();
  }

  //Setup default values method
  // ----
  setup() {
    // Listing type
    var data = {
      namespace: 'properties',
      display: 'Listing Type',
      type: 'dropdown',
      section: 'listing',
      values: ['Residential', 'Rural', 'Commercial', 'Business'],
      workflows: [{name: 'property', stage: 'appraisal'}],
      required: 'required'
    };
    var att = new Attribute(data);
    this.attributes.push(att);


    // Property type
    var data = {
      namespace: 'properties',
      display: 'Property Type',
      type: 'dropdown',
      section: 'listing',
      values: ['Apartment', 'House', 'Lifestyle - Bare land', 'Lifestyle - Dwelling', 'Section', 'Townhouse', 'Unit'],
      workflows: [{name: 'property', stage: 'appraisal'}],
      required: 'required' 
    };
    var att = new Attribute(data);
    this.attributes.push(att);


    // Property address
    var fields = ['Building/Complex Name', 'Line 1*', 'Line 2', 'Suburb', 'City*', 'Country*', 'Lot', 'Land Area'];
    this.addTextAttributes('properties', 'address', [{ name: 'property', stage: 'prospect' }], fields, {});

    //Rooms
    this.addOneToTenDropdown('Bedrooms', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);
    this.addOneToTenDropdown('Bathrooms', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);
    this.addOneToTenDropdown('Livingrooms', 'properties', 'listing', [{ name: 'property', stage: 'listing' }]);

  }


}

