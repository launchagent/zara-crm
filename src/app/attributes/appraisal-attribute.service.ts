/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Attribute } from './attribute';
import { AttributeService } from './attribute.service';

/*
 * Workflow Attribute Service
 * 
 */
@Injectable()

export class AppraisalAttributeService extends AttributeService {

  public attributes: Array<any>; 

  constructor(
  ) {
    super();
    this.attributes = [];
    this.setup();
  }

  //Setup default values method
  // ----
  setup() {
    // Listing type

  }


}

