import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth-guard';
// Directives
import { CompaniesComponent } from './companies/companies.component';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { ModalComponent } from './modal/modal.component';
import { ConfirmComponent } from './modal/confirm.component';
import { PeopleComponent } from './people/people.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileMenuComponent } from './profile/profile-menu.component';
import { SettingsComponent } from './settings/settings.component';
import { TodosComponent } from './todos/todos.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { ChangePasswordComponent } from './profile/change-password.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},  
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  { path: 'companies', component: CompaniesComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
  { path: 'changepassword', component: ChangePasswordComponent },
  { path: '', pathMatch: 'full', redirectTo: '/workflow'}
];

export const AppRouting = RouterModule.forRoot(routes);