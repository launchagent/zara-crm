import { Routes, RouterModule }          from '@angular/router';
import { AuthGuard } from '../auth/auth-guard';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { AdminLocationsComponent } from './admin-locations.component';
import { AdminFormsComponent } from './admin-forms.component';
import { AdminMarketingComponent } from './admin-marketing.component';
import { AdminReportsComponent } from './admin-reports.component';
import { AdminSettingsComponent } from './admin-settings.component';
import { AdminUsersComponent } from './admin-users.component';
import { AdminUsersDetailComponent } from './admin-users-detail.component';

export const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'forms',  component: AdminFormsComponent },
      { path: 'marketing',  component: AdminMarketingComponent },
      { path: 'reports',  component: AdminReportsComponent },
      { path: 'settings',  component: AdminSettingsComponent },
      { path: 'users',  component: AdminUsersComponent },
      { path: 'users/:id',  component: AdminUsersDetailComponent },
      { path: 'settings/locations/:id',  component: AdminLocationsComponent },
      { path: '',  component: AdminDashboardComponent }
    ]
  }
];

export const AdminRouting = RouterModule.forChild(routes);