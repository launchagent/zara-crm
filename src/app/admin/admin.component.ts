import { Component, OnInit } from '@angular/core';

// Services
import { AdminService } from './admin.service';

@Component({
  selector: 'admin',
  templateUrl: 'admin.component.html',
  styleUrls: ['admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(
    public admin: AdminService
  ) { }

  ngOnInit() {
  }

}
