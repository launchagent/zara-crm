import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from './admin.service';
import { AuthService } from '../auth/auth.service';
import { TrademeService } from '../integrations/trademe.service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

@Component({
  selector: 'admin-locations',
  providers: [ TrademeService ],
  styleUrls: ['admin.component.scss'],
  template: `<div class="small-12 columns">Locations</div>
      <div class="small-12 columns" *ngFor="let location of locations | async">{{location.region}} {{location.city}} {{location.suburb}} <a (click)="removePlace(location.$key)">x</a></div>
      <form (ngSubmit)="onSubmit()" #officeForm="ngForm">
        <div>
            <select #region [(ngModel)]="regionName" (change)="selectedRegion = region.value; updateSelects()" name="regionName">
              <option *ngFor="let region of localities" [value]="region.Name" >{{region.Name}}</option>
            </select>
        </div>
        <div>    
            <select #district [(ngModel)]="cityName" (change)="selectedDistrict = district.value; updateSelects()" name="cityName">
              <option value="all">All</option>
              <option *ngFor="let district of districts" [value]="district.Name" >{{district.Name}}</option>
            </select>
        </div>
        <div>    
            <select #suburb [(ngModel)]="suburbName" name="suburbName">
              <option value="all">All</option>
              <option *ngFor="let suburb of suburbs" [value]="suburb.Name" >{{suburb.Name}}</option>
            </select>       
        </div>
        <button type="submit">Add to location</button>
      </form>  
  `
})
export class AdminLocationsComponent implements OnInit {

    public locations: FirebaseListObservable<any>;

    public regionName: string = '';
    public cityName: string = '';
    public suburbName: string = '';

    public localities: Array<any> = [];
    public districts: Array<any> = [];
    public suburbs: Array<any> = [];
    public selectedRegion: number;
    public selectedDistrict: number;
    public selectedSuburb: number;

  constructor(
      private _route: ActivatedRoute,
      private _af: AngularFire,
      private _auth: AuthService,
      public admin: AdminService,
      public trademe: TrademeService
  ) {
    let ID = this._route.snapshot.params['id'];
    this.locations = this._af.database.list('/locations/' + this._auth.companyID + '/' + this.admin.selectedOfficeID + '/' +  ID + '/places');  
    
    if (!this.trademe.localities || this.trademe.localities.length == 0 ) {
      this.trademe.getLocalities(() => {
        this.localities = this.trademe.localities;
      });
    } else {
      this.localities = this.trademe.localities;
    }
    this.updateSelects();
   }

  ngOnInit() {
  }

  updateSelects() {
    this.districts = [];
    this.suburbs = [];
    for (var i = 0; i < this.localities.length; ++i) {
      if(this.localities[i].Name == this.selectedRegion) {
        this.districts = this.localities[i].Districts;
        for (var j = 0; j < this.localities[i].Districts.length; ++j) {
          if (this.localities[i].Districts[j].Name == this.selectedDistrict) {
            this.suburbs = this.localities[i].Districts[j].Suburbs;
          }
        }
      }
    }      
  }

  removePlace(key) {
    this.locations.remove(key);
  }

  onSubmit() {
    this.locations.push({region: this.regionName, city: this.cityName, suburb: this.suburbName});
  }

}
