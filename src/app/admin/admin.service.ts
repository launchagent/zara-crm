/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// services
import { AuthService } from '../auth/auth.service';

/*
 * Admin Service
 * 
 */
@Injectable()
export class AdminService {

    private _ref: FirebaseListObservable<any>;
    public company: FirebaseObjectObservable<any>;
    public office: FirebaseObjectObservable<any>;
    public selectedOfficeID: string;

    constructor(
        private _auth: AuthService,
        public af: AngularFire
    ) {
        this.company = this.af.database.object('/companies/' + this._auth.companyID);
    }

    gotoRoot() {
        this.selectedOfficeID = '';
    }

    selectOffice(ID) {
        this.selectedOfficeID = ID;
        this.office = this.af.database.object('/companies/' + this._auth.companyID + '/offices/' + ID);
    }

}

