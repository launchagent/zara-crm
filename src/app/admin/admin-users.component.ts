import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Services
import { AdminService } from './admin.service';

@Component({
  selector: 'admin-users',
  styleUrls: ['admin.component.scss'],
  template: `<div>
          <h3>Users</h3>
            <div class="row">
                <div class="small-12 columns" *ngFor="let user of users | async "><a [routerLink]=" [user.$key]">{{user.name}}</a></div>
            </div>
          
      </div>`
})
export class AdminUsersComponent implements OnInit {

  public users: FirebaseListObservable<any>;

  constructor(
      private _af: AngularFire,
      private _auth: AuthService,
      public admin: AdminService
  ) {
    this.users = this._af.database.list('/people/' + this._auth.companyID, {
      query: {
        orderByChild: 'officeID',
        equalTo: this.admin.selectedOfficeID
      }
    });

   }

  ngOnInit() {
  }

}
