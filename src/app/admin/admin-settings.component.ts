import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Company } from '../companies/company';

// Services
import { AdminService } from './admin.service';

@Component({
  selector: 'admin-settings',
  providers: [ Company ],
  styleUrls: ['admin.component.scss'],
  template: `<div>
          <h3>Settings</h3>
            <div class="row">
                <div class="small-12 columns">Locations</div>
                <div class="small-12 columns" *ngFor="let location of locations | async"><a [routerLink]=" ['locations/' + location.$key] ">{{location.name}}</a></div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <form (ngSubmit)="onSubmit()" #locationForm="ngForm">
                        <input type="text" placeholder="add location" name="locationName" [(ngModel)]="locationName" required>
                        <button type="submit" class="tiny round">+</button>
                    </form>
                </div>
            </div>  
      </div>`
})
export class AdminSettingsComponent implements OnInit {

  public locations: FirebaseListObservable<any>;
  public officePath: string;
  public locationName: string;

  constructor(
      private _af: AngularFire,
      private _auth: AuthService,
      public admin: AdminService,
      public company: Company
  ) {

   }

  ngOnInit() {
    this.officePath = (this.admin.selectedOfficeID) ? '/locations/' + this._auth.companyID + '/' + this.admin.selectedOfficeID : '/locations/' + this._auth.companyID;
    this.locations = this._af.database.list(this.officePath);
  }

  onSubmit() {
    this.locations.push({name: this.locationName, suburbs: {}});
  }

}
