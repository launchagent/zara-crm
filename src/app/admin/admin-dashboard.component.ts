import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Company } from '../companies/company';

// Services
import { AdminService } from './admin.service';

@Component({
  selector: 'admin-users',
  styleUrls: ['admin.component.scss'],
  providers: [ Company ],
  template: `
      <div>
          <h3>{{(admin.company | async)?.name}} Offices</h3>
            <div class="row">
                <div class="small-12 columns" *ngFor="let office of offices | async "><a (click)="admin.selectOffice(office.$key)">{{office.name}}</a></div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <form (ngSubmit)="onSubmit()" #officeForm="ngForm">
                        <input type="text" placeholder="add office" name="officeName" [(ngModel)]="officeName" required>
                        <button type="submit" class="tiny round">+</button>
                    </form>
                </div>
            </div>            
      </div>
  `
})
export class AdminDashboardComponent implements OnInit {

    public offices: FirebaseListObservable<any>;
    public companyObject: FirebaseObjectObservable<any>;
    public officeName: string;

  constructor(
      private _af: AngularFire,
      private _auth: AuthService,
      public admin: AdminService,
      public company: Company
  ) { 
      this.offices = this._af.database.list('/companies/' + this._auth.companyID + '/offices');
      this.company.load(this._auth.companyID);
 }

  ngOnInit() {
  }

  onSubmit() {
    if(this.officeName != '') {
       this.company.addTo('offices', {'name': this.officeName}, {'testKey': 'name', 'testValue': this.officeName, 'onSuccess': () => {
           this.officeName = '';
       }});
    } else {
        return false;
    } 
  }

}
