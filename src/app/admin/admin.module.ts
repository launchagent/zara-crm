import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AdminRouting } from './admin.routing';

// Services
import { AdminService } from './admin.service';

// Directives
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { AdminFormsComponent } from './admin-forms.component';
import { AdminLocationsComponent } from './admin-locations.component';
import { AdminMarketingComponent } from './admin-marketing.component';
import { AdminReportsComponent } from './admin-reports.component';
import { AdminSettingsComponent } from './admin-settings.component';
import { AdminUsersComponent } from './admin-users.component';
import { AdminUsersDetailComponent } from './admin-users-detail.component';

@NgModule({
    imports: [ CommonModule, SharedModule, AdminRouting ],
    declarations: [ 
        AdminComponent,
        AdminDashboardComponent,
        AdminFormsComponent,
        AdminLocationsComponent,
        AdminMarketingComponent,
        AdminReportsComponent,
        AdminSettingsComponent,
        AdminUsersComponent,
        AdminUsersDetailComponent
    ],
    providers: [ AdminService ],
    exports: [ AdminComponent, AdminDashboardComponent, AdminSettingsComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AdminModule {
    
}