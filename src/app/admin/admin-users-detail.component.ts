import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Person } from '../people/person';

// Services
import { AdminService } from './admin.service';

@Component({
  selector: 'admin-users-detail',
  styleUrls: ['admin.component.scss'],
  providers: [ Person ],
  template: `<div>
          <h3>Edit User {{user.snapshot['name']}}</h3>
            <div class="row">
                <div class="small-12 columns" *ngFor="let location of userLocations | async">{{location.name}} <a (click)="removeLocation(location)">x</a></div>
            </div>
            <div class="row">
                <div class="small-12 columns" *ngFor="let location of locations | async"><a (click)="addLocation(location)">{{location.name}}</a></div>
            </div>
          
      </div>`
})
export class AdminUsersDetailComponent implements OnInit {

  public locations: FirebaseListObservable<any>;
  public userLocations: FirebaseListObservable<any>;

  constructor(
      private _af: AngularFire,
      private _auth: AuthService,
      private _route: ActivatedRoute,
      public admin: AdminService,
      public user: Person
  ) {
    
    
   }

  ngOnInit() {
    let ID = this._route.snapshot.params['id'];
    this.user.load(ID);
    this.userLocations = this._af.database.list('/people/' + this._auth.companyID + '/' + ID + '/locations');
    this.locations = this._af.database.list('/locations/' + this._auth.companyID + '/' + this.admin.selectedOfficeID);
  }

  addLocation(location) {
   this.user.update('locations/' + location.$key, {name: location.name, officeID: this.admin.selectedOfficeID});
  }

  removeLocation(location) {
   this.user.removeFrom('locations', location.$key);
  }

}
