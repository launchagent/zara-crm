import { Pipe } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe {
  transform(value, term, key) {

    term = term || '';
    value = value || [];
    return value.filter((val) =>  { 
      if(val[key]) {
        return val[key].toUpperCase().startsWith(term.toUpperCase());
      } 
    });	
  }
}
