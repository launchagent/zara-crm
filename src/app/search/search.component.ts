import { Component, Output, EventEmitter } from '@angular/core';

// Models

// Directives

// Services

// Pipes

@Component({
  selector: 'search-box',
  template: `<div>
    <input #input type="text" (input)="update.emit(input.value)" placeholder="search">
  </div>`
})
export class SearchComponent {
  @Output() update = new EventEmitter();

  ngOnInit() {
    this.update.emit('');
  }

}
