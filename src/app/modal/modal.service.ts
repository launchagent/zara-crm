import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';

@Injectable()
export class ModalService {

  public modal: Observable<any>;
  private _modalObserver: Observer<any>;
  public confirm: Observable<any>;
  private _confirmObserver: Observer<any>;
  public component: string = '';
  public value: any;
  public yesCallback: any;
  public noCallback: any;
  public message: string;

  constructor() {
    this.component = '';
    this.message = '';
    // Create Observable Stream to output our data
    this.modal = new Observable(observer => this._modalObserver = observer)
                    .startWith(this.component)
                    .share();
     this.confirm = new Observable(observer => this._confirmObserver = observer)
                    .startWith(this.message)
                    .share();                
  }

  trigger(component: string, value: any) {
    this.component = component || '';
    this.value = value || '';
    this._modalObserver.next(this.component);
  }

  triggerConfirm(message: string, yesCallback: any, noCallback: any) {
    this.message = message;
    this.yesCallback = yesCallback;
    this.noCallback = noCallback;
    this._confirmObserver.next(this.message);
  }  

}
