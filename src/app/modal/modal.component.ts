/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';

// Models

// Directives

// Services
import { ModalService } from './modal.service';

// Pipes

/*
 * Modal Component
 * 
 */
@Component({
  selector: 'modal-component',
  providers: [],
  styleUrls: ['./modal.component.scss'],
  template: `
      <div class="modal" [ngClass]="state + ' ' + level">
        <ng-content></ng-content>
        <div (click)="close()" class="modal-close-button"><p>+</p></div>
      </div> 
    <div class="modal-overlay {{level}}" [ngClass]="state" (click)="close()"></div>
  `
}) 

export class ModalComponent {

  @Input() level: string = '';
  @Input() instance: string;
  @Output() change = new EventEmitter(false);
  @Input() state: string = 'closed';

  constructor(
    public modalService: ModalService
  ) {

  }

  ngOnInit() {
    this.modalService.modal.subscribe(instance => { 
      if(instance == this.instance) {
        this.open();  
      }
    });
  }

  open() {
    this.state = 'open';
    this.change.emit(this.state);
  }

  close() {
    this.state = 'closed';
    this.change.emit(this.state);
  }

  toggle() {
    this.state = (this.state === 'closed') ? 'open' : 'closed';
    this.change.emit(this.state);
  }

}

