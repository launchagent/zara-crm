/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';

// Models

// Directives

// Services
import { ModalService } from './modal.service';

// Pipes

/*
 * Splash Overlay Component
 * 
 */
@Component({
  selector: 'splash-overlay',
  providers: [],
  styleUrls: ['./modal.component.scss'],
  template: `
    <div class="modal-overlay splash {{level}}" [ngClass]="state" (click)="close()"><ng-content></ng-content></div>
  `
}) 

export class SplashOverlayComponent {

  @Input() level: string = '';
  @Output() change = new EventEmitter();
  @Input() state: string = 'open';

  constructor(
    public modalService: ModalService
  ) {

  }

  ngOnInit() {

  }

  open() {
    this.state = 'open';
    this.change.emit(this.state);
  }

  close() {
    this.state = 'closed';
    this.change.emit(this.state);
  }

  toggle() {
    this.state = (this.state === 'closed') ? 'open' : 'closed';
    this.change.emit(this.state);
  }

}

