/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';

// Models

// Directives
import { ModalComponent } from './modal.component';

// Services
import { ModalService } from './modal.service';

// Pipes

/*
 * Message Component
 * 
 */
@Component({
  selector: 'confirm-modal',
  providers: [ ],
  styleUrls: ['./modal.component.scss'],
  template: `
      <div class="modal" [ngClass]="state + ' second'">
        <p>{{message}}</p>
        <button (click)="confirm()">Yes</button> <button (click)="cancel()">No</button>
        <div (click)="toggle()" class="modal-close-button"><p>+</p></div>
      </div> 
    <div class="modal-overlay second" [ngClass]="state" (click)="toggle()"></div>
  `
})

export class ConfirmComponent {

  @Input() message: string;
  @Output() change = new EventEmitter();
  public state: string = 'closed';

  constructor(
    public modalService: ModalService
  ) {
    this.message = '';
  }

  ngOnInit() {
    this.modalService.confirm.subscribe(message => { 
      if(message !== '') {
        this.message = message;
        this.toggle();  
      }
    });
  }

  confirm() {
    this.modalService.yesCallback();
    this.close();
  }

  cancel() {
    this.modalService.noCallback();
    this.close();
  }

  open() {
    this.state = 'open';
    this.change.emit(this.state);
  }

  close() {
    this.state = 'closed';
    this.change.emit(this.state);
  }

  toggle() {
    this.state = (this.state === 'closed') ? 'open' : 'closed';
    this.change.emit(this.state);
  }

}

