/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';

// Models

// Directives

// Services
import { ModalService } from './modal.service';

// Pipes

/*
 * Overlay Component
 * 
 */
@Component({
  selector: 'overlay-component',
  providers: [],
  styleUrls: ['./modal.component.scss'],
  template: `
    <div class="modal-overlay light {{level}}" [ngClass]="state" (click)="close()"><div class="spinner"></div></div>
  `
}) 

export class OverlayComponent {

  @Input() level: string = '';
  @Output() change = new EventEmitter();
  @Input() state: string = 'open';

  constructor(
    public modalService: ModalService
  ) {

  }

  ngOnInit() {

  }

  open() {
    this.state = 'open';
    this.change.emit(this.state);
  }

  close() {
    this.state = 'closed';
    this.change.emit(this.state);
  }

  toggle() {
    this.state = (this.state === 'closed') ? 'open' : 'closed';
    this.change.emit(this.state);
  }

}

