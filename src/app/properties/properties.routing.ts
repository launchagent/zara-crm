import { Routes, RouterModule }          from '@angular/router';
import { AuthGuard } from '../auth/auth-guard';
import { PropertiesComponent } from './properties.component';
import { PropertyDetailComponent } from './property-detail.component';
import { PropertiesListComponent } from './properties-list.component';

export const routes: Routes = [
  {
    path: 'properties',
    component: PropertiesComponent,
    canActivate: [AuthGuard],
    children: [
        { path: ':id', component: PropertyDetailComponent },
        { path: '', component: PropertiesListComponent }
    ]
  }
];

export const PropertiesRouting = RouterModule.forChild(routes);