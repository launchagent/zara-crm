/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services

// Pipes


/*
 * Properties Component
 */
@Component({

  selector: 'properties',
  providers: [  ],
  styleUrls: ['./properties.component.scss'],
  templateUrl: './properties.component.html'
})

export class PropertiesComponent {


  constructor(

  ) {

  }


}

