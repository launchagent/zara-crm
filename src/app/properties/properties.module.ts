import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertiesRouting } from './properties.routing';
import { SharedModule } from '../shared/shared.module';

// Directives
import { PropertiesComponent } from './properties.component';
import { PropertyDetailComponent } from './property-detail.component';
import { PropertyDetail } from './property-detail';
import { PropertyForm } from './property-form.component';
import { PropertiesListComponent } from './properties-list.component';
import { PropertyFeaturesComponent } from './property-features.component';
import { PropertyGallery } from './property-gallery.component';

// Pipes
import { PropertiesPipe } from './properties.pipe';

@NgModule({
    imports: [ CommonModule, SharedModule, PropertiesRouting ],
    declarations: [ 
        PropertiesComponent,
        PropertyDetailComponent,
        PropertyDetail,
        PropertyForm,
        PropertiesListComponent,
        PropertyGallery,
        PropertyFeaturesComponent,
        PropertiesPipe
    ],
    exports: [  
        PropertiesComponent,
        PropertyDetail,
        PropertyForm,
        PropertyGallery,
        PropertiesPipe
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PropertiesModule {
}