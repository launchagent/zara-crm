import { Pipe } from '@angular/core';

@Pipe({
  name: 'filterProperties'
})
export class PropertiesPipe {
  transform(value, [term, key]) {
    term = term || '';
    value = value || [];
    return value.filter((val) => val[key].toUpperCase().startsWith(term.toUpperCase()));
  }
}
