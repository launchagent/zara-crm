/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Property } from './property';

// Directives
import { PropertyDetail } from './property-detail';
import { FilesComponent } from '../files/files.component';

// Services
import { PeopleService } from '../people/people.service';
import { WorkflowService } from '../workflow/workflow.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Property Detail Component
 * 
 */
@Component({
  selector: 'property-detail-component',
  providers: [ WorkflowItemsService, Property ],
  styleUrls: ['./properties.component.scss'],
  template: `
  <a class="button left" (click)="goBack()">&laquo; back</a>
    <div class="small-12 columns content">
      <property-detail [property]="property" [id]="propertyID" [minimal]="false"></property-detail>
    </div>  
  `
})

export class PropertyDetailComponent {

  public propertyID: string;

  constructor(
      af:AngularFire,
      private _route: ActivatedRoute,
      private _location: Location,
      public property: Property
  ) {
    this.propertyID = this._route.snapshot.params['id'];
    if(this.propertyID) {
      this.property.load(this.propertyID);
    }
  }

  ngOnInit() {

  }

  goBack() {
    this._location.back();
  }

}

