/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import * as moment from 'moment';
import { Model } from '../model';

// Models
import { WorkflowItem } from '../workflow-items/workflow-item';

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ConfigService } from '../config.service';
import { HttpService } from '../http.service';
import { TrademeService } from '../integrations/trademe.service';
import { WebsiteService } from '../integrations/website.service';

/*
 * Property Model
 *
 */
@Injectable()

export class Property extends Model {

  public snapshot: propertyFields= {
      created: '',
      createdByID: '',
      workflowItems: {},
      attributes: {},
      features: {},
      name: '',
      featuredImages: {},
      galleryImages: {}
  };

  // Local only
  public regionIndex: number = 0;
  public cityIndex: number = 0;
  public suburbIndex: number = 0;
  public localities: any;
  private _workflowItem: WorkflowItem;

  constructor(
    _af: AngularFire,
    _auth: AuthService,    
    private _config: ConfigService,
    private _message: MessageService,
    private _http: HttpService,
    private _trademe: TrademeService,
    private _website: WebsiteService   
 
  ) {
    super(_af, _auth);
    if (!this._trademe.localities || this._trademe.localities.length == 0 ) {
      this._trademe.getLocalities(() => {
        this.localities = this._trademe.localities;
      });
    } else {
      this.localities = this._trademe.localities;
    }

  }

  /*
  * Run this once auth has completed
  */
  init() {
    this._ref = "/properties/" + this._auth.companyID;
    this.list = this._af.database.list(this._ref);
  }

  // create(object) {
  //   if(object.name === '') {
  //       this._message.set('You need a name for this property', 'error');
  //   } else {
  //     const promise = super.create(object);
  //     return promise;
  //   }
  // }

  addWorkflowItem(id: string) {
    return this.addTo('workflowItems', id);
  }  

  addFeature(name: string, qty: number) {
    return this.addTo('features', { name: name, qty: qty });
  } 

  removeFeature(ID: string, callback) {
    return this.removeFrom('features', ID);
  }   

  // Save featured images
  saveFeaturedImages(images) {
    this.update('featuredImages', images);
  }

  // Save gallery images
  saveGalleryImage(image) {
    var galleryImages = this._af.database.list(this._ref + '/' + this.ID + '/galleryImages');
    return galleryImages.push(image);
  }

  // Delete gallery image 
  deleteGalleryImage(image: Object) {
    var images = [image['large'], image['medium'], image['small']];    
    this._http.postPath(this._config.restAPI + '/files/delete', {type: 'images', files:  images }).subscribe((data: any) => {
      return this.removeFrom('galleryImages', image['$key']);
    });
  }

  // Add image to marketing gallery images
  addMarketingGalleryImage(imageID) {
    var galleryImage = this._af.database.object(this._ref + '/' + this.ID + '/galleryImages/' + imageID + '/');
    return galleryImage.update({marketing: true});
  }

  // Remove image from marketing gallery images 
  removeMarketingGalleryImage(imageID) {
    var galleryImage = this._af.database.object(this._ref + '/' + this.ID + '/galleryImages/' + imageID + '/');
    return galleryImage.update({marketing: false});
  }

  // Set locaton attributes on the property
  setLocationAttributes() {

    for (var i = 0; i < this.localities.length; ++i) {
      if(this.localities[i].LocalityId == parseInt(this.snapshot['attributes']['region-id'])) {
        this.snapshot['attributes']['region-name'] = this.localities[i].Name;
        for (var j = 0; j < this.localities[i].Districts.length; ++j) {
          if (this.localities[i].Districts[j].DistrictId == parseInt(this.snapshot['attributes']['city-id'])) {
            this.snapshot['attributes']['city-name'] = this.localities[i].Districts[j].Name;
            for (var k = 0; k < this.localities[i].Districts[j].Suburbs.length; ++k) {
              if (this.localities[i].Districts[j].Suburbs[k].SuburbId == parseInt(this.snapshot['attributes']['suburb-id'])) {
                this.snapshot['attributes']['suburb-name'] = this.localities[i].Districts[j].Suburbs[k].Name;
                return true;
              }  
            }
          }
        }
      }
    }       
  }

 // Add the Trade Me photo id to the featured images
  addFeaturedImageTrademeID(trademeID: string) {
    this._af.database.object(this._ref + '/' + this.ID + '/featuredImages/large/').update({'trademeID': trademeID}).then(() => {
      this._af.database.object(this._ref + '/' + this.ID + '/featuredImages/medium/').update({'trademeID': trademeID}).then(() => {
        return this._af.database.object(this._ref + '/' + this.ID + '/featuredImages/small/').update({'trademeID': trademeID});
      });       
    }); 
  }

  // Add the Trade Me photo id to a gallery image
  addGalleryImageTrademeID(ID: string, trademeID: string) {
    var galleryImage = this._af.database.object(this._ref + '/' + this.ID + '/galleryImages/' + ID).update({'trademeID': trademeID});
    return galleryImage;
  }

  // Set the order of a gallery image
  setGalleryImageOrder(ID: string, order: number) {
    var galleryImage = this._af.database.object(this._ref + '/' + this.ID + '/galleryImages/' + ID).update({'order': order});
    return galleryImage;
  }
  
}

export interface propertyFields {
  created: string,
  createdByID: string,
  workflowItems: any,
  attributes: Object,
  features: Object,
  name: string,
  featuredImages: Object,
  galleryImages: any;
}

