/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Property } from './property';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { ModalService } from '../modal/modal.service';
import { MessageService } from '../message/message.service';
import { PropertyFeaturesService } from './property-features.service';

// Pipes

/*
 * Property Features Component
 * 
 */
@Component({

  selector: 'property-features',
  providers: [ PropertyFeaturesService, Property ],
  styles: [''],
  template: `
  <div class="small-12 columns content">
    <ul *ngIf="property && features">
    <li *ngFor="let feature of features | async">{{feature.name}} <a (click)="delete(feature.$key)">[x]</a></li>
    </ul>
    <div class="row">
      <div class="small-7 columns">
        <select [(ngModel)]="selected">
          <option *ngFor="let feature of propertyFeatures.values" [value]="feature">{{feature}}</option>
        </select>
      </div>
      <div class="small-2 columns">
        Qty: <input type="number" [(ngModel)]="qty" min="1" max="10" step="1"> 
      </div>
      <div class="small-5 columns"><a class="button small" (click)="add()">Add feature</a></div>
   </div>
  </div>  
  `
})

export class PropertyFeaturesComponent {

  @Input() property: Property;
  public features: any;
  public selected: string;  
  public qty: number = 1;

  constructor(
    private _confirm: ModalService,
    private _message: MessageService,
    private _auth: AuthService,
    public propertyFeatures: PropertyFeaturesService,
    public af: AngularFire
  ) {

  }

  ngOnInit() {
    if(this.property) {
      this.features = this.af.database.list('/properties/' + this._auth.companyID + '/' + this.property.ID + '/features');
    }
  }

  add() {
    if(this.selected != '') {
      this.property.addFeature(this.selected, this.qty).subscribe(() => {
        this._message.set('Feature added', 'success');
        this.selected = '';
        this.qty = 1;
      });      
    } else {
      this._message.set('You need to select a feature to add', 'warning');
    }

  }

  delete(ID) {
    this._confirm.triggerConfirm('Are you sure you want to remove this feature?', () => {
      this.property.removeFeature(ID, () => {
        this._message.set('Feature removed', 'success');
      });
    }, () => {});
  }


}

