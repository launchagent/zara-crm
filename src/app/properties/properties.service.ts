/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { AuthService } from '../auth/auth.service';

/*
 * Properties Service
 * 
 */
@Injectable()

export class PropertiesService {

  public list: FirebaseListObservable<any>;

  constructor(
      private _af: AngularFire,
      private _auth: AuthService
  ) {
    this.list = this._af.database.list("/properties/" + this._auth.companyID);

  }

}

