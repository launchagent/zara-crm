/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';

/*
 * Property Features Service
 * 
 */
@Injectable()

export class PropertyFeaturesService {

  public values: Array<any>;

  constructor(

  ) {
    this.setup();
  }

  setup() {
    this.values = [
      '',
      'Air Conditioning Unit',
      'BBQ Area', 
      'Double Glazing',
      'Fibre Optic Broadband',                 
      'Gym',
      'Heat Pump',
      'Intercom',   
      'Lift',   
      'Sauna',      
      'Spa Pool',            
      'Swimming Pool',
      'Vacuum Ducted',
      'Walk In Wardrobe'
    ];
  }

}

