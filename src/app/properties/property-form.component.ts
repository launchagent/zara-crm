/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Event } from '../events/event';
import { Property } from './property';

// Directives
import { AttributesComponent } from '../attributes/attributes.component';
import { AutocompleteProperty } from '../autocomplete/autocomplete-property.component';
import { PropertyFeaturesComponent } from './property-features.component';

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { PropertyAttributeService } from '../attributes/property-attribute.service';

// Pipes
 
/*
 * Property Form Component
 * 
 */
@Component({
  selector: 'property-form',
  providers: [ Event, Property ],
  styleUrls: ['./properties.component.scss'],
  template: `
      <div>
      <form (ngSubmit)="onSubmit()" #propertyForm="ngForm">
        <label for="property-name">Property Name</label>
        <input name="property-name" type="text" placeholder="property name" [(ngModel)]="property.snapshot.name" (keyup)="autocompleteProperty.open()" (blur)="autocompleteProperty.close()" #nameInput autocomplete="off" >
        <autocomplete-property #autocompleteProperty [key]="nameInput.value" [values]="properties | async" (update)="load($event.$key)"></autocomplete-property>
        <property-features class="property-features" *ngIf="!minimal" [(property)]="property"></property-features>
        <attributes *ngIf="!minimal" [(model)]="property" [attributes]="attributes" [namespace]="'properties'"></attributes>
        <file-uploader *ngIf="showUploader" [ID]="property.ID" [type]="'property'" (update)="saveFeaturedImages($event)"></file-uploader>
        <button type="submit" class="button" [disabled]="!propertyForm.form.valid">Save property</button>
      </form>
      </div>
  `
})

export class PropertyForm {
  @Input() minimal: boolean = false;
  @Input() property: any;
  @Input() stage: string; 
  @Input() showUploader: boolean = false;
  @Input() withAutocomplete: boolean = false;
  @Output() update = new EventEmitter();
  @Output() stageFields = new EventEmitter();
  @Output() requiredStageFields = new EventEmitter();

  public active: boolean;
  public attributes: Array<any>;
  public totalFields: number = 0;
  public totalInvalidFields: number = 0;
  public properties: Observable<any[]>;

  constructor(
    private _auth: AuthService,
      private _message: MessageService,
      private _property: Property,
      public event: Event,
      public attributeService: PropertyAttributeService,
      af: AngularFire
  ) {
    this.active = true;
    this.attributes = this.attributeService.attributes;
    this.stage = 'prospect';
    this.properties = this._property.list;
  }

  ngOnInit() {
    // Assign blank property object if no property supplied
    if(!this.property || typeof this.property === 'undefined') {
      this.property = this._property;
    } 
  }

  ngOnChanges() {
    if(!this.minimal) {
      this.validateForStage();       
    }     
  }

  onSubmit() {
    this.property.setLocationAttributes();  
    
    var eventDescription = (this.property.ID === '') ? 'property created' : 'property updated';
    if(this.property.ID) {
      this.property.update('name', this.property.snapshot['name']);
      this.property.saveAttributes(() => {
        this.update.emit(this.property);
        this._message.set('Property saved.');
          if(!this.minimal) {
            this.validateForStage();          
          }
      });

    } else {
      this.property.create(this.property.snapshot).then(() => {
        this._message.set('property saved', 'success');
        this.event.create({ createdByID: this._auth.uid, propertyID: this.property.ID, description: eventDescription }).then(() => {
          this.update.emit(this.property);
          if(!this.minimal) {
            this.validateForStage();          
          }
        });
      });
    }

  }

  clear() {

  }

  load(ID: string) {
    if(ID && ID != '') {
      this.property.load(ID, () => {

      });      
    } 
  }

  outputTotals() {
    this.stageFields.emit(this.totalFields);
    this.requiredStageFields.emit(this.totalInvalidFields);
  }

  validateForStage() {
    this.totalFields = 0;
    this.totalInvalidFields = 0;    
    for (var i = 0; i < this.attributes.length; ++i) {
      var field = this.attributes[i];
      if (field.workflows[0].stage === this.stage) {
        this.totalFields++;
      }
      
      if (field.workflows[0].stage === this.stage && (typeof this.property.snapshot.attributes[field.slug] === 'undefined' || this.property.snapshot.attributes[field.slug] == '')) {
        this.totalInvalidFields++;
        this.attributes[i].validForStage = 'stage-required';
      } else {
        this.attributes[i].validForStage = '';
      }
    }
    this.outputTotals();

  }

  saveFeaturedImages(images) {
    var imageData = { 'large': images[0].large, 'medium': images[1].medium, 'small': images[2].small };
    this.property.saveFeaturedImages(imageData, () => {
      
    });
  }

}

