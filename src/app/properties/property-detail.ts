/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Property } from './property';

// Directives

// Services
import { ConfigService } from '../config.service';
import { AuthService } from '../auth/auth.service';
import { HttpService } from '../http.service';
import { PeopleService } from '../people/people.service';
import { PropertyAttributeService } from '../attributes/property-attribute.service';
import { WorkflowService } from '../workflow/workflow.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Property Detail
 * 
 */
@Component({

  selector: 'property-detail',
  providers: [ ConfigService, HttpService, WorkflowItemsService, Property ],
  styleUrls: ['./properties.component.scss'],
  templateUrl: './property-detail.component.html'
})

export class PropertyDetail {

  @Input() property: Property;
  @Input() id: string;
  @Input() minimal: boolean = true;

  public attributes: Array<any>;
  public address: string; 

  constructor(
      private _af: AngularFire,
      private _http: HttpService,
      private _auth: AuthService,
      public config: ConfigService,
      public people: PeopleService,
      public propertyAttributes: PropertyAttributeService,
      public workflows: WorkflowService,
      public workflowItems: WorkflowItemsService
  ) {
    this.attributes = propertyAttributes.attributes;
  }

  ngOnInit() {

  }

  ngOnChanges() {

  }

}


