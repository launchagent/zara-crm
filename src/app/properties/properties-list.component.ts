/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Property } from './property';
import { Person } from '../people/person';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { ConfigService } from '../config.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';
import { PeopleService } from '../people/people.service';
import { WorkflowService } from '../workflow/workflow.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes
import { PropertiesPipe } from './properties.pipe';
import { SearchPipe } from '../search/search.pipe';

/*
 * Properties Component
 */
@Component({

  selector: 'properties',
  providers: [ ConfigService, WorkflowItemsService, Person ],
  styleUrls: ['./properties.component.scss'],
  templateUrl: './properties-list.component.html'
})

export class PropertiesListComponent {

  public properties:Observable<any[]>
  public propertyWorkflows: Object;
  public ref: any;
  public show: Array<boolean> = [];

  constructor(
    private _af: AngularFire,
    private _auth: AuthService,
    private _person: Person,
    private _message: MessageService,
    public config: ConfigService,
    public af: AngularFire,
    public property: Property, 
    public modal: ModalService,
    public people: PeopleService,
    public workflows: WorkflowService,
    public workflowItems: WorkflowItemsService
  ) {
    this._auth.isLoggedIn.subscribe((val) => {
      if(val) {
        this.setupList();
      }
    }); 
  }

  ngOnInit() {
    if(this._auth.companyID) {
      this.setupList();
    }
  }

  setupList() {
    this.properties = this._af.database.list("/properties/" + this._auth.companyID);
  }

  getFirst(obj) {
    if(obj) {
      return obj[Object.keys(obj)[0]];      
    }
  }

  getVendors(workflowItems) {
    var vendors = [];
    var vendorIDS = this.workflowItems.getVendors(this.getFirst(workflowItems));
    for (var prop in vendorIDS) {
      vendors.push({ 'ID': vendorIDS[prop], 'value': this.people.values[vendorIDS[prop]]});
    }
    return vendors;
  }

  getAgents(workflowItems) {
    var agents = [];
    var agentIDS = this.workflowItems.getAgents(this.getFirst(workflowItems));
    for (var prop in agentIDS) {
      agents.push({ 'ID': agentIDS[prop], 'value': this.people.values[agentIDS[prop]]});
    }
    return agents;
  }

  duplicate(ID, name) {
    let duplicated = false;
    this.property.load(ID).subscribe(() => {
      if(!duplicated) {
        this.property.duplicate('name', name);
        duplicated = true;
        this._message.set('Property duplicated', 'success');
      }
    });
  }

}

