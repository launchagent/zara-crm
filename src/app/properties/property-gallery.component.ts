/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Property } from './property';

// Directives

// Services
import { ConfigService } from '../config.service';
import { AuthService } from '../auth/auth.service';
import { HttpService } from '../http.service';
import { PeopleService } from '../people/people.service';
import { WorkflowService } from '../workflow/workflow.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Property Detail
 * 
 */
@Component({
  selector: 'property-gallery',
  providers: [ ConfigService, HttpService, WorkflowItemsService, Property ],
  styleUrls: ['./properties.component.scss'],
  template: `
<div class="row">
  <div class="small-12 medium-6 columns">
  <h3>Featured Image</h3>
    <img *ngIf="property.featuredImages && property.featuredImages.medium" [src]="config.imageServer + property.featuredImages.medium.src">
    <a (click)="toggleEditFeature()">Edit</a>
    <div *ngIf="showEditFeature" class="feature-edit">
      <file-uploader [ID]="property.ID" [type]="'property'" (update)="saveFeaturedImages($event.files)"></file-uploader>  
    </div>
  </div>
  <div class="small-12 medium-6 columns gallery">
    <h3>Gallery</h3>
    <div class="wrapper">
      <ul class="row small-up-2 medium-up-3 large-up-4 container">
        <li *ngFor="let image of gallery | async" class="column"><img [src]="config.imageServer + image.medium" alt=""> <a (click)="deleteGalleryImage(image)">x</a> <a (click)="addMarketingGalleryImage(image.$key)">marketing</a></li>
      </ul>      
    </div>
    <a (click)="addAllMarketingGallery()">Add all to marketing gallery</a><br>
    <a (click)="toggleEditGallery()">+ Add new</a>
    <div *ngIf="showEditGallery" class="gallery-edit">
      <file-uploader [ID]="property.ID" [type]="'gallery'" [index]="galleryCount" (update)="addGalleryImages($event.files)" [multi]="true"></file-uploader>  
    </div>
    <h3>Marketing Gallery</h3>
    <small>Images will be uploaded to web site and integrations</small>
    <div class="wrapper">
      <ul class="row small-up-2 medium-up-3 large-up-4 container">
        <li *ngFor="let image of marketingGallery | async | sort" class="column">
          <img [src]="config.imageServer + image.medium" alt="">
          <img *ngIf="image.trademeID != ''" src="/img/trademe.png" width="15px" height="15px">
          <input type="number" [(ngModel)]="image.order" (change)="setImageOrder(image)">
          <a (click)="removeMarketingGalleryImage(image.$key)">remove</a>
        </li>
      </ul>      
    </div>
  </div>
</div>
  `
})

export class PropertyGallery {

  @Input() property: Property;
  @Input() id: string;
  @Output() update = new EventEmitter();

  public gallery: Observable<any>; 
  public marketingGallery: Observable<any>;
  public galleryCount: number = 1;
  public address: string; 
  public galleryValues: Array<any>;
  public showEditFeature: boolean = false;
  public showEditGallery: boolean = false;

  constructor(
      private _af: AngularFire,
      private _http: HttpService,
      private _auth: AuthService,
      public config: ConfigService,
      public workflows: WorkflowService,
      public workflowItems: WorkflowItemsService
  ) {

  }

  ngOnChanges() {      
      this.gallery = this._af.database.list('properties/' + this._auth.companyID + '/' + this.id + '/galleryImages');
      this.marketingGallery = this._af.database.list('properties/' + this._auth.companyID + '/' + this.id + '/galleryImages', {
        query: {
          orderByChild: 'marketing',
          equalTo: true
        }
      });
      this.gallery.subscribe((values) => {
        this.galleryCount = values.length+1;
        this.galleryValues = values;
      });       
  }

  toggleEditGallery() {
    this.showEditGallery = (this.showEditGallery) ? false : true;
  }

  toggleEditFeature() {
    this.showEditFeature = (this.showEditFeature) ? false : true;
  }

  saveFeaturedImages(images) {
    var imageData = { 
      'large': { 'src': images[0].large, 'trademeID': '' }, 
      'medium': { 'src': images[1].medium, 'trademeID': '' }, 
      'small': { 'src': images[2].small, 'trademeID': '' }
    };
    this.property.saveFeaturedImages(imageData);
  }

  addGalleryImages(images) {
    var numImages = images.length / 3;    
    for (var i = 0; i < numImages; ++i) {
      var a = (i === 0) ? 0 : i * 3;
      var imageData = {
        'trademeID': '',
        'order': this.galleryCount+i,
        'large': images[a].large, 
        'medium': images[a+1].medium, 
        'small': images[a+2].small
      };

      this.property.saveGalleryImage(imageData);      
    }
  }

  deleteGalleryImage(image) {
    this.property.deleteGalleryImage(image);
  }

  addMarketingGalleryImage(imageID) {
    this.property.addMarketingGalleryImage(imageID);
  }

  addAllMarketingGallery() {

    for (var i in this.galleryValues) {
      this.property.addMarketingGalleryImage(this.galleryValues[i].$key);      
    }
  }

  removeMarketingGalleryImage(imageID) {
    this.property.removeMarketingGalleryImage(imageID);
  }

  setImageOrder(image) {
    this.property.setGalleryImageOrder(image.$key, image.order);
  }

}


