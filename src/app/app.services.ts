
import { AuthService }           from './auth/auth.service';
import { WorkflowService }       from './workflow/workflow.service';
import { WorkflowItemsService }  from './workflow-items/workflow-items.service';
import { PeopleService }         from './people/people.service';
import { PropertiesService }     from './properties/properties.service';
import { ModalService }          from './modal/modal.service';
import { MessageService }        from './message/message.service';
import { PropertyAttributeService } from './attributes/property-attribute.service';
import { ConfigService }         from './config.service';
import { HttpService }           from './http.service';
import { TrademeService }        from './integrations/trademe.service';
import { RealestateService }     from './integrations/realestate.service';
import { WebsiteService }        from './integrations/website.service';

export const appServices: Array<any> = [
    ConfigService,
    PeopleService,
    PropertiesService,    
    MessageService,
    ModalService,
    AuthService,
    HttpService,
    TrademeService,
    RealestateService,
    WebsiteService,
    PropertyAttributeService,
    WorkflowService,
    WorkflowItemsService 
  ];

