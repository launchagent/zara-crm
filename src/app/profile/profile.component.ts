/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { File } from '../files/file';
import { Person } from '../people/person';

// Directives
import { FilesComponent } from '../files/files.component';
import { AttributesComponent } from '../attributes/attributes.component';

// Services
import { ConfigService } from '../config.service';
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { PersonAttributeService } from '../attributes/person-attribute.service';

// Pipes

/*
 * Profile Component
 * 
 */
@Component({
  selector: 'profile',
  providers: [ File, Person, ConfigService, PersonAttributeService ],
  styleUrls: ['./profile.component.scss'],
  templateUrl: './profile.component.html'
})

export class ProfileComponent {

  public attributes: any;

  constructor(
    private _af: AngularFire,
    private _router: Router,
    private _message: MessageService,
    private _file: File,
    public config: ConfigService,
    public auth: AuthService,
    public person: Person,
    public personAttributes: PersonAttributeService
  ) {
    this.attributes = this.personAttributes.attributes;
  }

  ngOnInit() {
    this.person.load(this.auth.uid);
  }

  setImages(images) {
    var imageData = { 'large': images[0].large, 'medium': images[1].medium, 'small': images[2].small };
    this.person.saveProfileImages(imageData);
    
  }

  saveAgentFile(files) {
    for (var i = 0; i < files.length; ++i) {
      if (files[i].id) {
        this._file.init(files[i].id);
        this._file.snapshot.itemID = files[i].id;
        this._file.snapshot.name = files[i].name;
        this._file.snapshot.extension = files[i].extension;
        this._file.create(this._file.snapshot).then((id) => {
          this.person.saveAgentFile(id).subscribe(() => {
            this._message.set('File saved', 'success');
          });
        });
      }
     
    }
  }

  removeAgentFile(file) {
    this.person.removeAgentFile(() => {
      
    });
  }

  onSubmit() {
    // this.person.update().subscribe(() => {
    //   this._message.set('Profile saved', 'success');
    // });
  }

 

}

