/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

// Pipes

/*
 * Profile Menu Component
 * 
 */
@Component({
  selector: 'profile-menu',
  providers: [  ],
  styleUrls: ['./profile.component.scss'],
  template: `
      <div class="profile-menu" [ngClass]="'profile-menu-' + state">
        <ul>
          <li><a [routerLink]=" ['/profile'] ">My Profile</a></li>
          <li><a [routerLink]=" ['/admin'] ">Admin</a></li>
          <li *ngIf="isLoggedIn"><a (click)="logout()">Log Out</a></li>
          <li *ngIf="!isLoggedIn"><a [routerLink]=" ['/login'] ">Log In</a></li>
        </ul>
      </div>
      <div class="profile-menu-overlay" (click)="toggle()" [ngClass]="state"></div>
  `
})

export class ProfileMenuComponent {

  public state: string = 'closed';
  public isLoggedIn: boolean = false;

  constructor(
    private _af: AngularFire,
    private _router: Router,
    private _auth: AuthService,
    private _message: MessageService
  ) {

  }

  ngOnInit() {
    this._auth.isLoggedIn.subscribe((val) => {
      this.isLoggedIn = val;
    });
  }


  logout() {
    localStorage.removeItem('uid');
    this._af.auth.logout();
    this._auth.uid = null;
    this._auth.companyID = null;
    this._router.navigate(['login']);
    this._message.set("You have been logged out", 'success');
  }

  toggle() {
    this.state = (this.state === 'open') ? 'closed' : 'open';
  }


}

