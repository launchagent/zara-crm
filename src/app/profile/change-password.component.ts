/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services
import { MessageService } from '../message/message.service';

// Pipes


/*
 * Change Password Component
 * 
 */
@Component({
  selector: 'ChangePassword',
  providers: [ ],
  styleUrls: ['./profile.component.scss'],
  template: `
      <a class="button left" (click)="goBack()">&laquo; back</a><br>
      <div class="row content">
      <div class="small-12 columns">
        <h1>Change your password</h1>  
        <form (ngSubmit)="changePassword()" #ChangePasswordForm="ngForm" >
          <div><p>Enter your email, password and a new password</p></div>
          <input type="email" placeholder="Email" [(ngModel)]="email" ngControl="email" required>
          <input type="password" placeholder="current password" [(ngModel)]="password" ngControl="password" required>
          <input type="password" placeholder="new password" [(ngModel)]="newPassword" required>  
          <button type="submit" class="button" [disabled]="!ChangePasswordForm.form.valid">ChangePassword</button>
        </form>
        </div>
      </div>
  `
})

export class ChangePasswordComponent {

  public email: string;
  public password: string;
  public newPassword: string;

  constructor(
    private _af: AngularFire,
    private _router: Router,
    private _message: MessageService,
    private _location: Location
  ) {

  }

  ngOnInit() {

  }

  clear() {
    this.email = '';
    this.password = '';
    this.newPassword = '';
  }

  goBack() {
    this._location.back();
  }


  changePassword() {
    // this._af.database.changePassword({
    //   email: this.email,
    //   oldPassword: this.password,
    //   newPassword: this.newPassword
    // }, (error) => {
    // if (error) {
    //   switch (error.code) {
    //     case "INVALID_PASSWORD":
    //       this._message.set("The specified user account password is incorrect.", 'error');
    //       break;
    //     case "INVALID_USER":
    //       this._message.set("The specified user account does not exist.", 'error');
    //       break;
    //     default:
    //       this._message.set("Error changing password:" + error, 'error');
    //   }
    // } else {
    //     this.clear();
    //     this._message.set("User password changed successfully!", 'success');
    //     this._router.navigate(['Profile']);
    //   }
    // });      

  }


}

