import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';

// Directives
import { OfferForm } from './offers-form.component';
import { OffersComponent } from './offers.component';

@NgModule({
    imports: [ RouterModule, CommonModule, SharedModule ],
    declarations: [ 
        OffersComponent,
        OfferForm
    ],
    exports: [ 
        OffersComponent,
        OfferForm
     ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class OffersModule {
}