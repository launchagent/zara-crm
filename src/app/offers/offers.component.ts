/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Material components

// Models
import { Person } from '../people/person';
import { Offer } from './offer';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives


// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';
import { PeopleService } from '../people/people.service';

// Pipes

/*
 * Offers Component
 * 
 */
@Component({
  selector: 'offers.component',
  providers: [ Offer, WorkflowItem, Person ],
  styleUrls: ['./offers.component.scss'],
  template: `
    <ul class="offers-list"> 
      <li *ngFor="let item of offers | async" class="row">
        <div class="small-2 columns">{{item.created | todate | date:'yMMMd'}}</div>  
        <div class="small-2 columns">{{item.purchasePrice}}</div>  
        <div class="small-4 columns"><a (mouseover)="showDetails(item)" (mouseout)="hideDetails(item)">{{people.values[item.buyerID].name}}</a></div>
        <div class="small-2 columns"><a class="" (click)="acceptOffer(item)" *ngIf="item.status != 'accepted'">Accept</a></div>
        <div class="small-2 columns"><a class="delete tiny" (click)="delete(item.$key)">X</a></div>
        <div class="details-callout" *ngIf="item.showDetails">
          <div class="row">
          <h5>Offer Details</h5>
          <div class="small-12 columns">
            <p>Type: {{item.type}} {{item.status}}</p>
            <p>Placed by {{people.values[item.buyerID].name}} on {{item.date | todate | date:'yMMMd'}} </p>
          </div>
          <div class="small-12 columns">
            <p>Purchase price: $ {{item.purchasePrice}} 
              <span *ngIf="item.purchaseIncludesGST == 'yes'">Including</span>
              <span *ngIf="item.purchaseIncludesGST == 'no'">Excluding</span> GST</p>
            <p>Desposit: $ {{item.deposit}} due on 
              <span *ngIf="item.depositDue == 'acceptance'">acceptance</span>
              <span *ngIf="item.depositDue == 'unconditional'">unconditional</span></p>
            <p>Interest rate: {{item.interestRate}}%</p>
          </div>
          <div class="small-12 columns">
            <p>Settlement date: {{item.settlementDate | todate | date:'yMMMd'}}</p>
            <p>Unconditional date: {{item.unconditionalDate | todate | date:'yMMMd'}}</p>
          </div> 
        </div>
        </div>
      </li>
    </ul>     

  `
})

export class OffersComponent {
  @Input() workflowItem: WorkflowItem;
  @Input() showAccepted: boolean = false;

  public offers: Observable<any[]>;
  public buyers: Observable<any[]>;

  public startDate: any;
  public startTime: any;
  public endDate: any;
  public endTime: any;
  public types: Array<string>;
  

  constructor(
    private _auth: AuthService,
    private _message: MessageService,
    private _confirm: ModalService,
    public offer: Offer,
    public af: AngularFire,
    public people: PeopleService,
    public person: Person
  ) {

  }

  ngOnInit() {
    
  }

  ngOnChanges() {
    if(this.workflowItem) {
      var root = '/workflow-items/' + this._auth.companyID + '/' + this.workflowItem.snapshot['$key'];
      this.offers = (this.showAccepted) ? this.af.database.list(root + '/offers', {
        query: {
          'orderByChild': 'status',
          'equalTo': 'accepted'
        }
      }) : this.af.database.list(root + '/offers');
      this.buyers = this.af.database.list(root + '/buyers');
    }
  }

  acceptOffer(offer) {
    // this._confirm.triggerConfirm('Are you sure you want to accept this offer?', () => {
    //   this.workflowItem.acceptOffer(offer, () => {
    //     this.workflowItem.setRequirement('market', 1, '', () => {
    //       this._message.set('Offer accepted', 'success');
    //     });
    //   });
    // }, () => { });    
  }

  delete(ID) {
    // this._confirm.triggerConfirm('Are you sure you want to delete this offer?', () => {
    //   this.workflowItem.removeOffer(ID, () => {
    //     this._message.set('Offer deleted', 'success');
    //   });
    // }, () => {});
  }

  showDetails(offer) {
    offer.showDetails = true;
    return offer;
  }

  hideDetails(offer) {
    offer.showDetails = false;
    return offer;
  }

}

