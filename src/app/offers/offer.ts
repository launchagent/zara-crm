/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Model } from '../model';

// Services
import { AuthService } from '../auth/auth.service';

/*
 * Offer Model
 * 
 */
@Injectable()

export class Offer extends Model {

    public fields: offerFields;  

  constructor(
    _auth: AuthService,
    _af: AngularFire

  ) {
    super(_af, _auth);
  }

}

export interface offerFields {
  created: string,
  createdByID: string,      
  date: string,
  type: string,
  deposit: number,
  depositDue: string,
  purchaseIncludesGST: string,
  settlementDate: string,
  unconditionalDate: string,
  purchasePrice: number,
  interestRate: number,
  status: string,
  buyerID: string  
}

