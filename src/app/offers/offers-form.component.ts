/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Person } from '../people/person';
import { Offer } from './offer';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives


// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';
import { PeopleService } from '../people/people.service';

// Pipes

/*
 * Offer Form Component
 * 
 */
@Component({
  selector: 'offer-form',
  providers: [ Offer, WorkflowItem, Person ],
  styles: [''],
  template: `
<div *ngIf="workflowItem.stage != 'sold'">
  <form (ngSubmit)="add()" >
    <h2>Add an offer</h2>
    <h5>Offer details</h5>
    <div class="row">
      <div class="small-4 columns">
        <label>Offer Signed:</label>
        <input type="date" [(ngModel)]="offer.fields.date" placeholder="offer signed">  
      </div>
      <div class="small-4 columns">
        <label>Buyer:</label>
        <select [(ngModel)]="offer.fields.buyerID">
          <option *ngFor="let buyer of buyers | async" [value]="buyer.$value">{{people.values[buyer.personID].name}}</option>
        </select>
      </div>
      <div class="small-4 columns">
        <label>Offer type:</label>
        <select [(ngModel)]="offer.fields.type">
          <option *ngFor="let type of types" [value]="type">{{type}}</option>
        </select>         
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
        <label>Expected unconditional date:</label>
        <input type="date" [(ngModel)]="offer.fields.unconditionalDate">  
      </div>
      <div class="small-12 columns">
        <label>Settlement Date:</label>
        <input type="date" [(ngModel)]="offer.fields.settlementDate">
      </div>
      <div class="small-12 columns">        
      </div>
    </div>
    <h5>Purchase price</h5>
    <div class="row">
      <div class="small-4 columns">
        <input type="number" [(ngModel)]="offer.fields.purchasePrice" placeholder="purchase price" min="0.00" max="9999999999" step="50.00">   
      </div>
      <div class="small-4 columns">
        
      </div>
      <div class="small-4 columns">
        <md-radio-group [(ngModel)]="offer.fields.purchaseIncudesGST">
          <md-radio-button [value]="'yes'">Inclusive</md-radio-button>
          <md-radio-button [value]="'no'">Exclusive</md-radio-button>
        </md-radio-group>        
      </div>
    </div>
    <h5>Deposit</h5>
    <div class="row">
      <div class="small-4 columns">
        <input type="number" [(ngModel)]="offer.fields.deposit" placeholder="deposit" min="0.00" max="9999999999" step="50.00">  
      </div>
      <div class="small-4 columns">
          <input type="number" [(ngModel)]="offer.fields.interestRate" placeholder="interest rate" min="0.00" max="100.00" step="0.00">% 
      </div>
      <div class="small-4 columns">
        <md-radio-group [(ngModel)]="offer.fields.depositDue">
        <md-radio-button [value]="'acceptance'">On Acceptance</md-radio-button>
        <md-radio-button [value]="'unconditional'">On Unconditional</md-radio-button>
      </md-radio-group>
      </div>    
    </div>
    <button type="submit" class="button">Add offer</button>
  </form>
</div>  

  `
})

export class OfferForm {
  @Input() workflowItem: WorkflowItem;
  public offers: Observable<any[]>;
  public buyers: Observable<any[]>;

  public startDate: any;
  public startTime: any;
  public endDate: any;
  public endTime: any;
  public types: Array<string>;
  

  constructor(
    private _auth: AuthService,
    private _message: MessageService,
    private _confirm: ModalService,
    public offer: Offer,
    public af: AngularFire,
    public people: PeopleService,
    public person: Person
  ) {

    this.types = ['Primary', 'Backup'];
  }

  ngOnInit() {
 
  }

  ngOnChanges() {
    if(this.workflowItem) {
      this.buyers = this.af.database.list('/workflow-items/' + this._auth.companyID + '/' + this.workflowItem.ID + '/buyers');
    }
  }


  add() {
    this.workflowItem.addOffer(this.offer.fields).then(() => {
      this._message.set('Offer added', 'success');
    });
  }


}

