/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Observable } from 'rxjs/Observable';

// Models
import { Person } from './person';

// Directives


// Services
import { PeopleService } from './people.service';
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

// Pipes

/*
 * Solicitor Component
 * 
 */
@Component({
  selector: 'solicitor.component',
  providers: [ Person ],
  styleUrls: ['./people.component.scss'],
  template: `
    <div>
      <div *ngIf="person.solicitorID != ''">
        <div class="row">
          <div class="small-12 columns">
            <h5>{{people.values[person.solicitorID].name}} <a class="small" (click)="removeSolicitor()">change</a></h5>
          </div>
        </div>    
        <div class="row">
          <div class="small-12 columns">
            {{people.values[person.solicitorID].attributes['firm']}}
          </div>
        </div>
        <div class="row">
          <div class="small-12 columns">
            <a [href]="people.values[person.solicitorID].attributes['website']">{{people.values[person.solicitorID].attributes['website']}}</a>
          </div>
        </div>
        <div class="row">
          <div class="small-12 columns">
            <a [href]="'tel:' + people.values[person.solicitorID].attributes['work-phone']">{{people.values[person.solicitorID].attributes['work-phone']}}</a>
           </div>
        </div>
        <div class="row">
          <div class="small-12 columns">   
            <a [href]="'tel:' + people.values[person.solicitorID].attributes['mobile-phone']">{{people.values[person.solicitorID].attributes['mobile-phone']}}</a>
          </div>
        </div>  
      </div>
      <div *ngIf="person.solicitorID == ''">
        <input type="text" placeholder="Add Solicitor" [(ngModel)]="solicitor.name" (keyup)="autocompletePerson.open()" (blur)="autocompletePerson.close();" required>
        <autocomplete-person #autocompletePerson [key]="solicitor.name" [values]="solicitorsList | async" (update)="solicitor.name = $event.name; addSolicitor($event.$key); autocompletePerson.close();" ></autocomplete-person>
      </div>

    </div>
  `
})

export class SolicitorComponent {

  @Input() person: Person;
  @Output() update = new EventEmitter();
  public solicitorsList: Observable<any>;

  constructor(
    private _person: Person,
    private _auth: AuthService,
    private _message: MessageService,
    public people: PeopleService,
    public af: AngularFire,
    public solicitor: Person
  ) {

  }

  ngOnInit() {
    this.solicitorsList = this.af.database.list("/people/" + this._auth.companyID, {
      query: {
       'orderByChild': 'solicitor',
       'equalTo': true  
      }
    });

  }

  ngOnChanges() {

  }

  addSolicitor(id) {
    if(id && id != '') {  
      this.person.addSolicitorID(id).subscribe(() => {
        this._message.set('Solicitor added', 'success');
        this.update.emit(id);
      });      
    }
  }

  removeSolicitor() {
    this.person.object['solicitorID'] = '';
    this.update.emit('');
  }

}

