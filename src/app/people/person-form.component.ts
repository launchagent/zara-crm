/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Event } from '../events/event';
import { Person } from './person';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { PersonAttributeService } from '../attributes/person-attribute.service';

// Pipes
 
/*
 * Person Form Component
 * 
 */
@Component({
  selector: 'person-form',
  providers: [ Event, Person, PersonAttributeService ],
  styleUrls: ['./people.component.scss'],
  template: `
      <div>
      <h2 class="add-header"><span *ngIf="!person.ID">Add New</span><span *ngIf="person.ID">Edit {{person.snapshot.name}}</span></h2>
      <form (ngSubmit)="onSubmit()" #personForm="ngForm" >
        <input type="text" placeholder="person" #name="ngModel" [(ngModel)]="person.snapshot.name" (keyup)="autocompletePerson.open()" (blur)="autocompletePerson.close()" name="name" #nameInput autocomplete="off" >
        <autocomplete-person #autocompletePerson [key]="nameInput.value" [values]="people | async" (update)="load($event.$key)"></autocomplete-person>
        <div [hidden]="name.valid || name.pristine" class="alert alert-danger">
          Name is required
        </div>
        <input type="email" placeholder="Email" [(ngModel)]="person.snapshot.email" name="email" #email="ngModel" required>
        <div [hidden]="email.valid || email.pristine" class="alert alert-danger">
          Email is required
        </div>
        <attributes *ngIf="!minimal" [(model)]="person" [attributes]="attributes" [namespace]="'general'"></attributes>
        <button type="submit" class="button" [disabled]="!personForm.form.valid">Save person</button>
      </form>
      </div>
  `
})

export class PersonForm {
  @Input() person: Person; 
  @Input() stage: string = ''; 
  @Input() minimal: boolean = false;
  @Input() mode: string = 'create';
  @Output() update = new EventEmitter();

  public people: Observable<any[]>;
  public attributes: Array<any>;
  public active: boolean;

  constructor(
      private _auth: AuthService,
      private _message: MessageService,
      private _person: Person,
      public event: Event,
      public attributeService: PersonAttributeService,
      af: AngularFire
  ) {
    this.attributes = this.attributeService.attributes;
    this.people = af.database.list("/people/" + this._auth.companyID);
  }


  ngOnInit() {
    if (!this.person || typeof this.person === 'undefined') {
      this.person = this._person;
    }
  }

  onSubmit() {

    var eventDescription = (this.person.ID === '') ? 'person created' : 'person updated';
    this.person.create(this.person.snapshot).then(() => {
      this._message.set('Person saved', 'success');
      this.event.create({ createdByID: this._auth.uid, personID: this.person.ID, description: eventDescription }).then(() => {
        this.update.emit(this.person);
      });
    });
  }

  clear() {

  }

  load(ID: string) {
    if(ID && ID != '') {
      this.person.load(ID);
    } 
  }


}

