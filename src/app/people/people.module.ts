import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleRouting } from './people.routing';
import { SharedModule } from '../shared/shared.module';

// Directives
import { PeopleComponent } from './people.component';
import { PeopleListComponent } from './people-list.component';
import { PersonDetailComponent } from './person-detail.component';
import { PersonForm } from './person-form.component';
import { SolicitorComponent } from './solicitor.component';

@NgModule({
    imports: [ CommonModule, SharedModule, PeopleRouting ],
    declarations: [ 
        PeopleComponent,
        PersonDetailComponent,
        PersonForm,
        SolicitorComponent,
        PeopleListComponent
    ],
    exports: [  
        PeopleComponent,
        PersonDetailComponent,
        SolicitorComponent,
        PersonForm,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PeopleModule {
}