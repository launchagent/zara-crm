import { Routes, RouterModule }          from '@angular/router';
import { AuthGuard } from '../auth/auth-guard';
import { PeopleComponent } from './people.component';
import { PeopleListComponent } from './people-list.component';
import { PersonDetailComponent } from './person-detail.component';

export const routes: Routes = [
  {
    path: 'people',
    component: PeopleComponent,
    canActivate: [AuthGuard],
    children: [
        { path: ':id', component: PersonDetailComponent },
        { path: '', component: PeopleListComponent }
    ]
  }
];

export const PeopleRouting = RouterModule.forChild(routes);