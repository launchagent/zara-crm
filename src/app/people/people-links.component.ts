/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Event } from '../events/event';
import { Person } from './person';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

// Pipes
 
/*
 * People List Component
 * 
 */
@Component({
  selector: 'people-list',
  providers: [ Event, Person ],
  styleUrls: ['./people.component.scss'],
  template: `
    <ul class="no-bullet">
      <!--<li *ngFor="let person of people | async"><a [routerLink]=" ['/people', person.$key] ">{{person.name}}</a></li>-->
    </ul>  
  `
})

export class PeopleLinksComponent {

  @Input() workflowID: string;
  @Input() type: string; 

  public people: Observable<any[]>;
  public active: boolean;

  constructor(
      af: AngularFire,
      private _message: MessageService,
      public event: Event,
      public person: Person
  ) {
    this.people = af.database.list('/people');
  }


  ngOnInit() {

  }


}

