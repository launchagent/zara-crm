/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services

// Pipes

/*
 * People Component
 * 
 */
@Component({

  selector: 'people',
  providers: [  ],
  styleUrls: ['./people.component.scss'],
  templateUrl: './people.component.html'
})

export class PeopleComponent {

  constructor(

  ) {
        
  }

}

