/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Event } from '../events/event';
import { Person } from './person';
import { Setting } from '../settings/setting';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { ConfigService } from '../config.service';
import { MessageService } from '../message/message.service';
import { PersonAttributeService } from '../attributes/person-attribute.service';
 
// Pipes
 
/*
 * Person Detail Component
 * 
 */
@Component({
  selector: 'person-detail',
  providers: [ Event, Person, Setting, PersonAttributeService ],
  styleUrls: ['./people.component.scss'],
  templateUrl: './person-detail.component.html'
})

export class PersonDetailComponent {
  @Output() update = new EventEmitter();

  public questions: Observable<any[]>;
  public active: boolean;
  public attributes: Array<any>;

  constructor(
      private _route: ActivatedRoute,
      private _auth: AuthService,
      private _message: MessageService,
      private _location: Location,
      private _personAttributes: PersonAttributeService,
      public config: ConfigService,
      public event: Event,
      public person: Person,
      public setting: Setting,
      public workflowItem: WorkflowItem
  ) {
    this.person.load(this._route.snapshot.params['id']).subscribe(() => {
      this.attributes = this._personAttributes.attributes;
    });
    
  }


  ngOnInit() {

  }

  goBack() {
    this._location.back();
  }

  setType(type: string) {
    this.person.addType(type);
  }

  setProfileImage(images) {
    var imageData = { 'large': images[0].large, 'medium': images[1].medium, 'small': images[2].small };
    this.person.saveProfileImages(imageData);
  }

  createProspect() {
    this.workflowItem.update('stage', 'prospect');
    this.workflowItem.addTo('agents', this._auth.uid);
    this.workflowItem.addTo('vendors', this.person);
    this.person.addTo('workfow-items', this.workflowItem.ID).subscribe(() => {
        this._message.set('workflow item saved', 'success');
    });              

  }


}

