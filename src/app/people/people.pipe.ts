import { Pipe } from '@angular/core';

@Pipe({
  name: 'filterpeople'
})
export class PeoplePipe {
  transform(value, type) {

    type = type || 'all';
    value = value || [];
    if(type == 'all') {
		return value;
    } else {
    	return value.filter((val) => val[type] == true);		
    }
    
  }
}
