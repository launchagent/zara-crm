/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Person } from './person';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { ModalService } from '../modal/modal.service';

// Pipes
import { PeoplePipe } from './people.pipe';

/*
 * People Component
 * 
 */
@Component({
  selector: 'people',
  providers: [ Person ],
  styleUrls: ['./people.component.scss'],
  templateUrl: './people-list.component.html'
})

export class PeopleListComponent {

  public people: FirebaseListObservable<any[]>;

  constructor(
      private _af: AngularFire,
      private _auth: AuthService,
      private _person: Person,
      public modal: ModalService
  ) {
    this._auth.isLoggedIn.subscribe((val) => {
      if(val) {
        this.setupList();
      }
    });         
  }

  ngOnInit() {
    if(this._auth.companyID) {
      this.setupList();
    }
  }

  setupList() {
    this.people = this._af.database.list("/people/" + this._auth.companyID);
  }

}

