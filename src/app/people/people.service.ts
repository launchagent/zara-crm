/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { AuthService } from '../auth/auth.service';

/*
 * People Service
 * 
 */
@Injectable()

export class PeopleService {

  private _ref: string;
  private _list: FirebaseListObservable<any>;
  private _object: FirebaseObjectObservable<any>;

  public values: Object;

  constructor(
      private _af: AngularFire,
      private _auth: AuthService
  ) {
    this._ref = "/people/" + this._auth.companyID;
    this._list = this._af.database.list(this._ref, { preserveSnapshot: true });
  }

}

