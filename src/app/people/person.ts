/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import * as moment from 'moment';
import { Model } from '../model';

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { WebsiteService } from '../integrations/website.service';
import { TrademeService } from '../integrations/trademe.service';


export interface personFields {
  created: string,
  createdByID: string,
  companyID: string,
  name: string,
  email: string,
  profileImages: Object,
  workflowItems?: any,
  attributes: Object,
  solicitorID: string,
  admin: boolean,
  agent: boolean,
  buyer: boolean,
  vendor: boolean,
  solicitor: boolean,
  websiteID: string,
  userLevel: number 
}

/*
 * Person Model
 * 
 */
@Injectable()

export class Person extends Model {

  public snapshot: personFields = {
    created: '',
    createdByID: '',
    companyID: '',
    name: '',
    email: '',
    profileImages: {},
    attributes: {'mobile-phone': '', 'work-phone': ''},
    solicitorID: '',
    admin: false,
    agent: false,
    buyer: false,
    vendor: false,
    solicitor: false,
    websiteID: '',
    userLevel: 0     
  };

  constructor(
      _af: AngularFire,
      _auth: AuthService,
      private _message: MessageService,
      private _website: WebsiteService,
      private _trademe: TrademeService
  ) {
    super(_af, _auth);
  }

  /*
  * Run this once auth has completed
  */
  init() {
    this._ref = "/people/" + this._auth.companyID;
    this.list = this._af.database.list(this._ref);
  }

  /*
  * See if a person with email already exists
  */
  exists(email) {
    var existsQuery = this._af.database.list(this._ref, { 
      query: { orderByChild: 'email', equalTo: email},
      preserveSnapshot: true
    });
    return existsQuery;
  }

  // Check if person has a type
  isType(type: string) {
    var existsQuery = this._af.database.list(this._ref, { 
      query: { orderByChild: type, equalTo: true},
      preserveSnapshot: true
    });
    return existsQuery; 
  }

  // Add person type 
  addType(type: string) {
    return this.update(type, true);
  }  

  // Save profile images
  saveProfileImages(images: Object) {
    return this.update('profileImages', images);
  }

  // Save the trademe branding image id to the person
  saveTrademeImageID(ID: string) {
    return this.setAttribute('trademe-image-id', ID);
  }

  // Save agent file
  saveAgentFile(file: string) {
    return this.setAttribute('agent-file', file);
  }

  // Remove agent file
  removeAgentFile(callback) {
    return this.removeAttribute('agent-file');
  }

  // Add website ID to person
  addWebsiteID(id: string) {
    return this.update('websiteID', id);
  } 

  // Add solicitor ID to person - used for attaching a solicitor to a buyer or vendor 
  addSolicitorID(id: string) {
    return this.update('solicitorID', id);
  } 


}


