/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Model } from '../model';

// Services
import { AuthService } from '../auth/auth.service';

/*
 * Event Model
 * 
 */
@Injectable()
export class Event extends Model {

  public snapshot: eventFields = {
    created: '',
    description: '',
    createdByID: '',
    officeID: '',
    personID: '',
    propertyID: '',
    workflowID: '',
    todoID: ''    
  };

  constructor(
      _af: AngularFire,
      _auth: AuthService
  ) {
    super(_af, _auth);
    this._ref = "/events/" + this._auth.companyID;
    this.list = this._af.database.list(this._ref);
  }

}

export interface eventFields {
  created: string,
  description: string,
  createdByID: string,
  officeID: string,
  personID: string,
  propertyID: string,
  workflowID: string,
  todoID: string
}

