/*
 * Angular 2 decorators and services
 */
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models

// Directives

// Services

// Pipes

/*
 * Events Component
 * 
 */
@Component({
  selector: 'events',
  styles: [''],
  template: `

  `
})

export class EventsComponent {

  public questions:Observable<any[]>

  constructor(
      af:AngularFire
  ) {
    this.questions = af.database.list('/questions');
  }

  ngOnInit() {

  }

}

