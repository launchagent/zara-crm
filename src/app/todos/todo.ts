/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Model } from '../model';

// Models
import { Event } from '../events/event';
import { Person } from '../people/person';

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

/*
 * Todo Model
 * 
 */
@Injectable()

export class Todo extends Model {

  public fields: todoFields;  

  constructor(
      _af: AngularFire,
      _auth: AuthService,
      private _message: MessageService,
      private _event: Event

  ) {
    super(_af, _auth);
    this._ref = "/todos/" + this._auth.companyID;
    this.list = this._af.database.list(this._ref);
  }


  // save(callback) {
  //   if(this.ID === '') {      
  //     this.created = moment().format('YYYY-MM-DD H:mm:ss');
  //     var newRef = this._ref.child(this.assignedToID).push(this.get(), () => {
  //       this._ref.child(newRef.key()).once('value', (snapshot) => {
  //         // Create the event
  //         this._event.save({ createdByID: this._auth.uid, todoID: snapshot.key(), description: 'todo created' }, () => {
  //           callback({ key: snapshot.key(), value: snapshot.val() });
  //         });
  //       });
  //     });    
  //   } else {
  //     this.update(callback);
  //   }
  // }

  // update(callback) {
  //   this._ref.child(this.assignedToID).child(this.ID).child('description').set(this.description, () => { });
  //   //this._ref.child(this.ID).child('description').set(this.description, () => { });
  // }

  // load(id, callback) {
  //   this._ref.child(this._auth.uid).child(id).once('value', (snapshot) => {
  //     var data = snapshot.val();
  //     data.key = snapshot.key();
  //     this.set(data);
  //     callback({ key: snapshot.key() , value: data });  
  //   });   
  // }

  // complete() {
  //   this._ref.child(this.assignedToID).child(this.ID).child('completedByID').set(this._auth.uid, () => { 
  //     this._ref.child(this.assignedToID).child(this.ID).child('completed').set(moment().format('YYYY-MM-DD H:mm:ss'), () => {
  //       this._message.set('todo completed', 'success');
  //     });
  //   });
  // }

  // assign(person: Person) {
  //   var self = this;
  //   var oldRef = self._ref.child(self.assignedToID).child(self.ID).once('value', function(snapshot)  {
  //     var newRef = self._ref.child(person.ID).child(self.ID).set( snapshot.val(), function(error) {
  //        if( !error ) {  
  //         self._ref.child(self.assignedToID).child(self.ID).remove(); 
  //         self._ref.child(person.ID).child(self.ID).child('assignedToID').set(person.ID, () => {
  //           self._message.set('todo assigned to ' + person.name, 'success');
  //         }); 
  //        }
  //       else { self._message.set('error assigning todo to ' + person.name, 'error'); }
  //     });
  //   });

  // }

}

export interface todoFields {
  created: string,
  createdByID: string,  
  description: string,
  completedByID: string,
  completed: string,
  due: string,
  assignedToID: string,
  worklowItemID: string,
  personID: string,
  propertyID: string
}

