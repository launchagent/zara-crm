/*
 * Angular 2 decorators and services
 */
import { Component, Inject, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

// Models
import { Todo } from './todo';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

// Pipes

/*
 * Todo Form Component
 * 
 */
@Component({

  selector: 'todo-form',
  providers: [ Todo ],
  styleUrls: ['./todos.component.scss'],
  template: `
  <form (ngSubmit)="onSubmit()" >
    <input type="text" placeholder="task" [(ngModel)]="todo.description" name="description" required>
    <label>Due date:</label>
    <input type="date" [(ngModel)]="todo.due" name="due" required>
    <button type="submit" class="button">Add Todo</button>
  </form>
  `
})

export class TodoForm {

  @Input() personID = '';
  @Input() propertyID = '';
  @Input() workflowItemID = '';
  public todos: Observable<any[]>;

  constructor(
      private _auth: AuthService,
      private _message: MessageService,
      public todo: Todo
  ) {

  }

  ngOnInit() {

  }

  onSubmit() {
    this.todo.object['createdByID'] = this._auth.uid;
    this.todo.object['assignedToID'] = this._auth.uid;
    this.todo.object['personID'] = this.personID;
    this.todo.object['propertyID'] = this.propertyID;
    this.todo.object['worklowItemID'] = this.workflowItemID;
    // this.todo.create().subscribe(() => {
    //   this._message.set('Todo added', 'success');
    //   this.todo.clear();
    // });
  }

}

