/*
 * Angular 2 decorators and services
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Todo } from './todo';
import { Person } from '../people/person';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';

// Pipes

/*
 * Todos Component
 * 
 */
@Component({

  selector: 'todos',
  providers: [ Todo, Person ],
  styleUrls: ['./todos.component.scss'],
  templateUrl: './todos.component.html'
})

export class TodosComponent {

  public todos: Observable<any[]>;
  public people: Observable<any[]>;
  public selected: string;
  public showAssignPanel: boolean = false;

  constructor(
    private _auth: AuthService,
      private _message: MessageService,
      public todo: Todo,
      public assignee: Person,
      public af: AngularFire
  ) {
    this.people = this.af.database.list("/people/" + this._auth.companyID);
    this.todos = this.af.database.list("/todos/" + this._auth.companyID + '/' + this._auth.uid, {
      query: {
        orderByChild: 'due'
      }
    });
  }

  ngOnInit() {

  }

  complete(id) {
    // this.todo.load(id, () => {
    //   this.todo.complete();
    // });
  }

  toggleAssignPanel(id) {
    this.selected = id;
    this.showAssignPanel = (this.showAssignPanel) ? false : true;
  }

  assign() {
    // this.todo.load(this.selected, () => {
    //   this.todo.assign(this.assignee);
    // });
  }

  load(ID: string) {
    // if(ID && ID != '') {
    //   this.assignee.load(ID, () => {

    //   });      
    // } 
  }

}

