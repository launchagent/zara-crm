import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class ConfigService {

	public environment: string = 'dev';
	public domain: string;
	public api: string;
	public restAPI: string;
  	public imageServer: string;
  	public imageUploadServer: string;
	public fileServer: string;
	public websiteAPI: string;


constructor() {
	this.domain = environment.domain;
	this.api = '/api/v1';
	this.restAPI = this.domain + this.api;
    this.imageServer = this.domain + '/images/';
	this.imageUploadServer = this.domain + this.api + '/upload/';
	this.fileServer = this.domain + '/files/';
	this.websiteAPI = this.domain + this.api + '/website/';
	}

}