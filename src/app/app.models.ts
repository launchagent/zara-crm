import { Setting } from './settings/setting';
import { Event, eventFields } from './events/event';
import { File } from './files/file';
import { Person } from './people/person';
import { Property } from './properties/property';
import { WorkflowItem } from './workflow-items/workflow-item';

export const appModels: Array<any> = [
    Setting,
    Event,
    File,
    Person,
    Property,
    WorkflowItem,
  ];

