
import { Injectable}     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable}     from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ConfigService } from './config.service';
import { MessageService } from './message/message.service';

@Injectable()
export class HttpService {

  constructor (
    private http: Http, 
    private _config: ConfigService, 
    private _message: MessageService 
  ) {
  }

  getPath (path: string): Observable<any[]> {
    return this.http.get(path)
    .map(this.extractData).catch(this.handleError);
  }

  postPath (path: string, data: Object): Observable<any>  {
    let body = JSON.stringify({ data });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(path, body, options)
    .map(this.extractData).catch(this.handleError);
  }

  postFormPath (path: string, data: Object): Observable<any>  {
    let body = JSON.stringify({ data });
    let headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(path, body, options)
    .map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }
    let body = res.json();
    return body || { };
  }

  private handleError (error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = error.message || 'Server error';
    this._message.set('Oops there was an error connecting to an outside service', 'error');
    return Observable.throw(errMsg);
  }
}


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/