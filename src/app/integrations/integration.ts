
import { Injectable, Inject}     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable}  from 'rxjs/Observable';

// Models
import { File } from '../files/file';
import { Person } from '../people/person';
import { Property } from '../properties/property';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Services
import { ConfigService } from '../config.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';


@Injectable()

export class Integration {

  public htmlTagRegex: RegExp = /<\/?[\w\s="/.':;#-\/\?]+>/gi;

  constructor(
    public config: ConfigService,
    public file: File
  ) {

  }

  mapAgent(agent: Person) {
    var agentData = {
      name: agent.snapshot['name'],
      email: agent.snapshot['email'],
      phone: agent.snapshot['attributes']['work-phone'] || '',
      agentFile: '',
      areinz: agent.snapshot['attributes']['arienz'] || '',
      jobTitle: agent.snapshot['attributes']['job-title'] || '',
      youtubeVideoID: agent.snapshot['attributes']['youtube-video-id'] || '',
      mobile: agent.snapshot['attributes']['mobile-phone'] || '',
      description: agent.snapshot['attributes']['description'] || '',
      profileImages: []
    };

    for (var i in agent.snapshot['profileImages']) {
      agentData.profileImages.push(encodeURI(this.config.imageServer + agent.snapshot['profileImages'][i]));
    }

    if(agent.snapshot['attributes']['agent-file']) {
      this.file.init(agent.snapshot['$key']);
      this.file.load(agent.snapshot['attributes']['agent-file']).subscribe((value) => {
        agentData.agentFile = this.config.fileServer + agent.snapshot['$key'] + '/' + value.name;
      });
    }

    //URL encode and strip tags from all the properties
   
    for (var l in agentData) {
      if(typeof agentData[l] === 'string') {
        agentData[l] = agentData[l].replace(/"/gi, "#quote#");  
      }
      agentData[l] = encodeURI(agentData[l]);
      agentData[l] = agentData[l].replace(this.htmlTagRegex, "");
    }
   
    return agentData;

  }

  mapListing(workflowItem: WorkflowItem) {

    var propertyData = {
      'title': workflowItem.snapshot['attributes']['advertising-title'] || workflowItem.property.snapshot['name'],
      'description': workflowItem.snapshot['attributes']['advertising-copy'] || '',
      'agentIDs': '',
      'featuredImages': [],
      'trademePhotoIDs': [],
      'trademeDuration': workflowItem.snapshot['attributes']['trademe-duration'] || '',
      'price': workflowItem.snapshot['attributes']['price'] || '',
      'priceDate': workflowItem.snapshot['attributes']['auction-date'] || '',
      'expectedSalePrice':workflowItem.snapshot['attributes']['expected-sale-price'] || '',
      'cv': workflowItem.snapshot['attributes']['cv'] || 0,
      'bedrooms': workflowItem.property.snapshot['attributes']['bedrooms'] || 1,
      'bathrooms': workflowItem.property.snapshot['attributes']['bathrooms'] || 1,
      'ensuites': workflowItem.property.snapshot['attributes']['ensuites'] || 0,
      'livingrooms': workflowItem.property.snapshot['attributes']['livingrooms'] || 0,
      'diningrooms': workflowItem.property.snapshot['attributes']['diningrooms'] || 0,
      'saleType': workflowItem.snapshot['attributes']['sales-method'] || '',
      'listingType': workflowItem.property.snapshot['attributes']['listing-type'] || '',
      'propertyType': workflowItem.property.snapshot['attributes']['property-type'] || '',
      'garages': workflowItem.property.snapshot['attributes']['garages'] || '',
      'carports': workflowItem.property.snapshot['attributes']['carports'] || '',
      'offStreet': workflowItem.property.snapshot['attributes']['off-street'] || '',
      'landArea': workflowItem.property.snapshot['attributes']['land-area'] || '',
      'floorArea': workflowItem.property.snapshot['attributes']['floor-area'] || '',
      'smokeAlarm': workflowItem.property.snapshot['attributes']['smoke-alarm'] || '',
      'localAmenities': workflowItem.snapshot['attributes']['local-amenities'] || '',
      'listingID': workflowItem.snapshot['listingID'],
      'openHomes': [],
      'contacts': [],
      'streetName': workflowItem.property.snapshot['attributes']['street-name'] || '',
      'streetNumber': workflowItem.property.snapshot['attributes']['street-number'] || '',
      'unitNumber': workflowItem.property.snapshot['attributes']['unit-number'] || '',
      'address': '',
      'latitude': workflowItem.property.snapshot['attributes']['latitude'] || '',
      'longitude': workflowItem.property.snapshot['attributes']['longitude'] || '',
      'region': workflowItem.property.snapshot['attributes']['region-name'] || '',
      'regionID': workflowItem.property.snapshot['attributes']['region-id'] || '',
      'city': workflowItem.property.snapshot['attributes']['city-name'] || '',
      'cityID': workflowItem.property.snapshot['attributes']['city-id'] || '',
      'suburb': workflowItem.property.snapshot['attributes']['suburb-name'] || '',
      'suburbID': workflowItem.property.snapshot['attributes']['suburb-id'] || '',
      'websiteID': workflowItem.snapshot['websiteID'] || '',
      'trademeListingID': workflowItem.snapshot['trademeListingID'] || '',
      'trademeCategoryID': workflowItem.snapshot['attributes']['trademe-category-id'] || '',
      'endDate': workflowItem.snapshot['attributes']['trademe-end-date'] || '',
      'realestateListingID': workflowItem.snapshot['realestateListingID'] || '',
      'propertyFile': workflowItem.propertyFileUrl,
      'features': '',
      'youtubeVideoID': workflowItem.snapshot['attributes']['youtube-video-id'] || '',
      'galleryImages': [],
      'stage': workflowItem.snapshot['stage'],
      'status': workflowItem.snapshot['status'],
      'websiteFeatured': workflowItem.snapshot['attributes']['website-featured'] || ''     
    };

      // Create address string for the website
      if(propertyData.unitNumber != '') {
        propertyData.address = propertyData.unitNumber + '-' + propertyData.streetNumber + ' ' + propertyData.streetName;
      } else {
        propertyData.address = propertyData.streetNumber + ' ' + propertyData.streetName;
      }
      
      // Filter featured images src for website
      for (var i in workflowItem.property.snapshot['featuredImages']) {
        propertyData.featuredImages.push( this.config.imageServer + workflowItem.property.snapshot['featuredImages'][i].src );          
      } 

      // Add property features 
      var features = [];
      for (var j in workflowItem.property.snapshot['features']) {
        var feature = workflowItem.property.snapshot['features'][j].name + ':' + workflowItem.property.snapshot['features'][j].qty;
        features.push(feature);
      }
      propertyData.features = features.join();

      // Sort the gallery images
      var galleryImages = [];
      for (var m in workflowItem.property.snapshot['galleryImages']) {
        galleryImages.push(workflowItem.property.snapshot['galleryImages'][m]);
      } 
      galleryImages.sort((a: any, b: any) => {
          if (a.order < b.order) {
            return -1;
          } else if (a.order > b.order) {
            return 1;
          } else {
            return 0;
          }
      });

      // Add gallery images trade me ids
      for (var k in galleryImages) {
        if (galleryImages[k]['marketing'] == true) {
          if (galleryImages[k]['trademeID'] != '') {
            propertyData.trademePhotoIDs.push(galleryImages[k]['trademeID']);
          }
          // Add the image paths to the gallery images array
          propertyData.galleryImages.push( this.config.imageServer + galleryImages[k]['large']); 
        }  
      }

      //URL encode all the properties
      for (var l in propertyData) {
        if(l != 'openHomes' && l != 'contacts') {
          if(typeof propertyData[l] === 'string') {
            propertyData[l] = propertyData[l].replace(/"/gi, "#quote#");  
          }
          propertyData[l] = encodeURI(propertyData[l]);  
          propertyData[l] = propertyData[l].replace(this.htmlTagRegex, "");
        }
      }

      return propertyData;

  }


  getFirst(obj) {
    if (obj) {
      return obj[Object.keys(obj)[0]];      
    }
  }

}  