
import { Injectable}     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';


// Models
import { Integration } from './integration';
import { File } from '../files/file';
import { Person } from '../people/person';
import { Property } from '../properties/property';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Services
import { ConfigService } from '../config.service';
import { HttpService } from '../http.service';

@Injectable()
export class RealestateService extends Integration {

  public listingTypes: Array<any>;
  public saleTypes: Array<any>;
  public pricingCodes: Array<any>;

  constructor(private _http: HttpService, config: ConfigService, file: File) {
    super(config, file);

    this.listingTypes = [
    {
      name: 'Residential / Lifestyle / Rental', id: 1,
      classes: [
        { name: 'Apartment', id: 1 },
        { name: 'House', id: 2 },
        { name: 'Home & Income', id: 3 },
        { name: 'Lifestyle Property', id: 4 },
        { name: 'Unit', id: 5 },
        { name: 'Townhouse', id: 6 },
        { name: 'Section - Residential', id: 7 },
        { name: 'Section - Lifestyle', id: 8 },
        { name: 'Studio', id: 9 }
      ]
    },
    {
      name: 'Commercial / Business', id: 2,
      classes: [
        { name: 'Industrial to Buy', id: 1 },
        { name: 'Retail to Buy', id: 2 },
        { name: 'Office to Buy', id: 3 },
        { name: 'Hotel/Motel/Leisure to Buy', id: 4 },
        { name: 'Industrial to Lease', id: 5 },
        { name: 'Retail to Lease', id: 6 },
        { name: 'Office to Lease', id: 7 },
        { name: 'Hotel/Motel/Leisure to Lease', id: 8 },
        { name: 'Investment Flats', id: 10 },
        { name: 'Commercial Land', id: 11 },
        { name: 'Industrial Land', id: 12 },
        { name: 'Accommodation', id: 21 },
        { name: 'Contractor', id: 22 },
        { name: 'Distribution', id: 23 },
        { name: 'Franchise', id: 24 },
        { name: 'Hospitality', id: 25 },
        { name: 'Manufacturing', id: 26 },
        { name: 'Professional', id: 27 },
        { name: 'Restaurant', id: 28 },
        { name: 'Rest Home', id: 29 },
        { name: 'Retail Food', id: 30 },
        { name: 'Retail General', id: 31 },
        { name: 'Services', id: 32 },
        { name: 'Sporting', id: 33 },
        { name: 'Tourism', id: 34 },
        { name: 'Transport', id: 35 },
        { name: 'Wholesale', id: 36 }
      ]
    },
    {
      name: 'Rural', id: 3,
      classes: [
        { name: 'Lifestyle Property', id: 1 },
        { name: 'Cropping', id: 2 },
        { name: 'Dairy', id: 3 },
        { name: 'Finishing', id: 4 },
        { name: 'Forestry', id: 5 },
        { name: 'Grazing', id: 6 },
        { name: 'Horticultural', id: 7 },
        { name: 'High Country', id: 8 },
        { name: 'Specialist Livestock', id: 9 },
        { name: 'Bare Land', id: 10 },
        { name: 'Lifestyle Section', id: 11 }
      ]
    }
    ];

    this.saleTypes = [
      { name: 'Sole', id: 1 },
      { name: 'General', id: 3 },
      { name: 'Joint Agency', id: 8 }
    ];

    this.pricingCodes = [
      { name: 'Fixed Price', id: 1 },
      { name: 'POA', id: 2 },
      { name: 'By Negotiation', id: 3 },
      { name: 'Offers', id: 4 },
      { name: 'Auction', id: 5 },
      { name: 'Tender', id: 6 },
      { name: 'Deadline Treaty', id: 7 },
      { name: 'Asking Price', id: 8 },
      { name: 'Buyer Budget From', id: 9 },
      { name: 'Buyer Budget Over', id: 10 },
      { name: 'Buyer Enquiry Over', id: 11 },
      { name: 'Buyer Interest From', id: 12 },
      { name: 'Enquiries Over', id: 13 },
      { name: 'Expressions of Interest Over', id: 14 },
      { name: 'Negotiable From', id: 15 },
      { name: 'Offers Over', id: 16 }
    ];    

  }

  getRegions() {
    return this._http.getPath(this.config.restAPI + '/realestate/regions');
  }

  getDistricts(region) {
    return this._http.getPath(this.config.restAPI + '/realestate/districts/' + region);
  }

  getSuburbs(region, district) {
    return this._http.getPath(this.config.restAPI + '/realestate/suburbs/' + region + '/' + district);
  }

  mapTradeMePricingType(type) {
    var id = 0;
    for (var index = 0; index < this.pricingCodes.length; index++) {
      var element = this.pricingCodes[index];
      if(element.name.toUpperCase() == type.toUpperCase()) {
        id = element.id;
      }
    }
    return id;
  }

}  