
import { Injectable}     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';


// Models
import { Integration } from './integration';
import { File } from '../files/file';
import { Person } from '../people/person';
import { Property } from '../properties/property';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Services
import { ConfigService } from '../config.service';
import { MessageService } from '../message/message.service';
import { HttpService } from '../http.service';

@Injectable()
export class TrademeService extends Integration {

  private _url: string;
  public localities: Array<any> = [];
  public isSet: boolean = false; 
  public categories: Array<any>; 

  constructor(private _http: HttpService, private _message: MessageService, config: ConfigService, file: File) {
    super(config, file);
    this.getLocalities(() => {});

    this.categories = [
      { name: 'Residential - Carparks', id: 8945 },
      { name: 'Residential - Sections For Sale', id: 3400 },
      { name: 'Residential - For Sale', id: 3399 },
      { name: 'Residential - Lifestyle Property', id: 9120 },
      { name: 'Residential - To Rent', id: 4233 },
      { name: 'Commercial - Carparks', id: 8946 },
      { name: 'Commercial - For Lease', id: 5747 },
      { name: 'Commercial - For Sale', id: 5746 },
      { name: 'Rural', id: 5745 }
    ];
  }

  ngOnInit() {
    
  }

  getListingData(workflowItem: WorkflowItem) {
    var propertyData = this.mapListing(workflowItem);
    // Add open homes
    for (var l in workflowItem.snapshot['openHomes']) {
      propertyData.openHomes.push({ 'start': workflowItem.snapshot['openHomes'][l]['start'], 'end': workflowItem.snapshot['openHomes'][l]['end'] });
    }

    // Add agent contact details
    for (let agent of workflowItem.agents) {
        propertyData.contacts.push({ 'name': agent.name, 'email': agent.email, 'phone': agent['attributes']['work-phone'] || '', 'mobile': agent['attributes']['mobile-phone'] || '', 'agentID': agent['attributes']['reaa-number'] || '', 'brandingImageID': agent['attributes']['trademe-image-id'] || '' });
    } 
    
    return propertyData;   
  }

  addListing(workflowItem: WorkflowItem) {

    var propertyData = this.getListingData(workflowItem);

    this._http.postPath(this.config.restAPI + '/trademe/listing', propertyData).subscribe((data: any) => {
      var res = (typeof data.response == 'object') ? data.response : JSON.parse(data.response);
      if (res.ListingId && res.ListingId != '') {
        workflowItem.addTrademeListingID(res.ListingId).subscribe(() => {
          this._message.set('Listing created on Trade Me', 'success');
        });
      } else {
        var err = res.ErrorDescription || res.Description;
        this._message.set('There was a problem creating the listing on the Trade Me: ' + err, 'error');
      }  
    }); 
  }

  editListing(workflowItem: WorkflowItem) {

    var propertyData = this.getListingData(workflowItem);

    this._http.postPath(this.config.restAPI + '/trademe/listing/edit', propertyData).subscribe((data: any) => {
      var res = (typeof data.response == 'object') ? data.response : JSON.parse(data.response);
      if (res && res.Success == 'true') {
          this._message.set('Listing updated on Trade Me', 'success');
      } else {
        var err = res.Description || res.ErrorDescription;
        this._message.set('There was a problem updating the listing on the Trade Me: ' + err, 'error');
      }
    });
  }

  removeListing(workflowItem: WorkflowItem) {
    var withdrawData = {
      'trademeListingID': workflowItem.snapshot['trademeListingID'],
      'reason': 'Sale ended'
    };

    this._http.postPath(this.config.restAPI + '/trademe/listing/withdraw', withdrawData).subscribe((data: any) => {
      var res = (typeof data.response == 'object') ? data.response : JSON.parse(data.response);
      console.log(res);
      if (res.Success && res.Success == 'true') {
        workflowItem.removeTrademeListingID().subscribe(() => {
          this._message.set('Listing withdrawn from Trade Me', 'success');
        });
      } else {
        this._message.set('There was a problem withdrawing the listing from Trade Me', 'error');
      }
    });
  }

  getAddListingFees(workflowItem: WorkflowItem, callback) {
    var propertyData = this.mapListing(workflowItem);

    this._http.postPath(this.config.restAPI + '/trademe/listing/fees', propertyData).subscribe((data: any) => {
      // var res = (typeof data.response == 'object') ? data.response : JSON.parse(data.response);
      // callback(res);

    }); 
  }

  // Upload branding image photo to trade me and save the id to the image once done
  addAgentPhoto(agent: Person) {
    if (agent.snapshot['profileImages'] && agent.snapshot['profileImages']['large'] && (!agent['attributes']['trademe-image-id'] || agent['attributes']['trademe-image-id'] == '')) {
      var photo = agent.snapshot['profileImages']['large'];

      this._http.postPath(this.config.restAPI + '/trademe/photos/agent', { photo: photo }).subscribe((data: any) => {
        var res = (typeof data.response == 'object') ? data.response : JSON.parse(data.response);
        if (res.PhotoId) {
          agent.saveTrademeImageID(res.PhotoId);
        }
      });
    }    
  }

  addPhotos(property: Property) {

    if (property.snapshot['featuredImages'] && property.snapshot['featuredImages']['large'] && property.snapshot['featuredImages']['large']['trademeID'] == '') {
      var photo = property.snapshot['featuredImages']['large']['src'];
      this._http.postPath(this.config.restAPI + '/trademe/photos/add', {photo: photo}).subscribe((data: any) => {
        var res = (typeof data.response == 'object') ? data.response : JSON.parse(data.response);
        if(res.PhotoId) {
          property.addFeaturedImageTrademeID(res.PhotoId);
        } 
      });       
    }

    if(property.snapshot['galleryImages']) {
      for (var i in property.snapshot['galleryImages']) {
      if (property.snapshot['galleryImages'][i]['trademeID'] == '' && property.snapshot['galleryImages'][i]['marketing'] == true) {
          var photo = property.snapshot['galleryImages'][i]['large'];
          this._http.postPath(this.config.restAPI + '/trademe/photos/add', {photo: photo, ID: i}).subscribe((data: any) => {
            var res = (typeof data.response == 'object') ? data.response : JSON.parse(data.response);
            if(res.PhotoId && res.ID) {
              property.addGalleryImageTrademeID(res.ID, res.PhotoId); 
            }
          });           
        }
      }
    }
 
  }

  getPhotos(callback) {
    this._http.getPath(this.config.restAPI + '/trademe/photos').subscribe((data: any) => {
      console.log(data);
      callback();
    });    
  }

  getLocalities(callback) {
    var localities = localStorage.getItem('localities');
    if(localities) {
      this.localities = JSON.parse(localities);
    } else {  
      this._http.getPath(this.config.restAPI + '/localities').subscribe((data: any) => {
          localStorage.setItem('localities', data.response);
          this.localities = JSON.parse(data.response);
          callback();
      });
    } 
  }
}  