
import { Injectable, Inject}     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable}  from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable}  from 'angularfire2';

// Models
import { Integration } from './integration';
import { File } from '../files/file';
import { Person } from '../people/person';
import { Property } from '../properties/property';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Services
import { ConfigService } from '../config.service';
import { MessageService } from '../message/message.service';
import { HttpService } from '../http.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';


@Injectable()

export class WebsiteService extends Integration {

  private _url: string;
  public properties: any;

  constructor(
    private _http: HttpService, 
    private _message: MessageService,
    private _workflowItems: WorkflowItemsService,
    private _af: AngularFire,
    config: ConfigService,
    file: File
  ) {
    super(config, file);
  }

  syncAgent(agent: Person) {
    var agentData = this.mapAgent(agent);

    this._http.postPath(this.config.websiteAPI + 'agent', agentData).subscribe((data: any) => {

      try {
        var res = JSON.parse(data.response);
      } catch(err) {
        this._message.set('There was an error connecting to the website');
      }
      
      if(res && res.ID) {
        agent.addWebsiteID(res.ID).subscribe(() => {
          this._message.set('Agent updated on website', 'success');
        });
      } else {
        this._message.set('There was a problem updating the agent on the website', 'error');
      }
    });
  }



  syncListing(workflowItem: WorkflowItem) {
    
    var propertyData = this.mapListing(workflowItem);
    // Filter characters that the web can't handle
    propertyData['description'] = propertyData['description'].replace(/%0A/gi, "#brk#");
    propertyData['description'] = propertyData['description'].replace(/&/gi, "#amp#");      
    propertyData['agentIDs'] = workflowItem.agentWebsiteIDs.join();

      // Add open homes
      var openHomes = [];
      
      for (var l in workflowItem.snapshot.openHomes) {
        openHomes.push(workflowItem.snapshot.openHomes[l]['start'] + '#' + workflowItem.snapshot.openHomes[l]['end']);
      }
  
      propertyData['openHomes'].push(openHomes.join());
  
      this._http.postPath(this.config.websiteAPI + 'property', propertyData).subscribe((data: any) => {
        
        try {
          var res = JSON.parse(data.response);
        } catch(err) {
          this._message.set('There was an error connecting to the website');
        }
   
        if (res && res.ID && typeof res.ID == 'number') {
          // Don't overwrite the websiteID if it already exists
          if(workflowItem.snapshot['websiteID'] && workflowItem.snapshot['websiteID'] != '') {
            this._message.set('Listing updated on website', 'success');
          } else {
            // Add a websiteID if it doesn't exist
            workflowItem.addWebsiteID(res.ID).subscribe(() => {
             this._message.set('Listing updated on website', 'success');
            });
          }
        } else {
          this._message.set('There was a problem updating the listing on the website', 'error');
        }
      });   

  }

  syncProperty(property: Property, workflowItem: WorkflowItem) {
    
    var items = this._workflowItems.getWithProperty(property.object['$key']);
    for (var i = 0; i < items.length; ++i) {
      workflowItem.load(items[i]).subscribe(() => { 
      workflowItem.property = property;

        this.syncListing(workflowItem);
      });
    }

  }


}  