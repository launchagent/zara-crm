/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Observable } from 'rxjs/Observable';

// Models
import { Person } from '../../people/person';
import { WorkflowItem } from '../../workflow-items/workflow-item';

// Directives

// Services
import { PeopleService } from '../../people/people.service';
import { AuthService } from '../../auth/auth.service';
import { MessageService } from '../../message/message.service';
import { WorkflowItemsService } from '../../workflow-items/workflow-items.service';

// Pipes

/*
 * Vendor Section Component
 * 
 */
@Component({
  selector: 'vendor-section',
  providers: [ Person, WorkflowItem ],
  styleUrls: ['./sections.scss'],
  template: `
    <div class="vendor-section overlay-section">
      <h5>Vendor</h5>
      <person-form [(person)]="person" (update)="onUpdate()" [minimal]="false" [stage]="stage"></person-form>
      <h5>Vendor Solicitor</h5>
      <solicitor-component [(person)]="person"></solicitor-component>

    </div>
  `
})

export class VendorSectionComponent {

  @Input() stage: string = '';
  @Input() workflows: Object;
  @Input() person: Person;
  @Input() workflowItem: WorkflowItem;
  private _hasVendor: boolean = true;

  constructor(
    private _person: Person,
    private _auth: AuthService,
    private _message: MessageService,
    public people: PeopleService,
    public af: AngularFire,
    public workflowItemsService: WorkflowItemsService
  ) {

  }

  ngOnInit() {

  }

  ngOnChanges() {
    if(!this.person) {
      this.person = this._person;
      this._hasVendor = false;
    }

  }

  onUpdate() {
    if(!this._hasVendor) {
      this.workflowItem.addTo('vendors', this.person.ID);
      this.workflowItem.person = this.person;
    }
    this.workflowItemsService.setTotals();
  }


}

