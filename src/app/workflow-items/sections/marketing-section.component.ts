/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Observable } from 'rxjs/Observable';

// Models
import { MarketingItem } from '../../marketing-items/marketing-item';
import { WorkflowItem } from '../../workflow-items/workflow-item';

// Directives
import { MarketingItemsComponent } from '../../marketing-items/marketing-items.component';
import { OpenHomesComponent } from '../../workflow-items/open-homes.component';
import { PriceSelectComponent } from '../../attributes/price-select.component';

// Services
import { AuthService } from '../../auth/auth.service';
import { MessageService } from '../../message/message.service';
import { ModalService } from '../../modal/modal.service';
import { WorkflowItemsService } from '../../workflow-items/workflow-items.service';
import { TrademeService } from '../../integrations/trademe.service';
import { RealestateService } from '../../integrations/realestate.service';
import { MarketingAttributeService } from '../../attributes/marketing-attribute.service';

// Pipes

/*
 * Marketing Section Component
 * 
 */
@Component({
  selector: 'marketing-section',
  providers: [ WorkflowItem, MarketingItem, MarketingAttributeService ],
  styleUrls: ['./sections.scss'],
  templateUrl: './marketing-section.component.html'
})

export class MarketingSectionComponent {

  @Input() workflowItem: WorkflowItem;
  public listingClasses: Array<any> = [];
  public selectedCode: number;
  public attributes: Array<any>;
  public realestateRegions: Array<any>;
  public realestateDistricts: Array<any>;
  public realestateSuburbs: Array<any>;

  constructor(
    private _confirm: ModalService,
    private _auth: AuthService,
    private _af: AngularFire,
    private _message: MessageService, 
    private _marketingAttributes: MarketingAttributeService,
    public workflowItemsService: WorkflowItemsService,
    public trademe: TrademeService,
    public realestate: RealestateService
  ) {
    this.attributes = this._marketingAttributes.attributes;

  }

  ngOnInit() {
    if(typeof this.workflowItem.snapshot['attributes']['expected-sale-price'] === "undefined") {
      this.workflowItem.snapshot['attributes']['expected-sale-price'] = '100000';
    }
    this.updateSelects();
    this.getRealestateRegions();
    if(this.workflowItem.snapshot.attributes['realestate-region']) {
      this.getRealestateDistricts(this.workflowItem.snapshot.attributes['realestate-region']);
      this.getRealestateSuburbs(this.workflowItem.snapshot.attributes['realestate-region'], this.workflowItem.snapshot.attributes['realestate-district']);
    }
    this.autofill();
  }

  ngOnChanges() {

  }

  autofill() {
    if(this.workflowItem.snapshot.attributes['sales-method']) {
      var id = this.realestate.mapTradeMePricingType(this.workflowItem.snapshot.attributes['sales-method']);
      if(id > 0) {
        this.workflowItem.snapshot.attributes['realestate-pricing-code'] = id;
      }
    }
  }

  updateSelects() {
    this.listingClasses = [];
    for (var i = 0; i < this.realestate.listingTypes.length; ++i) {
      if (this.realestate.listingTypes[i].id == this.selectedCode) {
        this.listingClasses = this.realestate.listingTypes[i].classes;
      }
    } 
  }


  save() {
    this.workflowItem.property.load(this.workflowItem.property.ID).subscribe(() => {
      this.workflowItem.saveAttributes();
    });
  }

  getTrademeFees() {
    this.workflowItem.getTrademeFees();
  }

  addTrademePhotos() {
    this.workflowItem.addTrademePhotos();
  }

  addTrademe() {
    this._confirm.triggerConfirm('Are you sure you want to add this listing to Trade Me?', () => {
        this.workflowItem.createTradeMe();
    }, () => {});
  }

  withdrawTrademe() {
    this._confirm.triggerConfirm('Are you sure you want to remove this listing from Trade Me?', () => {
      this.workflowItem.withdrawTradeMe();
    }, () => { });    
  }

  getRealestateRegions() {
    this.realestate.getRegions().subscribe((data) => {
      this.realestateRegions = data['response'];
    });
  }

  getRealestateDistricts(region) {
    this.realestateDistricts = [];
    this.realestateSuburbs = [];    
    this.realestate.getDistricts(region).subscribe((data) => {
      this.realestateDistricts = data['response'];
    });
  }

  getRealestateSuburbs(region, district) {
    this.realestateSuburbs = []; 
    this.realestate.getSuburbs(region, district).subscribe((data) => {
      this.realestateSuburbs = data['response'];
    });
  }

}

