/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';

// Models
import { Property } from '../../properties/property';
import { WorkflowItem } from '../../workflow-items/workflow-item';

// Directives
import { ChattelsComponent } from '../../workflow-items/chattels.component';

// Services
import { WorkflowItemsService } from '../../workflow-items/workflow-items.service';

// Pipes

/*
 * Property Section Component
 * 
 */
@Component({
  selector: 'property-section',
  providers: [ Property, WorkflowItem ],
  styleUrls: ['./sections.scss'],
  template: `
  <div class="property-section overlay-section">
  <div >
    <button (click)="toggle()" class="button right">Edit <span *ngIf="totalRequiredFields > 0">(!)</span></button>
    <div>
      <property-detail [property]="property" *ngIf="show === 'detail'"></property-detail>
      <property-form [minimal]="false" [(property)]="property" (update)="onUpdate()" [stage]="stage" *ngIf="show === 'form'"></property-form> 
    </div>
    <div>
    <h4>Chattels</h4>
      <chattels-component [workflowItem]="workflowItem"></chattels-component>
    </div>
    </div>

  </div>
  `
})

export class PropertySectionComponent {

  @Input() workflows: Object;
  @Input() property: Property; 
  @Input() workflowItem: WorkflowItem;
  @Input() stage: string;
  @Input() totalRequired: number = 0;

  public show: string;
  private _hasProperty: boolean = true;

  constructor(
    private _property: Property,
    public workflowItemsService: WorkflowItemsService
  ) {
    this.show = 'detail';
  }

  ngOnInit() {

  }

  ngOnChanges() {    
    if(!this.property || !this.property.ID) {
      this.property = this._property;
      this._hasProperty = false;
    }
  }

  toggle() {
    this.show = (this.show === 'detail') ? 'form' : 'detail';
  }

  onUpdate() {
    if(!this._hasProperty) {
      this.workflowItem.addTo('properties', this.property.ID);
      this.workflowItem.property = this.property;
    }
    this.workflowItemsService.setTotals();
  }

}

