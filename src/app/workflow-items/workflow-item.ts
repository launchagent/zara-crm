/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import * as moment from 'moment';
import { Model } from '../model';

// Models
import { Event } from '../events/event';
import { Offer } from '../offers/offer';
import { Person } from '../people/person';
import { Property } from '../properties/property';
import { Setting } from '../settings/setting';

// Services
import { ConfigService } from '../config.service';
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { WorkflowService } from '../workflow/workflow.service'; 
import { WebsiteService } from '../integrations/website.service';
import { TrademeService } from '../integrations/trademe.service';

/*
 * WorkflowItem Model
 * 
 */
@Injectable()

export class WorkflowItem extends Model {

  public snapshot: workflowItemFields = {
    created: '',
    createdByID: '',
    isArchived: false,
    stage: '',
    properties: {},
    vendors: {},
    agents: {},
    buyers: {},
    attributes: {'set': 1 },
    requirements: {},
    listingID: '',
    trademeListingID: '',
    realestateListingID: '',
    websiteID: '',
    openHomes: {},
    offers: {},
    chattels: {},
    status: '',
    buyerID: ''  
  };   

  // Only used as holders for currently loaded models

  public canAdvance: boolean;
  public agentWebsiteIDs: Array<any> = [];  
  public agents: Array<any> = [];
  public propertyFileUrl: string = '';
  private _listingPrefix: string;
  private _listingIncrement: number;
  private _statusChange: Subject<any>;

  constructor(
      _af: AngularFire,
      _auth: AuthService,
      private _config: ConfigService,
      private _message: MessageService,
      private _event: Event,
      private _setting: Setting,
      private _workflow: WorkflowService,
      private _website: WebsiteService,
      private _trademe: TrademeService,
      public person: Person,
      public property: Property,
      public agent: Person
  ) {
    super(_af, _auth);
    this._statusChange = new Subject();
  }

  /*
  * Getter for statusChange Observable
  */
  get statusChange() {
    return this._statusChange.asObservable();
  }

  /*
  * Run this once auth has completed
  */
  init() {
    this._ref = "/workflow-items/" + this._auth.companyID;
    this.list = this._af.database.list(this._ref);
    this.saved.subscribe(() => {
      this.addListingID();
    });
    this.userSaved.subscribe(() => {
      this.sync();
    });
  }

  /*
  * Load the workflowItem and subscribe to changes to add the listingID
  */
  load(ID: string, options?: any) {
    this.object = super.load(ID, options);
    this.object.subscribe(() => {
        if(this.snapshot && this.snapshot['vendors']) {
          this.person.load(this.getFirst(this.snapshot['vendors']));         
        }
        if(this.snapshot && this.snapshot['properties']) {
          this.property.load(this.getFirst(this.snapshot['properties']));          
        }
        if(this.snapshot && this.snapshot['websiteID'] && this.snapshot['websiteID'] != '') {
          this.canDuplicate = false;
        }
        if(this.snapshot && this.snapshot['trademeListingID'] && this.snapshot['trademeListingID'] != '') {
          this.canDuplicate = false;
        }
    }); 
    this.loadPropertyFileUrl();
    this.loadAgents();
    this.loadAgentWebsiteIDs();
    this.watchListingSettings();
    return this.object;   
  }

  /*
  * Watch the listing settings so we can create listingIDs with the correct values
  */
  watchListingSettings() {  
    this._setting.get('listing-prefix').$ref.once('value', (value) => {
      this._listingPrefix = value.val();
    });
    this._setting.get('listing-increment').subscribe((data) => {
      this._listingIncrement = data.$value;
    });  
  }  

  /*
  * Loop through workflow stages to create empty requirements object
  */
  initRequirements() {
    for (var i = 0; i < this._workflow.stages.length; ++i) {
      this.snapshot['requirements'][this._workflow.stages[i].className] = 0;
    }
  }


  /*
  * Sync the workflow item with all available constant integrations
  */
  sync() {
    if (this.snapshot['attributes'] && this.snapshot['attributes']['list-on-website']) {
      this._website.syncListing(this);
    } 
    // Only update existing trade listings
    if (this.snapshot['attributes'] && this.snapshot['attributes']['list-on-trademe'] && this.snapshot['trademeListingID'] != '') {
      this._trademe.editListing(this);
    } 
  }

  /*
  * Sync the workflow item with trade me integration
  */
  createTradeMe() {
    if (this.snapshot['attributes'] && this.snapshot['attributes']['list-on-trademe']) {
      this._trademe.addListing(this);
    }  
  }


  /*
  * Sync the workflow item with trade me integration
  */
  withdrawTradeMe() {
    if (this.snapshot['trademeListingID'] && this.snapshot['trademeListingID'] != '') {
      this._trademe.removeListing(this);
    }
  }

  /*
  * Archive the workflowitem
  */
  archive() {
  
    this.update('isArchived', true).subscribe(() => {
      // Create the event
      this._event.create({ createdByID: this._auth.uid, workflowID: this.ID, description: 'item archived' });
    });        
  }

  /*
  * Unarchive the workflowitem
  */
  unarchive() {
    this.update('isArchived', false).subscribe(() => {
      // Create the event
      this._event.create({ createdByID: this._auth.uid, workflowID: this.ID, description: 'item unarchived' });
    });   
  }

 /* 
 *Save thestage of he workflowitem
 */
  setStage(stage: string) {
    this.update('stage', stage, false).subscribe(() =>  {
      // Create the event
        this._event.create({ createdByID: this._auth.uid, workflowID: this.ID, description: 'stage changed' });
    });      
  } 

  /*
  * Add an open home to the workflow item
  */
  addOpenHome(startDate: string, startTime: string, endTime: string) {
    var openHome = {
      created: moment().format('YYYY-MM-DD H:mm:ss'),
      createdByID: this._auth.uid,
      start: startDate + ' ' + startTime + ':00',
      end: startDate + ' ' + endTime + ':00' 
    };
    var list = this._af.database.list(this.path + '/openHomes');
    return list.push(openHome);
  }

  /*
  * Remove an open home
  */
  removeOpenHome(ID: string) {
    return this.removeFrom('openHomes', ID);
  }

  /*
  * Add chattel to the workflow item
  */
  addChattel(type: string, qty: number) {
    var chattel = {
      created: moment().format('YYYY-MM-DD H:mm:ss'),
      createdByID: this._auth.uid,
      chattel: type,
      qty: qty
    };
    var list = this._af.database.list(this._ref + '/' + this.ID + '/chattels');
    return list.push(chattel);
  }
  
  /*  
  * Remove a chattel
  */
  removeChattel(ID: string) {
    var list = this._af.database.list(this._ref + '/' + this.ID + '/chattels');
    return list.remove(ID);
  }

  /*
  * Add an offer to the workflow item
  */
  addOffer(offer: any) {
    var list = this._af.database.list(this._ref + '/' + this.ID + '/offers');
    return list.push(offer);
  }

  /*
  * Remove an offer from the workflow item
  */
  removeOffer(ID: string) {
    var list = this._af.database.list(this._ref + '/' + this.ID + '/offers');
    return list.remove(ID);
  }

  // /*
  // * Accept a selected an offer
  // */
  // acceptOffer(offer) {
  //   this.addBuyerID(offer.buyerID, () => {
  //     this._ref.child(this.ID).child('offers').child(offer.$key).child('status').set('accepted', () => {
  //       callback();
  //     });  
  //   });
  // }

  // /*
  //  * Add the selected buyer ID to the workflow item
  //  */
  // addBuyerID(id) {
  //   this.buyerID = id;
  //   this._ref.child(this.ID).child('buyerID').set(id, () => {
  //     callback();
  //   });
  // }

  /*
  * Add a marketing item to the workflow item
  */
  addMarketingItem(type: string, option: string, price: number, orderedBy: string) {
    var item = {
      created: moment().format('YYYY-MM-DD H:mm:ss'),
      createdByID: this._auth.uid,
      type: type,
      option: option,
      price: price,
      booked: false,
      completed: false,
      orderedBy: orderedBy,
      amountPaid: 0
    };
    var list = this._af.database.list(this._ref + '/' + this.ID + '/marketing-items');
    return list.push(item);
  }

  /*
  * Remove a marketing item from the workflow item
  */
  removeMarketingItem(ID: string) {
    return this.removeFrom('marketing-items', ID);
  }

  /*
  * Update a marketing item on the workflow item
  */
  updateMarketingItem(Item) {
    var marketingItem = {
      booked: Item.booked,
      completed: Item.completed,
      created: Item.created,
      createdByID: Item.createdByID,
      option: Item.option,
      price: Item.price,
      type: Item.type,
      orderedBy: Item.orderedBy,
      amountPaid: Item.amountPaid
    };
    var object = this._af.database.object(this._ref + '/' + this.ID + '/marketing-items/' + Item['$key']);
    return object.set(marketingItem);
  }

  /*
  * Add a payment to a marketing item 
  */
  addMarketingItemPayment(Item, amount) {
    var total = parseFloat(amount) + parseFloat(Item.amountPaid);
    var object = this._af.database.object(this._ref + '/' + this.ID + '/marketing-items/' + Item['$key']);
    return object.set({'amountPaid': total});
  }

  /*
  * Save requirements to the workflow item
  */
  setRequirement(stage: string, num: number, type: string) {
    var requirements = this.snapshot.requirements;
    if (requirements[stage] != num) {
      requirements[stage] = num;
    }
    return this.update('requirements', requirements);
  }

  /*
  * Save the websiteID of the workflow item
  */
  addWebsiteID(ID: string) {
    return this.update('websiteID', ID, false);     
  }

  /*
  * Save the trademeListingID of the workflow item
  */
  addTrademeListingID(ID: string) {
    return this.update('trademeListingID', ID, false);     
  }

  /*
  * Remove the trademeListingID from the workflow item once it has been withdrawn
  */
  removeTrademeListingID() {
    return this.update('trademeListingID', null);    
  }

  /*
  * Save the listingID of the workflow item
  */
  addListingID() {
      var object; 
      if(this.snapshot['stage'] === 'listing' && (!this.snapshot['listingID'] || this.snapshot['listingID'] == '')) {
        this._listingIncrement++;
        this._setting.generate('listing-increment', this._listingIncrement);
         this.snapshot['listingID'] = this._listingPrefix + '-' + this._listingIncrement;
        object = this.update('listingID', this.snapshot['listingID'], false); 
      }
     return object;
  }

  /*
  * Save the status of the workflow item
  */
  setStatus(status: string, reason: string) {
    var eventDescription = 'Status changed to ' + status;
    if(reason != '') {
      eventDescription = eventDescription + ' - Reason: ' + reason;
    }
    this.update('status', status).subscribe(() => {
      if (status == 'Trashed' || status == 'Withdrawn' || status == 'Completed' || status == 'On Hold') {
        this.archive();
      } else if(status == 'Active') {
        this.unarchive();
      }
      this._event.create({ createdByID: this._auth.uid, workflowID: this.ID, description: eventDescription });   
      this._statusChange.next(status);         
    });
    return this.object;
  }

  /*
  * Get the trade me fees for the workflow item
  */
  getTrademeFees() {
    // this._trademe.getAddListingFees(this, (data) => {
    // console.log(data);
    // });
  }

  /*
  * Upload all the images to Trade Me for the workflow item
  */
  addTrademePhotos() {
    this.property.load(this.property.ID).subscribe(() => {
      this._trademe.addPhotos(this.property);
    });
  }

  /*
  * Load the agent website IDs for the workflow item
  */
  loadAgentWebsiteIDs() {
    var agents = this._af.database.list(this.path + '/agents');
    agents.subscribe((snapshots) => {
      this.agentWebsiteIDs = [];
      snapshots.forEach((snap) => {
        var agent = this.agent.queryList({orderByKey: true, equalTo: snap.$value}, false).subscribe((values) => {
          values.forEach((val) => {
            if(val.websiteID) {
              this.agentWebsiteIDs.push(val.websiteID);
            }
          });
        });  
      });
    });
  }

  /*
  * Load the agent website IDs for the workflow item
  */
  loadAgents() {
    var agents = this._af.database.list(this.path + '/agents');
    agents.subscribe((snapshots) => {
      this.agents = [];
      snapshots.forEach((snap) => {
        var agent = this.agent.queryList({orderByKey: true, equalTo: snap.$value}, false).subscribe((values) => {
          this.agents.push(values[0]);
        });  
      });
    });
  }

  /*
  * Watch for property file to be changed and auto-load the url
  */
  loadPropertyFileUrl() {
      // Get the src url for the property file
      var propertyFile = this._af.database.object(this.path + '/attributes/property-file');
      propertyFile.subscribe((snapshot) => {
        if(snapshot.$value) {
          var file = this._af.database.object('/files/' + this._auth.companyID + '/' + this.ID + '/' + snapshot.$value);
          file.subscribe((snap) => {
            if(snap.$value && snap.$value['name']) {
              this.propertyFileUrl = this._config.fileServer + this.ID + '/' + snap.$value['name'];
            }
          });
        }
      });  
  }

}

export interface workflowItemFields {
  created: string,
  createdByID: string,
  isArchived: boolean,
  stage: string,
  properties: any,
  vendors: any,
  agents: any,
  buyers: any,
  attributes: any,
  requirements: any,
  listingID: string,
  trademeListingID: string,
  realestateListingID: string,
  websiteID: string,
  openHomes: any,
  offers: any,
  chattels: any,
  status: string,
  buyerID: string
}

