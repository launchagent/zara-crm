/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';

/*
 * Chattels Service
 * 
 */
@Injectable()

export class ChattelsService {

  public values: Array<any>;

  constructor(

  ) {
    this.setup();
  }

  setup() {
    this.values = [
      { name: 'Stove', qty: 0 },
      { name: 'Fixed Floor Coverings', qty: 0 },
      { name: 'Blinds', qty: 0 },
      { name: 'Curtains', qty: 0 },
      { name: 'Alarm', qty: 0 },
      { name: 'Dishwasher', qty: 0 },
      { name: 'Drapes', qty: 0 },
      { name: 'Extractor Fan', qty: 0 },
      { name: 'Garage Door Opener', qty: 0 },
      { name: 'Garden Shed', qty: 0 },
      { name: 'Heated Towel Rail', qty: 0 },
      { name: 'Light Fittings', qty: 0 },
      { name: 'Pool Equipment', qty: 0 },
      { name: 'Rangehood', qty: 0 },
      { name: 'Heat Pump', qty: 0 },
      { name: 'Satellite Dish', qty: 0 },
      { name: 'Smoke Alarm', qty: 0 },
      { name: 'Stove', qty: 0 },
      { name: 'TV Aerial', qty: 0 },
      { name: 'Waste Disposal', qty: 0 }
    ];
  }

}

