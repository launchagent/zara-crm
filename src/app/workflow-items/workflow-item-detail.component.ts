/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { WorkflowItem } from './workflow-item';

// Directives

// Services
import { WorkflowService } from '../workflow/workflow.service';
import { WorkflowItemsService } from './workflow-items.service';
import { ModalService } from '../modal/modal.service';
import { MessageService } from '../message/message.service';

// Pipes

/*
 * Workflow Items Component
 * 
 */
@Component({
  selector: 'workflow-item-detail',
  providers: [ WorkflowItem ],
  styleUrls: ['./workflow-item-detail.component.scss'],
  templateUrl: './workflow-item-detail.component.html'
})

export class WorkflowItemDetail {

  public selected: string = 'details';
  public reason: string = '';
  public showStatusPanel: boolean = false;
  public showTodoPanel: boolean = false;
  public disableSave: boolean = false;
  public enableDuplicate: boolean = false;
  public saveButtonText: string = 'Save';

  constructor(
      private _confirm: ModalService,
      private _message: MessageService,
      private _route: ActivatedRoute,
      private _workflowItem: WorkflowItem,
      public af:AngularFire,
      public workflows: WorkflowService,
      public workflowItemsService: WorkflowItemsService
  ) {

  }

  ngOnInit() {
    var ID = this._route.snapshot.params['id'];
    if(!this.workflowItemsService.workflowItem && ID) {
      this._workflowItem.load(ID).subscribe(() => {
        this.workflowItemsService.workflowItem = this._workflowItem;
      });
    }

  }

  ngOnChanges() {

  }

  save() {
    this.saveButtonText = 'Saving...';
    this.disableSave = true;
    var synced = false;
      this.workflowItemsService.workflowItem.saveAttributes(() => {
        this.workflowItemsService.workflowItem.load(this.workflowItemsService.workflowItem.ID, {'onSuccess': () => {
          if(!synced) {            
            this.workflowItemsService.workflowItem.sync();
            synced = true;
            this.saveButtonText = 'Save';
            this.disableSave = false;
          }
        }});
      });
  }

  toggleStatusPanel() {
    this.showStatusPanel = (this.showStatusPanel) ? false : true;
  }

  toggleTodoPanel() {
    this.showTodoPanel = (this.showTodoPanel) ? false : true;
  }

  duplicate() {
    this.workflowItemsService.workflowItem.duplicate('listingID', '');
    this._message.set('Listing duplicated', 'success');
  }
 
  changeStatus(type: string) {
    if(this.reason == '') {
      this._message.set('You need to enter a reason', 'warning');
    } else {
      this._confirm.triggerConfirm('Are you sure you want to change the status to ' + type + '?', () => {
        this.workflowItemsService.workflowItem.setStatus(type, this.reason).subscribe(() => {
          this._message.set('Status changed', 'success');
          this.reason = '';
        });
      }, () => {});      
    }
  }

}

