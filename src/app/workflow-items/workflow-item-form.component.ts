/*
 * Angular 2 decorators and services
 */
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Person } from '../people/person';
import { Property } from '../properties/property';
import { WorkflowItem } from './workflow-item';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';


// Pipes

/*
 * Workflow Item Form Component
 * 
 */
@Component({
  selector: 'workflow-item-form',
  providers: [ WorkflowItem, Person, Property],
  styleUrls: ['./workflow-items.component.scss'],
  template: `
      <div class="modal-form">
      	<div class="add-header">Add New</div>
	      <form (ngSubmit)="onSubmit()" *ngIf="active">
	        <div class="form-padding">
		        <input type="text" placeholder="Person" [(ngModel)]="person.name" (keyup)="autocompletePerson.open()" (blur)="autocompletePerson.close()" name="personName" autocomplete="off" >
		        <autocomplete-person #autocompletePerson [key]="person.name" [values]="people | async" (update)="person.name = $event.name; person.ID = $event.$key;" (onAddNew)="personFormModal.open()"></autocomplete-person>
		        <input type="text" placeholder="Property" [(ngModel)]="property.name" (keyup)="autocompleteProperty.open()" (blur)="autocompleteProperty.close()" name="propertyName" autocomplete="off" >
		        <autocomplete-property #autocompleteProperty [key]="property.name" [values]="properties | async" (update)="property.name = $event.name; property.ID = $event.$key;" (onAddNew)="propertyFormModal.open()"></autocomplete-property>
	        </div>
	        <button type="submit" class="button" [disabled]="disableSave">Save</button>
	      </form>
	      <modal-component #personFormModal class="modal second"><person-form (update)="person.name = $event.name; person.ID = $event.ID; personFormModal.toggle();"></person-form></modal-component>
	      <modal-component #propertyFormModal class="modal second"><property-form (update)="property.name = $event.name; property.ID = $event.ID; propertyFormModal.toggle();"></property-form></modal-component>     
      </div>

  `
})

export class WorkflowItemForm {

  public people: Observable<any[]>;
  public properties: Observable<any[]>;
  public active: boolean = true;  
  public disableSave = false;

  constructor(
    private _auth: AuthService,
    private _message: MessageService,
    private _af:AngularFire,
    public workflowItem: WorkflowItem,
    public person: Person,
    public property: Property    
  ) {

  }


  ngOnInit() {
    if(this._auth.companyID) {
      this.load();
    } else {
      this._auth.isLoggedIn.subscribe((val) => {
        if(val) {
          this.load();
        }
      });
    }
  }

  load() {
    this.people = this._af.database.list("/people/" + this._auth.companyID);
    this.properties = this._af.database.list("/properties/" + this._auth.companyID);
  }

  clear() {
    
    if((this.person.ID=== '' || typeof this.person.ID === 'undefined') && (this.property.ID === '' || typeof this.property.ID === 'undefined')) {
      this.disableSave = false;
    }
  }

  onSubmit() {
    
    if((!this.person.ID || this.person.ID == '') && (!this.property.ID || this.property.ID == '')) {
      this._message.set('You need to enter either a property or a person.', 'warning');
    } else {

      this.workflowItem.snapshot['stage'] = 'prospect';
      this.disableSave = true;
      this.workflowItem.initRequirements();
      this.workflowItem.create(this.workflowItem.snapshot).then(() => {
        this._message.set('workflow item saved', 'success');
        this.workflowItem.addTo('agents', this._auth.uid).subscribe(() => {
          this.addVendor();
          this.addProperty();
          this.workflowItem.clear();            
        });
      });      
    }

  }

  addVendor() {
    // Only add vendor if a person set
    if(this.person.ID) {
      this.workflowItem.addTo('vendors', this.person.ID);
      this.person.update('vendor', true);        
      this.person.addTo('workflow-items', this.workflowItem.ID).subscribe(() => {
        this.person.clear();
        this.clear();
      });             
    }
  }

  addProperty() {
    // Only add property if property set
    if(this.property.ID) {
      this.workflowItem.addTo('properties', this.property.ID);
      this.property.addTo('workflow-items', this.workflowItem.ID).subscribe(() => {
        this.property.clear();
        this.clear();  
      });                
    }
  }

}

