/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives


// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';
import { WorkflowItemsService } from './workflow-items.service';

// Pipes

/*
 * Open Homes Component
 * 
 */
@Component({
  selector: 'open-homes',
  providers: [ WorkflowItem ],
  styleUrls: ['./workflow-item-detail.component.scss'],
  template: `
    <ul class="no-bullet"> 
      <li *ngFor="let openHome of openHomes | async" class="row">
        <div class="small-4 left">{{openHome.start | todate | date:'yMMMd'}}</div>  
        <div class="small-4 left">{{openHome.start | todate | date:'shortTime'}} - {{openHome.end | todate | date:'shortTime'}}</div>
        <div class="small-4 left"><a (click)="delete(openHome.$key)">X</a></div>
      </li>
      <li><a (click)="toggleForm()">{{showFormLabel}}</a></li>
    </ul>  
    <div class="form-sectionl" *ngIf="showForm">
      <form (ngSubmit)="add()" >
        <div class="row">
          <div class="small-6 left">
            <label>Start:</label>
            <input type="date" [(ngModel)]="startDate" name="startDate">
            <input type="time" [(ngModel)]="startTime" name="startTime">    
          </div>
          <div class="small-6 left">
            <label>End:</label>
            <input type="time" [(ngModel)]="endTime" name="endTime">   
          </div>
        </div>
        <button type="submit" class="button">Add open home</button>
       </form>
    </div>   

  `
})

export class OpenHomesComponent {
  @Input() workflowItem: WorkflowItem;
  public openHomes: Observable<any[]>;
  public startDate: any;
  public startTime: any;
  public endDate: any;
  public endTime: any;
  public showForm: boolean = false;
  public showFormLabel: string = '+ Add open home';

  constructor(
    private _auth: AuthService,
    private _message: MessageService,
    private _confirm: ModalService,
    public af: AngularFire
  ) {
  }

  ngOnInit() {
    
  }

  ngOnChanges() {
    if(this.workflowItem) {
      this.openHomes = this.af.database.list('/workflow-items/' + this._auth.companyID + '/' + this.workflowItem.ID + '/openHomes');
    }
  }

  getFirst(obj) {
    if(obj) {
      return obj[Object.keys(obj)[0]];      
    }
  }

  toggleForm() {
    this.showForm = (this.showForm) ? false : true;
    this.showFormLabel = (this.showForm) ? 'Done' : '+ Add open home';
  }

  add() {
    this.workflowItem.addOpenHome(this.startDate, this.startTime, this.endTime).then(() => {
      this._message.set('Open home added', 'success');
    });
  }

  delete(ID) {
    this._confirm.triggerConfirm('Are you sure you want to delete this open home?', () => {
      this.workflowItem.removeOpenHome(ID).subscribe(() => {
        this._message.set('Open home deleted', 'success');
      });
    }, () => {});
  }


}

