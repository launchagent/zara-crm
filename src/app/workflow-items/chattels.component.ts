/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';
import { ChattelsService } from './chattels.service';
import { WorkflowItemsService } from './workflow-items.service';

// Pipes

/*
 * Chattels Component
 * 
 */
@Component({
  selector: 'chattels-component',
  providers: [ WorkflowItem, ChattelsService ],
  styleUrls: ['./workflow-item-detail.component.scss'],
  template: `
    <ul class="no-bullet"> 
      <li *ngFor="let chattel of chattels | async" class="row">
        <div class="small-4 left">{{chattel.chattel}}</div>  
        <div class="small-4 left">{{chattel.qty}}</div>
        <div class="small-4 left"><a (click)="delete(chattel.$key)">X</a></div>
      </li>
    </ul>     
<form (ngSubmit)="add()" >
  <div class="row small-up-1 medium-up-2 large-up-3">
    <div class="column" *ngFor="let option of chattelOptions.values">
      <div class="small-8 columns">
          {{option.name}}  
      </div>
      <div class="small-4 columns">
        <input type="number" [(ngModel)]="option.qty" name="qty" #qty="ngModel" >
      </div>
    </div>
  </div>
  <button type="submit" class="button">Add chattels</button>
 </form>
  `
})

export class ChattelsComponent {
  @Input() workflowItem: WorkflowItem;
  public chattels: Observable<any[]>;
  public type: string;
  public qty: number = 1;


  constructor(
    private _auth: AuthService,
    private _message: MessageService,
    private _confirm: ModalService,
    public af: AngularFire,
    public chattelOptions: ChattelsService
  ) {
  }

  ngOnInit() {
    
  }

  ngOnChanges() {
    if(this.workflowItem) {
      this.chattels = this.af.database.list('/workflow-items/' + this._auth.companyID + '/' + this.workflowItem.ID + '/chattels');
    }
  }

  getFirst(obj) {
    if(obj) {
      return obj[Object.keys(obj)[0]];      
    }
  }

  add() {
    for (var i = 0; i < this.chattelOptions.values.length; ++i) {
      if (this.chattelOptions.values[i].qty > 0) {
        this.workflowItem.addChattel(this.chattelOptions.values[i].name, this.chattelOptions.values[i].qty);            
      }
    }
    this._message.set('Chattels added', 'success');
  }

  delete(ID) {
    this._confirm.triggerConfirm('Are you sure you want to delete this chattel?', () => {
      this.workflowItem.removeChattel(ID).then(() => {
        this._message.set('Chattel deleted', 'success');
      });
    }, () => {});
  }


}

