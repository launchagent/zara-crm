/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { WorkflowItem } from './workflow-item';

// Services
import { AuthService } from '../auth/auth.service';
import { WorkflowService } from '../workflow/workflow.service';
import { PropertyAttributeService } from '../attributes/property-attribute.service';
import { PersonAttributeService } from '../attributes/person-attribute.service';
import { MarketingAttributeService } from '../attributes/marketing-attribute.service';
import { ListingAttributeService } from '../attributes/listing-attribute.service';
import { MarketAttributeService } from '../attributes/market-attribute.service';
import { SoldAttributeService } from '../attributes/sold-attribute.service';
/*
 * WorkflowItems Service
 * This contains a lot of the workflow item enigine for running the worklow and advancing stages
 * 
 */
@Injectable()

export class WorkflowItemsService {

  private _ref: FirebaseListObservable<any>;
  public values: Object;
  public workflowItem: WorkflowItem;
  public selectedStage: any;
  public totals: Object;
  public propertyAttributes: Array<any>;
  public vendorAttributes: Array<any>;
  public marketingAttributes: Array<any>;
  public listingAttributes: Array<any>;
  public marketAttributes: Array<any>;
  public offersAttributes: Array<any>;
  public soldAttributes: Array<any>;

constructor(
    private _af: AngularFire,
    private _auth: AuthService,
    public workflow: WorkflowService
  ) {

    this.propertyAttributes = new PropertyAttributeService().attributes;
    this.vendorAttributes = new PersonAttributeService().attributes;
    this.marketingAttributes = new MarketingAttributeService().attributes;
    this.listingAttributes = new ListingAttributeService().attributes;
    this.marketAttributes = new MarketAttributeService().attributes;
    this.soldAttributes = new SoldAttributeService().attributes;

    // Load current workflow items from FB for cache access
    this._ref = this._af.database.list('/workflow-items/' + this._auth.companyID);

    this.totals = {};
    this.init();

  }  

  /*
  * Setup the totals array to have objects for each stage in the workflow
  */
  init() {
    for (var i = 0; i < this.workflow.stages.length; ++i) {
      var key = this.workflow.stages[i].className;
      var obj = { total: 0, completed: 0, required: 0 };
      this.totals[key] = {property: obj, vendor: obj, marketing: obj, stage: obj, totals: obj};
    }
  }

  /*
  * Set the selectedStage to the current stage of the workflowitem
  */
  loadCurrentStage() {
    if(this.workflowItem.snapshot) {
      this.selectedStage = this.workflow.loadStage(this.workflowItem.snapshot['stage']);
    }
  }

  /*
  * Advance the current workflow item to the next stage in the workflow
  */
  advance() {
    // For the stage we are on check the requirements are met
    if(this.workflowItem.canAdvance) {
      var stage = this.workflow.getNextStage(this.workflowItem.snapshot['stage']);
      if(stage == 'archived') {
        this.workflowItem.setStatus('Completed', '');
      } else {
        this.workflowItem.setStage(stage);
      }
      this.setTotals();         
    } 
  }

  /*
  * Set objects on our totals array with total actions/fields and completed actions/fields
  */
  setTotals() {
    for (var i = 0; i < this.workflow.stages.length; ++i) {
      var key = this.workflow.stages[i].className;
      this.totals[key].property = this.validateAttributes(key, this.propertyAttributes, this.workflowItem.property);
      this.totals[key].vendor = this.validateAttributes(key, this.vendorAttributes, this.workflowItem.person);
      this.totals[key].marketing = this.validateAttributes(key, this.marketingAttributes, this.workflowItem);
      var attributes = [];
      switch (key) {
        case "listing":
          attributes = this.listingAttributes;
        break;
        case "market":
          attributes = this.marketAttributes;
        break;
        case "sold":
          attributes = this.soldAttributes;
        break;  
      }
      var wfi = this.validateAttributes(key, attributes, this.workflowItem);
      this.totals[key].stage = { total: this.workflow.stages[i].requirements + wfi.total, completed: this.workflowItem.snapshot['requirements'][key] + wfi.completed || 0, required: (this.workflow.stages[i].requirements + wfi.total) - (this.workflowItem.snapshot['requirements'][key] + wfi.required) || 0};
      
      // add base requirements
      if (this.workflow.stages[i].base) {
        if (this.workflow.stages[i].base.vendor) {
          this.totals[key].vendor.total++;
          if (this.workflowItem.person && this.workflowItem.person.ID != '') {
            this.totals[key].vendor.completed++;
          } else { this.totals[key].vendor.required++; }
      }
      if (this.workflow.stages[i].base.property) {
          this.totals[key].property.total++;
          if (this.workflowItem.property && this.workflowItem.property.ID != '') {
            this.totals[key].property.completed++;
          } else { this.totals[key].property.required++; }
        }
      }

      // Add all total and completed and store on totals
      var total = this.totals[key].property.total + this.totals[key].vendor.total + this.totals[key].marketing.total + this.totals[key].stage.total;
      var completed = this.totals[key].property.completed + this.totals[key].vendor.completed + this.totals[key].marketing.completed + this.totals[key].stage.completed;
      this.totals[key].totals = {total: total, completed: completed, required: total - completed};
    }
    // Set canAdvance on the workflow item if the total required actions equals 0
    this.workflowItem.canAdvance = (this.totals[this.workflowItem.snapshot['stage']].totals.required === 0) ? true : false; 
  }

  /*
  * Loop through the attributes and check to see if the model meets the requirements for the stage
  */
  validateAttributes(stage, attributes, model) {
    var totalFields = 0;
    var totalRequiredFields = 0; 

    for (var i = 0; i < attributes.length; ++i) {
   
      var field = attributes[i];
      // if(stage == 'appraisal') {
      //  console.log(field.display + ' : ' stage + ' : ' + totalFields);  
      // }
      if (field.workflows[0].stage === stage) {
        totalFields++;
       }
      if(model && model.snapshot && typeof model.snapshot.attributes != 'undefined') {        
        if (field.workflows[0].stage === stage && (typeof model.snapshot.attributes[field.slug] === 'undefined' || model.snapshot.attributes[field.slug] == '')) {
          totalRequiredFields++;
        }
      } else if (field.workflows[0].stage === stage) {
        totalRequiredFields++;
      }
    }       

    return {total: totalFields, completed: totalFields - totalRequiredFields, required: totalRequiredFields};

  }

  getStage(id: string) {
    if (this.values[id] && this.values[id].stage) {
      return this.values[id].stage;
    } else { 
      return '';
    }
    
  }

  getVendors(id: string) {
    if(this.values[id] && this.values[id].vendors) {
      return this.values[id].vendors;
    } else { 
      return {};
    }
  }

  getProperties(id: string) {
    if(this.values[id] && this.values[id].properties) {
      return this.values[id].properties;
    } else { 
      return {};
    }
  }

  getAgents(id: string) {
    if(this.values[id] && this.values[id].agents) {
      return this.values[id].agents;
    } else { 
      return {};
    }
  }

  getBuyers(id: string) {
    if(this.values[id] && this.values[id].buyers) {
      return this.values[id].buyers;
    } else { 
      return {};
    }
  }

  getWithProperty(propertyID: string) {
    var ids = [];
    for (var i in this.values) {
      if(this.values[i].properties) {
        for (var j in this.values[i].properties) {
          if (this.values[i].properties[j] == propertyID) {
            ids.push(i);
          }
        }
      }
    }
    return ids;
  }

}

