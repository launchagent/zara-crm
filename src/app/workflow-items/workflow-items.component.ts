/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Person } from '../people/person';
import { Property } from '../properties/property';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives

// Services
import { ModalService } from '../modal/modal.service';
import { PeopleService } from '../people/people.service';
import { PropertiesService } from '../properties/properties.service';
import { WorkflowItemsService } from './workflow-items.service';

// Pipes

/*
 * Workflow Items Component
 * 
 */
@Component({
  selector: 'workflow-item',
  providers: [ Person, Property, WorkflowItem ],
  styleUrls: ['./workflow-items.component.scss'],
  templateUrl: './workflow-items.component.html'
})

export class WorkflowItemsComponent {
  @Input() item: any;
  @Output() modalTrigger = new EventEmitter(false);

  constructor(
    private _person: Person,
    public workflowItem: WorkflowItem,
    public workflowItemService: WorkflowItemsService,
    public people: PeopleService,
    public properties: PropertiesService
  ) {

  }

  ngOnInit() {
    this.load();
  }

  ngOnChanges() {
    this.load();
  }

  load() {
    if(this.item) {
      this.workflowItem.load(this.item.$key);
    }
  }

  select() {
    this.workflowItemService.workflowItem = this.workflowItem;    
    this.modalTrigger.emit('open');
  }

  addTodo(personID: string) {
    console.log(personID);
  }

}

