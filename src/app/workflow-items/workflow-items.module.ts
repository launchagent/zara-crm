import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { PeopleModule } from '../people/people.module';
import { PropertiesModule } from '../properties/properties.module';

// Directives
import { WorkflowItemsComponent } from './workflow-items.component';

import { MarketingSectionComponent } from './sections/marketing-section.component';
import { PropertySectionComponent } from './sections/property-section.component';

import { MarketingItemsComponent } from '../marketing-items/marketing-items.component';
import { OpenHomesComponent } from './open-homes.component';

@NgModule({
    imports: [ RouterModule, CommonModule, SharedModule, PeopleModule, PropertiesModule ],
    declarations: [ 

    ],
    exports: [  ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class WorkflowItemsModule {
}