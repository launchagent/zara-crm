/*
 * Angular 2 decorators and services
 */
import { Component, Input, Output, Inject, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';

// Models
import { Event } from '../events/event'
import { Person } from '../people/person';
import { WorkflowItem } from '../workflow-items/workflow-item'

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ModalService } from '../modal/modal.service';
import { PeopleService } from '../people/people.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';
import { ProspectAttributeService } from '../attributes/prospect-attribute.service';

// Pipes

/*
 * Prospect Component
 * 
 */
@Component({
  selector: 'prospect-stage',
  providers: [ Person, Event, ProspectAttributeService ],
  styleUrls: ['./stages.component.scss'],
  template: `
  	<div class="stage-wrapper">	
	    <h3>Prospect</h3>
	    <div class="panel">
		    Agents
		    <ul class="no-bullet agents"> 
		      <li *ngFor="let agent of agents" class="row">
		        <div class="small-3 columns">{{(agent.person | async)?.name}}</div>  
		        <div class="small-3 columns"><a href="mailto:{{(agent.person | async)?.email}}">{{(agent.person | async)?.email}}</a></div>
		        <div class="small-3 columns">{{(agent.person | async)?.attributes['work-phone']}}</div>
            <div class="small-3 columns actions"><a (click)="removeAgent(agent.$key)" *ngIf="workflowItem.snapshot.stage == 'prospect'">x</a></div>
		      </li>
		    </ul>  
		    <input type="text" placeholder="Add Agent" [(ngModel)]="person.name" (keyup)="autocompletePerson.open()" (blur)="autocompletePerson.close();" required>
		    <autocomplete-person #autocompletePerson [key]="person.name" [values]="agentsList | async" (update)="person.name = $event.name; person.ID = $event.$key; addAgent(); " ></autocomplete-person>
		</div>
	</div>

  `
})

export class ProspectStageComponent {

  @Input() workflowItem: WorkflowItem;
  @Output() totalActions = new EventEmitter();
  @Output() requiredActions = new EventEmitter(); 
  public agentsList: FirebaseListObservable<any[]>;
  public workflowItemAgents: FirebaseListObservable<any[]>;
  public agentCount: number = 0;
  public agents: Array<any>; 

  constructor(
    private _auth: AuthService,
    private _confirm: ModalService,
    private _message: MessageService,
    public af: AngularFire,
    public person: Person,
    public attributes: ProspectAttributeService,
    public workflowItemsService: WorkflowItemsService
  ) {
    
  }

  ngOnInit() {
    this.workflowItemsService.totals['prospect'].stage.required = 1;
    this.agentsList = this.af.database.list("/people/" + this._auth.companyID, {
      query: {
        orderByChild: 'agent',
        equalTo: true
      }
    });
  }

  ngOnChanges() {
    if(this.workflowItem) {
      this.workflowItemAgents = this.af.database.list("/workflow-items/" + this._auth.companyID + '/' + this.workflowItem.ID + '/agents');
    }
    
    this.workflowItemAgents.subscribe((values) => {
      this.agents = [];
      values.forEach((val) => {
        val['person'] = this.af.database.object("/people/" + this._auth.companyID + '/' + val.$value);
        this.agents.push(val);
      });
      this.agentCount = values.length;
      this.recalculate();
    });
  }

  clear() {
    this.person.clear();
  }

  addAgent() {
    if (this.person.ID && this.person.ID !== '') {
      this.workflowItem.addTo('agents', this.person.ID, {triggerSave: false}).subscribe(() => {
        this.clear();
      });
    }
  }

  removeAgent(ID) {
    this._confirm.triggerConfirm('Are you sure you want to remove this agent?', () => {
      this.workflowItem.removeFrom('agents', ID, {triggerSave: false}).subscribe(() => {
        this._message.set('Agent removed', 'success');
        this.recalculate();
      });
    }, () => {});
  }

  recalculate() { 
    var met = (this.agentCount > 0) ? 1 : 0;
    this.workflowItem.setRequirement('prospect', met, 'met').subscribe(() => {
      this.workflowItemsService.setTotals();
    });
  }

}

