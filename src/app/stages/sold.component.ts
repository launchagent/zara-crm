/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';

// Models
import { File } from '../files/file';
import { WorkflowItem } from '../workflow-items/workflow-item';
import { Person } from '../people/person';

// Directives
import { OpenHomesComponent } from '../workflow-items/open-homes.component';

// Services
import { MessageService } from '../message/message.service';
import { SoldAttributeService } from '../attributes/sold-attribute.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Sold Component
 * 
 */
@Component({
  selector: 'sold-stage',
  providers: [ File, SoldAttributeService, Person ],
  styleUrls: ['./stages.component.scss'],
  template: `
  	<div class="stage-wrapper">	
		<div class="panel">
		  <h5>Pre-settlement form</h5>
		  <div *ngIf="workflowItem.snapshot.attributes['pre-settlement-form']">
		    <files-component [itemID]="workflowItem.ID" [fileID]="workflowItem.snapshot.attributes['pre-settlement-form']"></files-component>
		  </div>
		  <div *ngIf="!workflowItem.snapshot.attributes['pre-settlement-form']">  
		    <file-uploader [ID]="workflowItem.ID" [type]="'file'" (update)="saveFile('pre-settlement-form', $event.files)"></file-uploader>
		  </div>
		</div>
    <h5>Accepted Offer</h5>
    <offers-component [showAccepted]="true" [workflowItem]="workflowItem"></offers-component>
    <div *ngIf="workflowItem.person.solicitorID != ''">
      <h5>Vendor Solicitor Details</h5>
      <solicitor-component [person]="workflowItem.person"></solicitor-component> 
    </div>
 
    <div *ngIf="buyer.solicitorID != ''">
      <h5>Buyer Solicitor Details</h5>
      <solicitor-component [person]="buyer"></solicitor-component> 
    </div>  

		<form (ngSubmit)="updateWorkflowItem()" #soldForm="ngForm" >
		  <!-- <attributes [(model)]="workflowItem" [attributes]="attributes" [namespace]="'sold'"></attributes> -->
		  <button type="submit" class="button">Settle</button>
		</form>
	</div>
  `
})

export class SoldStageComponent {

  @Input() workflowItem: WorkflowItem;
  public attributes: any;


  constructor(
    private _file: File,
    private _message: MessageService,
    public workflowItemsService: WorkflowItemsService,
    public soldAttributes: SoldAttributeService,
    public buyer: Person
  ) {
    this.attributes = this.soldAttributes.attributes;

  }

  ngOnInit() {
    
  }

  ngOnChanges() {
    if(this.workflowItem && this.workflowItem.snapshot['buyerID'] != '') {
      this.buyer.load(this.workflowItem.snapshot['buyerID']);      
    }

  }

  saveFile(type, files) {
    for (var i = 0; i < files.length; ++i) {
      if (files[i].id) {
        this._file.init(files[i].id);
        this._file.snapshot.itemID = files[i].id;
        this._file.snapshot.name = files[i].name;
        this._file.snapshot.extension = files[i].extension;
        this._file.create(this._file.snapshot).then((id) => {
          this.workflowItem.snapshot['attributes'][type] = id;
          this.updateWorkflowItem();
        });        
      }

    }
    
  }

  updateWorkflowItem() {
    this.workflowItem.saveAttributes();
    this.workflowItem.setRequirement('sold', 1, 'met');
    this.workflowItemsService.setTotals();
    this._message.set('Item saved', 'success');
  }



}

