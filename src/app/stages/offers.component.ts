/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';

// Models
import { File } from '../files/file';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives
import { OffersComponent } from '../offers/offers.component';

// Services
import { MessageService } from '../message/message.service';
import { SoldAttributeService } from '../attributes/sold-attribute.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Offers Stage Component
 * 
 */
@Component({
  selector: 'offers-stage',
  providers: [ File, SoldAttributeService ],
  styles: [''],
  template: `
<div class="panel">
  <h5>Pre-settlement form</h5>
  <div *ngIf="workflowItem.attributes['pre-settlement-form']">
    <files-component [itemID]="workflowItem.ID" [fileID]="workflowItem.attributes['pre-settlement-form']"></files-component>
  </div>
  <div *ngIf="!workflowItem.attributes['pre-settlement-form']">  
    <file-uploader [ID]="workflowItem.ID" [type]="'file'" (update)="saveFile('pre-settlement-form', $event.files)"></file-uploader>
  </div>
</div>
<form (ngSubmit)="updateWorkflowItem()" #soldForm="ngForm" >
  <attributes [(model)]="workflowItem" [attributes]="attributes"></attributes>
  <button type="submit" class="button">Settle</button>
</form>
  `
})

export class OffersStageComponent {

  @Input() workflowItem: WorkflowItem;
  public attributes: any;


  constructor(
    private _file: File,
    private _message: MessageService,
    public workflowItemsService: WorkflowItemsService,
    public soldAttributes: SoldAttributeService
  ) {
    this.attributes = this.soldAttributes.attributes;

  }

  ngOnInit() {
    
  }

  ngOnChanges() {

  }

  saveFile(type, files) {
    for (var i = 0; i < files.length; ++i) {
      if (files[i].id) {
        this._file.init(files[i].id);
        this._file.snapshot.itemID = files[i].id;
        this._file.snapshot.name = files[i].name;
        this._file.snapshot.extension = files[i].extension;
        this._file.create(this._file.snapshot).then((id) => {
          this.workflowItem.snapshot['attributes'][type] = id;
          this.updateWorkflowItem();
        });        
      }

    }
    
  }

  updateWorkflowItem() {
    this.workflowItem.saveAttributes();
    this.workflowItem.setRequirement('sold', 1, 'met');
    this.workflowItemsService.setTotals();
    this._message.set('Item saved', 'success');
  }



}

