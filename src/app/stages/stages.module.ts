import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';


// Directives
import { AppraisalStageComponent } from './appraisal.component';
import { ListingStageComponent } from './listing.component';
import { MarketStageComponent } from './market.component';
import { OffersStageComponent } from './offers.component';
import { ProspectStageComponent } from './prospect.component';
import { SoldStageComponent } from './sold.component';

@NgModule({
    imports: [ RouterModule, CommonModule, SharedModule ],
    declarations: [ 
        AppraisalStageComponent,
        ListingStageComponent,
        MarketStageComponent,
        OffersStageComponent,
        ProspectStageComponent,
        SoldStageComponent
    ],
    exports: [         AppraisalStageComponent,
        ListingStageComponent,
        MarketStageComponent,
        OffersStageComponent,
        ProspectStageComponent,
        SoldStageComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class StagesModule {
}