/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable }  from 'angularfire2';
import { Observable } from 'rxjs/Observable';

// Models
import { File } from '../files/file';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives

// Services
import { AuthService } from '../auth/auth.service';
import { MessageService } from '../message/message.service';
import { ListingAttributeService } from '../attributes/listing-attribute.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Listing Component
 * 
 */
@Component({
  selector: 'listing-stage',
  providers: [ File, ListingAttributeService ],
  styleUrls: ['./stages.component.scss'],
  templateUrl: './listing.component.html'
})

export class ListingStageComponent {

  @Input() workflowItem: WorkflowItem;
  public attributes: any;
  public files: Observable<any>;
  public fileCount: number = 0;
  public hasListingForm: boolean = false;
  public hasMarketingForm: boolean = false;
  public hasDisclosureForm: boolean = false;
  public hasChecklistForm: boolean = false;

  constructor(
    private _auth: AuthService,
    private _file: File,
    private _message: MessageService,
    public af: AngularFire,
    public workflowItemsService: WorkflowItemsService,
    public listingAttributes: ListingAttributeService
  ) {
    this.attributes = this.listingAttributes.attributes;

  }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.workflowItem) {
      this.files = this.af.database.list("/workflow-items/" + this._auth.companyID + '/' + this.workflowItem.ID + '/attributes');
    }
    this.files.subscribe((values) => {
    this.fileCount = 0;

      for (var i = 0; i < values.length; ++i) {
        switch (values[i].$key) {
          case "listing-form":
            this.hasListingForm = true;
            break;
          case "disclosure-transparency-form":
            this.hasDisclosureForm = true;
            break; 
          case "marketing-order-form":
            this.hasMarketingForm = true;
            break; 
          case "listing-checklist":
            this.hasChecklistForm = true;
            break;    
        }
      }
      if(this.hasListingForm) {
        this.fileCount++;
      }
      if(this.hasDisclosureForm) {
        this.fileCount++; 
      } 
      if (this.hasMarketingForm) {
        this.fileCount++;
      } 
      if (this.hasListingForm) {
        this.fileCount++;
      } 
      this.recalculate();       
    });
  }

  saveFile(type, files) {
    for (var i = 0; i < files.length; ++i) {
      if (files[i].id) {
        this._file.init(files[i].id);
        this._file.snapshot.itemID = files[i].id;
        this._file.snapshot.name = files[i].name;
        this._file.snapshot.extension = files[i].extension;
        this._file.create(this._file.snapshot).then(() => {
          this.workflowItem.setAttribute(type, this._file.ID).subscribe(() => {
            this._message.set('Item Saved', 'success');
          });
        });        
      }
    }
    
  }

  removeFile(type, file) {
    this.workflowItem.removeAttribute(type);
  }

  updateWorkflowItem() {
    this.workflowItem.saveAttributes().subscribe(() => {
      this._message.set('Item saved', 'success');
      this.recalculate();
    });
  }

  recalculate() {
    this.workflowItem.setRequirement('listing', this.fileCount, 'met');
    this.workflowItemsService.setTotals();    
  }

}

