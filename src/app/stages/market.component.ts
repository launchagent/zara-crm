/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';

// Models
import { File } from '../files/file';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives
import { OffersComponent } from '../offers/offers.component';
import { OfferForm } from '../offers/offers-form.component';

// Services
import { MessageService } from '../message/message.service';
import { MarketAttributeService } from '../attributes/market-attribute.service';
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Market Component
 * 
 */
@Component({
  selector: 'market-stage',
  providers: [ File, MarketAttributeService ],
  styleUrls: ['./stages.component.scss'],
  template: `
  	<div class="stage-wrapper">	
		<div class="panel">
      Offers
      <form>
      <div><input type="checkbox" [(ngModel)]="workflowItem.snapshot.attributes['emailedVendorLawyer']" name="emailedVendorLawyer"> <label>Emailed vendor lawyer</label></div>
      <div><input type="checkbox" [(ngModel)]="workflowItem.snapshot.attributes['emailedPurchaserLawyer']" name="emailedPurchaserLawyer"> <label>Emailed purchaser lawyer</label></div>
      <div><input type="checkbox" [(ngModel)]="workflowItem.snapshot.attributes['emailedPurchaser']" name="emailedPurchaser"> <label>Emailed purchaser</label></div>
      <div><input type="checkbox" [(ngModel)]="workflowItem.snapshot.attributes['emailedVendor']" name="emailedVendor"> <label>Emailed vendor</label></div>
      <div><input type="checkbox" [(ngModel)]="workflowItem.snapshot.attributes['despositReceived']" name="despositReceived"> <label>Deposit recieved</label></div>
      <div><input type="checkbox" [(ngModel)]="workflowItem.snapshot.attributes['conditionsMet']" name="conditionsMet"> <label>Conditions met</label></div>
      <div><input type="checkbox" [(ngModel)]="workflowItem.snapshot.attributes['depositPaidOut']" name="depositPaidOut"> <label>Deposit paid out</label></div>
      <div class="row"> <div class="small-6 medium-2 columns"><label>Reference number</label></div><div class="small-6 medium-10 columns"><input type="text" [(ngModel)]="workflowItem.snapshot.attributes['offerReferenceNumber']" name="offerReferenceNumber"></div></div>
      <div class="row"> <div class="small-6 medium-2 columns"><label>Price</label></div><div class="small-6 medium-10 columns"><input type="text" [(ngModel)]="workflowItem.snapshot.attributes['offerPrice']" name="offerPrice"></div></div>
      <div><textarea [(ngModel)]="workflowItem.snapshot.attributes['offerNotes']" name="offerNotes"></textarea></div>
      </form>
		</div>
	</div>
  `
})

export class MarketStageComponent {

  @Input() workflowItem: WorkflowItem;
  public attributes: any;


  constructor(
    private _file: File,
    private _message: MessageService,
    public workflowItemsService: WorkflowItemsService,
    public marketAttributes: MarketAttributeService
  ) {
    this.attributes = this.marketAttributes.attributes;

  }

  ngOnInit() {
    
  }

  ngOnChanges() {

  }

  saveFile(type, files) {
    for (var i = 0; i < files.length; ++i) {
      if (files[i].id) {
        this._file.init(files[i].id);
        this._file.snapshot.itemID = files[i].id;
        this._file.snapshot.name = files[i].name;
        this._file.snapshot.extension = files[i].extension;
        this._file.create(this._file.snapshot).then((id) => {
          this.workflowItem.snapshot['attributes'][type] = id;
          this.updateWorkflowItem();
        });        
      }

    }
    
  }

  updateWorkflowItem() {
    this.workflowItem.saveAttributes();
    this.workflowItem.setRequirement('appraisal', 1, 'met');
    this.workflowItemsService.setTotals();
    this._message.set('Item saved', 'success');
  }



}

