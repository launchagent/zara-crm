/*
 * Angular 2 decorators and services
 */
import { Injectable, Inject } from '@angular/core';

/*
 * Stage Model
 * 
 */
@Injectable()

export class Stage {

  public className: string;
  public title: string;
  public order: number;

  constructor(
  ) {
    
  }

  setCounts() {
 
  }

  validateAttributes(attributes, model) {
    var totalFields = 0;
    var totalRequiredFields = 0; 

    for (var i = 0; i < attributes.length; ++i) {
      var field = attributes[i];
      if (field.workflows[0].stage === this.className) {
        totalFields++;
      }
      if(model) {        
        if (field.workflows[0].stage === this.className && (typeof model.attributes[field.slug] === 'undefined' || model.attributes[field.slug] == '')) {
          totalRequiredFields++;
        }
      } else if (field.workflows[0].stage === this.className) {
        totalRequiredFields++;
      }
    }      

    return {total: totalFields, required: totalRequiredFields};

  }

}

