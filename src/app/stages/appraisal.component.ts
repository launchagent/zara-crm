/*
 * Angular 2 decorators and services
 */
import { Component, Input } from '@angular/core';

// Models
import { File } from '../files/file';
import { WorkflowItem } from '../workflow-items/workflow-item';

// Directives

// Services
import { WorkflowItemsService } from '../workflow-items/workflow-items.service';

// Pipes

/*
 * Appraisal Component
 * 
 */
@Component({
  selector: 'appraisal-stage',
  providers: [ File ],
  styleUrls: ['./stages.component.scss'],
  template: `
    <div class="stage-wrapper">
	    <h3>Appraisal</h3>
	    <div class="panel form-wrapper">
	    <div *ngIf="workflowItem.snapshot.attributes && workflowItem.snapshot.attributes['appraisal-doc']">
	      <files-component [itemID]="workflowItem.ID" [fileID]="workflowItem.snapshot.attributes['appraisal-doc']"></files-component>
	    </div>
	    <div *ngIf="!workflowItem.snapshot.attributes || !workflowItem.snapshot.attributes['appraisal-doc']">
	      <h5>Upload appraisal PDF</h5>
	      <file-uploader [ID]="workflowItem.ID" [type]="'file'" (update)="saveFile($event.files)"></file-uploader>
	    </div>
	
	    </div>
	</div>
  `
})

export class AppraisalStageComponent {

  @Input() workflowItem: WorkflowItem;


  constructor(
    private _file: File,
    public workflowItemsService: WorkflowItemsService
  ) {


  }

  ngOnInit() {
    
  }

  ngOnChanges() {

  }

  saveFile(files) {
    for (var i = 0; i < files.length; ++i) {
      if (files[i].id) {
        this._file.init(files[i].id);
        this._file.snapshot.itemID = files[i].id;
        this._file.snapshot.name = files[i].name;
        this._file.snapshot.extension = files[i].extension;
        this._file.create(this._file.snapshot).then((file) => {
          this.workflowItem.setAttribute('appraisal-doc', file.key);
          this.updateWorkflowItem();
        });        
      }
   
    }
    
  }

  updateWorkflowItem() {
    this.workflowItem.setRequirement('appraisal', 1, 'met');
    this.workflowItemsService.setTotals();
  }

}

