import { FirebaseAppConfig } from 'angularfire2';

const configFirebase: FirebaseAppConfig =  {
      apiKey: "AIzaSyA5EJgCNZFrXPQXvKEmIAEGjPXa3UbCyIk",
      authDomain: "zara-crm.firebaseapp.com",
      databaseURL: "https://zara-crm.firebaseio.com",
      storageBucket: "zara-crm.appspot.com",
  };

export const environment = {
  production: true,
  domain: 'https://api.zaracrm.com',
  configFirebase: configFirebase
};
