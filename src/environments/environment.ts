import { FirebaseAppConfig } from 'angularfire2';

const configFirebase: FirebaseAppConfig = {
          apiKey: "AIzaSyAETh6AJ-GdeWTcsHy239jWiOpE-APpBp8",
          authDomain: "staging-zara-crm.firebaseapp.com",
          databaseURL: "https://staging-zara-crm.firebaseio.com",
          storageBucket: "staging-zara-crm.appspot.com",
};

export const environment = {
  production: false,
  domain: 'http://localhost/zara-server/public',
  configFirebase: configFirebase
};
